<ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

    <!-- Sidebar - Brand -->
    <a class="sidebar-brand d-flex align-items-center justify-content-center" href="<?php echo base_url('admin') ?>">
        <div class="sidebar-brand-icon rotate-n-15">
            <i class="fas fa-laugh-wink"></i>
        </div>
        <div class="sidebar-brand-text mx-3">Community <sup>Monitoring</sup></div>
    </a>

    <!-- Divider -->
    <hr class="sidebar-divider my-0">

    <!-- Nav Item - Dashboard -->
    <?php //if ($_SESSION['position'] == 'ADMIN' || $_SESSION['position'] == 'admin') : 
    ?>
    <li class="nav-item active">
        <a class="nav-link" href="<?php echo base_url('admin') ?>">
            <i class="fas fa-fw fa-tachometer-alt"></i>
            <span>Dashboard</span></a>
    </li>
    <?php //endif; 
    ?>

    <!-- Divider -->
    <hr class="sidebar-divider">

    <!-- Heading -->
    <div class="sidebar-heading">Data</div>

    <!-- Nav Item - Pages Collapse Menu -->
    <?php if ($_SESSION['id_role'] == 1) : ?>
        <li class="nav-item active">
            <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapsePages" aria-expanded="true" aria-controls="collapsePages">
                <i class="fas fa-fw fa-folder"></i>
                <span>Master</span>
            </a>
            <div id="collapsePages" class="collapse" aria-labelledby="headingPages" data-parent="#accordionSidebar">
                <div class="bg-white py-2 collapse-inner rounded">
                    <h6 class="collapse-header">Master Management:</h6>
                    <a class="collapse-item" href="<?php echo base_url('admin/master/user') ?>">User</a>
                    <a class="collapse-item" href="<?php echo base_url('admin/master/province') ?>">Provinsi</a>
                    <a class="collapse-item" href="<?php echo base_url('admin/master/district') ?>">Kabupaten</a>
                    <a class="collapse-item" href="<?php echo base_url('admin/master/subdistrict') ?>">Kecamatan</a>
                    <a class="collapse-item" href="<?php echo base_url('admin/master/village') ?>">Kelurahan</a>
                    <a class="collapse-item" href="<?php echo base_url('admin/master/ksm') ?>">KSM</a>
                    <a class="collapse-item" href="<?php echo base_url('admin/master/facilitator') ?>">Fasilitator</a>
                    <a class="collapse-item" href="<?php echo base_url('admin/master/korkot') ?>">Koordinator Kota</a>
                    <a class="collapse-item" href="<?php echo base_url('admin/master/role') ?>">Role</a>
                </div>
            </div>
        </li>
    <?php endif; ?>

    <?php if ($_SESSION['id_role'] != 1) : ?>
        <li class="nav-item active">
            <a class="nav-link" href="<?php echo base_url('admin/report') ?>"> <i class="fas fa-fw fa-folder"></i> <span>Data Laporan KSM</span></a>
        </li>
        <?php if (in_array($_SESSION['id_role'], array(5, 9, 10, 11))) { ?>
            <li class="nav-item active">
                <a class="nav-link" href="<?php echo base_url('/dashboard-uji-petik') ?>"> <i class="fas fa-fw fa-folder"></i> <span>Data Laporan Supervisi</span></a>
            </li>
        <?php } ?>
        <?php
        $role = array(3, 4, 9, 10, 11);
        if (in_array($_SESSION['id_role'], $role)) : ?>
            <li class="nav-item active">
                <a class="nav-link" href="<?php echo base_url('report-monitoring') ?>"> <i class="fas fa-fw fa-folder"></i> <span>Data Laporan Fasilitator</span></a>
            </li>
        <?php endif ?>
    <?php endif ?>

    <!-- Divider -->
    <hr class=" sidebar-divider d-none d-md-block">

    <!-- Sidebar Toggler (Sidebar) -->
    <div class="text-center d-none d-md-inline">
        <button class="rounded-circle border-0" id="sidebarToggle"></button>
    </div>

</ul>