<html>

<head>
    <title>Page Title</title>
    <link rel="stylesheet" href="main.css">
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

    <!-- <script src="jquery.js"></script> -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.3/jquery.js"></script>

    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.panzoom/3.2.2/jquery.panzoom.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/angular-mousewheel/1.0.5/mousewheel.js"></script>
    <!-- <script src="custom.js"></script> -->
    <style>
        .container {
            /* background-color: #fad109 !important; */
        }

        .page-header {
            /* box-shadow: 10px 10px 5px #888888; */
        }

        div#page {
            margin-left: 1em;
            margin-right: 1em;
        }

        #options {
            margin: 1em 1em 0em 1em;
        }

        .carousel-control.left,
        .carousel-control.right {
            background: none !important;
        }

        #myCarousel img {
            width: 100%;
        }

        .optionBox,
        #optionHead1,
        #optionHead2 {
            text-align: center;
        }

        .carousel-inner {
            background-color: black;
        }

        .buttons {
            width: 6%;
            position: absolute;
            top: 2%;
            left: 4%;
            z-index: 99;
        }

        .buttons button {
            display: block;
            margin-bottom: 3px;

        }

        /* CSS REQUIRED */
        .state-icon {
            left: -5px;
        }

        .list-group-item-primary {
            color: rgb(255, 255, 255);
            background-color: rgb(66, 139, 202);
        }

        /* DEMO ONLY - REMOVES UNWANTED MARGIN */
        .well .list-group {
            margin-bottom: 0px;
        }

        @media screen and (min-width: 768px) {

            .carousel-control .glyphicon-chevron-left,
            .carousel-control .icon-prev {
                margin-left: -45px !important;
            }

            .carousel-control .glyphicon-chevron-right,
            .carousel-control .icon-next {
                margin-right: -45px !important;
            }

            #options .row:nth-child(1) {
                /* margin-bottom:4em; */
            }
        }
    </style>
</head>

<body>

    <div class="row">
        <div class="col-md-12">
            <div id="imgDiv">

                <div class="buttons">
                    <button type="button" class=" zoom-out btn btn-default">
                        <span class="glyphicon glyphicon-minus"></span>
                    </button>
                    <button type="button" class=" zoom-in btn btn-default">
                        <span class="glyphicon glyphicon-plus"></span>
                    </button>
                </div>

                <div id="myCarousel" class="carousel slide" data-ride="carousel">
                    <!-- Wrapper for slides -->
                    <div class="carousel-inner" role="listbox">
                        <div class="item active panzoom">
                            <img src="http://webneel.com/wallpaper/sites/default/files/images/04-2013/island-beach-scenery-wallpaper.preview.jpg" class="img-responsive" alt="Dicom">
                        </div>
                        <div class="item panzoom">
                            <img src="https://lh3.googleusercontent.com/y5fkm7STIjAYGdlNLwZIrlqvxqUwEoCGcvWmN3_r_uthFdnmMnQ2oFdELmpJE5cM3UA=h900" class="img-responsive" alt="Dicom">
                        </div>
                        <div class="item panzoom">
                            <img src="https://static.getjar.com/ss/d2/846344_3.jpg" class="img-responsive" alt="Dicom">
                        </div>
                    </div>

                    <!-- Left and right controls -->
                    <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
                        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
                        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>
                </div>

            </div>

            <div class="col-md-12" style="text-align:center;margin-top: 1em;margin-bottom: 1em;">
                <button class="reset btn btn-primary">Reset Zoom</button>
            </div>

        </div>
    </div>
    <script>
        (function() {
            var $section = $('div').first();
            $section.find('.panzoom').panzoom({
                $zoomIn: $section.find(".zoom-in"),
                $zoomOut: $section.find(".zoom-out"),
                $zoomRange: $section.find(".zoom-range"),
                $reset: $section.find(".reset")
            });

            $('#myCarousel').carousel({
                pause: true,
                interval: false
            });

        })();
    </script>
</body>

</html>