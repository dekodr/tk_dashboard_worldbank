<?php echo $this->extend('base/template'); ?>

<?php echo $this->section('content') ?>
<?php
// print_r($this->session());die;
?>
<!-- Page Heading -->
<div class="d-sm-flex align-items-center justify-content-between mb-4">
	<h1 class="h3 mb-0 text-gray-800">Dashboard</h1>
	<!-- <a href="#" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i class="fas fa-download fa-sm text-white-50"></i> Generate Report</a> -->
</div>
<?php if (session()->getFlashdata('success')) { ?>
	<div class="card mb-4 py-3 border-left-success">
		<div class="card-body">
			<?php print_r(session()->getFlashdata('success')) ?>
		</div>
	</div>
<?php } ?>
<!-- Content Row -->
<div class="row">
	<div class="col-xl-8 col-lg-7">
		<form method="POST" class="user" action="<?php echo base_url('admin/dashboard/filter_dashboard') ?>" id="formFilter">
			<div class="form-group">
				<label for="">Tanggal Awal</label>
				<input name="star_date" type="date" class="form-control form-control-user" id="star_date" required>
			</div>
			<div class="form-group">
				<label for="">Tanggal Akhir</label>
				<input name="end_date" type="date" class="form-control form-control-user" id="end_date" required>
			</div>
			<hr>
			<button type="submit" class="btn btn-primary btn-icon-split">
				<span class="icon text-white-50">
					<i class="fas fa-filter"></i>
				</span>
				<span class="text">Filter Data</span>
			</button>
		</form>
	</div>
</div>
<?php if ($_SESSION['id_role'] != 2) { ?>
	<a href="<?= base_url('Admin/Dashboard/detail_graph'); ?>" class="btn btn-primary btn-icon-split mt-3 mb-3" style="height: 40px;">
		<span class="icon text-white-50">
			<i class="fas fa-eye"></i>
		</span>
		<span class="text" style="color: white;">Lihat Detail Grafik</span>
	</a> <br>
<?php } ?>
<div class="row">
	<!-- Earnings (Monthly) Card Example -->
	<div class="col-xl-3 col-md-6 mb-4">
		<div class="card border-left-primary shadow h-100 py-2">
			<div class="card-body">
				<div class="row no-gutters align-items-center">
					<div class="col mr-2">
						<div class="text-xs font-weight-bold text-primary text-uppercase mb-1">Total Laporan</div>
						<div class="h5 mb-0 font-weight-bold text-gray-800" id="total_laporan"><?= $total ?></div>
					</div>
					<div class="col-auto">
						<i class="fas fa-file fa-2x text-gray-300"></i>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- Earnings (Monthly) Card Example -->
	<div class="col-xl-3 col-md-6 mb-4">
		<div class="card border-left-success shadow h-100 py-2">
			<div class="card-body">
				<div class="row no-gutters align-items-center">
					<div class="col mr-2">
						<div class="text-xs font-weight-bold text-success text-uppercase mb-1">Laporan Man. Keuangan</div>
						<div class="h5 mb-0 font-weight-bold text-gray-800" id="total_eco"><?= $total_eco ?></div>
					</div>
					<div class="col-auto">
						<!-- <i class="fas fa-dollar-sign fa-2x text-gray-300"></i> -->
						<i class="fas fa-money-check-alt fa-2x text-gray-300"></i>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="col-xl-3 col-md-6 mb-4">
		<div class="card border-left-info shadow h-100 py-2">
			<div class="card-body">
				<div class="row no-gutters align-items-center">
					<div class="col mr-2">
						<div class="text-xs font-weight-bold text-info text-uppercase mb-1">Laporan Infrastruktur</div>
						<div class="h5 mb-0 font-weight-bold text-gray-800" id="total_infra"><?= $total_infra ?></div>
					</div>
					<div class="col-auto">
						<i class="fas fa-building fa-2x text-gray-300"></i>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- Pending Requests Card Example -->
	<?php if ($_SESSION['id_role'] != 2) : ?>
		<div class="col-xl-3 col-md-6 mb-4">
			<div class="card border-left-warning shadow h-100 py-2">
				<div class="card-body">
					<div class="row no-gutters align-items-center">
						<div class="col mr-2">
							<div class="text-xs font-weight-bold text-warning text-uppercase mb-1">Total Input KSM</div>
							<div class="h5 mb-0 font-weight-bold text-gray-800" id="total_ksm"><?= ($total_ksm) ?></div>
						</div>
						<div class="col-auto">
							<i class="fas fa-comments fa-2x text-gray-300"></i>
						</div>
					</div>
				</div>
			</div>
		</div>
	<?php endif ?>
</div>

<!-- Content Row -->
<div class="row">

	<!-- Content Column -->
	<div class="col-lg-8 mb-4">
		<!-- Project Card Example -->
		<div class="card shadow mb-4">
			<div class="card-header py-3">
				<h6 class="m-0 font-weight-bold text-primary">Laporan Infrastruktur</h6>
			</div>

			<div class="card-body" id="showLaporanInfra">
				<?php foreach ($project as $key => $value) { ?>
					<h4 class="small font-weight-bold"> <?= $key; ?> <span class="float-right"><?php echo ($value > 0) ? $value : 'N/A' ?></span></h4>
					<!-- <div class="progress mb-4">
						<div class="progress-bar bg-danger" role="progressbar" style="width: 0%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
					</div> -->
				<?php } ?>
				<!-- <h4 class="small font-weight-bold">Sales Tracking <span class="float-right">40%</span></h4>
				<div class="progress mb-4">
					<div class="progress-bar bg-warning" role="progressbar" style="width: 40%" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100"></div>
				</div>
				<h4 class="small font-weight-bold">Customer Database <span class="float-right">60%</span></h4>
				<div class="progress mb-4">
					<div class="progress-bar" role="progressbar" style="width: 60%" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100"></div>
				</div>
				<h4 class="small font-weight-bold">Payout Details <span class="float-right">80%</span></h4>
				<div class="progress mb-4">
					<div class="progress-bar bg-info" role="progressbar" style="width: 80%" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100"></div>
				</div>
				<h4 class="small font-weight-bold">Account Setup <span class="float-right">Complete!</span></h4>
				<div class="progress">
					<div class="progress-bar bg-success" role="progressbar" style="width: 100%" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"></div>
				</div> -->
			</div>
		</div>
	</div>

	<!-- Pie Chart -->
	<?php if ($_SESSION['id_role'] != 2) : ?>
		<div class="col-xl-4 col-lg-5">
			<div class="card shadow mb-4">
				<!-- Card Header - Dropdown -->
				<div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
					<h6 class="m-0 font-weight-bold text-primary">Laporan per Kelurahan</h6>
					<!-- <div class="dropdown no-arrow"> -->
					<!-- <a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
							<i class="fas fa-ellipsis-v fa-sm fa-fw text-gray-400"></i>
						</a>
						<div class="dropdown-menu dropdown-menu-right shadow animated--fade-in" aria-labelledby="dropdownMenuLink">
							<div class="dropdown-header">Dropdown Header:</div>
							<a class="dropdown-item" href="#">Action</a>
							<a class="dropdown-item" href="#">Another action</a>
							<div class="dropdown-divider"></div>
							<a class="dropdown-item" href="#">Something else here</a>
						</div> -->
					<!-- </div> -->
				</div>
				<!-- Card Body -->
				<!-- <div class="card-body">
					<div class="chart-pie pt-4 pb-2">
						<canvas id="kelChart"></canvas>
					</div>
				</div> -->
				<figure class="highcharts-figure">
					<div id="container"></div>

					<table id="datatable" style="display: none;">
						<thead>
							<tr>
								<th></th>
								<th>Lap.Keuangan</th>
								<th>Lap. Infrastuktur</th>
							</tr>
						</thead>
						<tbody>
							<?php foreach ($reportByType as $key => $value) { ?>
								<tr>
									<th><?php echo $key ?> (<?php echo $value['keuangan'] + $value['infrastruktur']; ?>)</th>
									<td><?php echo $value['keuangan']; ?></td>
									<td><?php echo $value['infrastruktur']; ?></td>
								</tr>
							<?php } ?>
						</tbody>
					</table>
				</figure>
			</div>
		</div>

	<?php endif ?>
</div>

<!-- Content Row -->
<div class="row">
	<!-- Content Column -->
	<div class="col-lg-8 mb-4">
		<!-- Project Card Example -->
		<div class="card shadow mb-4">
			<div class="card-header py-3">
				<h6 class="m-0 font-weight-bold text-primary">Progress Laporan Man. Keuangan</h6>
			</div>

			<div class="card-body">
				<h4 class="small font-weight-bold">Total laporan yang telah disetujui - <?= $total_eco_approve ?>/<?= $total_eco ?>
					<span class="float-right">
						<?php
						$percent = ($total_eco_approve > 0) ? number_format((float)($total_eco_approve / $total_eco * 100), 2, '.', '') : '0';
						echo $percent;
						?>
						%
					</span>
				</h4>
				<div class="progress mb-4">
					<div class="progress-bar bg-info" role="progressbar" style="width: <?= $percent ?>%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
				</div>

				<!-- <h4 class="small font-weight-bold">Sales Tracking <span class="float-right">40%</span></h4>
				<div class="progress mb-4">
					<div class="progress-bar bg-warning" role="progressbar" style="width: 40%" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100"></div>
				</div>
				<h4 class="small font-weight-bold">Customer Database <span class="float-right">60%</span></h4>
				<div class="progress mb-4">
					<div class="progress-bar" role="progressbar" style="width: 60%" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100"></div>
				</div>
				<h4 class="small font-weight-bold">Payout Details <span class="float-right">80%</span></h4>
				<div class="progress mb-4">
					<div class="progress-bar bg-info" role="progressbar" style="width: 80%" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100"></div>
				</div>
				<h4 class="small font-weight-bold">Account Setup <span class="float-right">Complete!</span></h4>
				<div class="progress">
					<div class="progress-bar bg-success" role="progressbar" style="width: 100%" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"></div>
				</div> -->
			</div>
		</div>
	</div>
	<?php if ($_SESSION['id_role'] != 2) : ?>
		<div class="col-xl-4 col-lg-7">
			<div class="card shadow mb-4">
				<div class="card-header py-3">
					<h6 class="m-0 font-weight-bold text-primary">Laporan per KSM</h6>
				</div>
				<div class="card-body">
					<div class="chart-pie pt-4 pb-2">
						<canvas id="ksmChart"></canvas>
					</div>
				</div>
			</div>
		</div>

	<?php endif ?>
</div>

<?php echo $this->endSection() ?> ?>

<?php echo $this->section('script'); ?>

<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/data.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="https://code.highcharts.com/modules/accessibility.js"></script>
<script>
	// Set new default font family and font color to mimic Bootstrap's default styling
	Chart.defaults.global.defaultFontFamily = 'Nunito', '-apple-system,system-ui,BlinkMacSystemFont,"Segoe UI",Roboto,"Helvetica Neue",Arial,sans-serif';
	Chart.defaults.global.defaultFontColor = '#858796';

	// Pie Chart KSM
	var ctx = document.getElementById("ksmChart");
	var ksm = <?= $ksm ?>;
	var ksmChart = new Chart(ctx, {
		type: 'pie',
		data: {
			labels: ksm.label,
			datasets: [{
				data: ksm.value,
				backgroundColor: ['#16a085', '#c0392b', '#2980b9', '#8e44ad', '#2c3e50', '#f39c12'],
				// hoverBackgroundColor: ['#2e59d9', '#17a673', '#2c9faf', '#36b9cc', '#36b9cc'],
				hoverBorderColor: "rgba(234, 236, 244, 1)",
			}],
		},
		options: {
			maintainAspectRatio: false,
			tooltips: {
				backgroundColor: "rgb(255,255,255)",
				bodyFontColor: "#858796",
				borderColor: '#dddfeb',
				borderWidth: 2,
				xPadding: 20,
				yPadding: 20,
				displayColors: true,
				caretPadding: 10,
			},
			legend: {
				display: true
			},
			cutoutPercentage: 60,
		},
	});

	// Pie Chart KELURAHAN
	// var ctx 		= document.getElementById("kelChart");
	// var kel 		= <?= $kel ?>;
	// var ksmChart 	= new Chart(ctx, {
	// 	type: 'pie',
	// 	data: {
	// 		labels: kel.label,
	// 		datasets: [{
	// 			data: kel.value,
	// 			backgroundColor: ['#16a085', '#c0392b', '#2980b9', '#8e44ad', '#2c3e50', '#f39c12'],
	// 			// hoverBackgroundColor: ['#2e59d9', '#17a673', '#2c9faf', '#36b9cc', '#36b9cc'],
	// 			hoverBorderColor: "rgba(234, 236, 244, 1)",
	// 		}],
	// 	},
	// 	options: {
	// 		maintainAspectRatio: false,
	// 		tooltips: {
	// 			backgroundColor: "rgb(255,255,255)",
	// 			bodyFontColor: "#858796",
	// 			borderColor: '#dddfeb',
	// 			borderWidth: 2,
	// 			xPadding: 20,
	// 			yPadding: 20,
	// 			displayColors: true,
	// 			caretPadding: 10,
	// 		},
	// 		legend: {
	// 			display: true
	// 		},
	// 		cutoutPercentage: 60,
	// 	},
	// });

	Highcharts.chart('container', {
		data: {
			table: 'datatable'
		},
		chart: {
			type: 'column'
		},
		title: {
			text: 'Grafik Laporan'
		},
		yAxis: {
			allowDecimals: false,
			title: {
				text: 'Banyak Laporan'
			}
		},
		tooltip: {
			formatter: function() {
				return '<b>' + this.series.name + '</b><br/>' +
					this.point.y + ' ' + this.point.name.toUpperCase();
			}
		}
	});

	$("#formFilter").on('submit', function(e) {
		e.preventDefault();
		url = $(this).attr('action');
		formData = $(this).serialize;

		$.ajax({
			url: url,
			method: 'POST',
			data: formData,
			dataType: 'json',
			success: function(xhr) {
				$('#total_laporan').html(xhr.total);
				$('#total_eco').html(xhr.total_eco);
				$('#total_infra').html(xhr.total_infra);
				$('#total_ksm').html(xhr.total_ksm);
			}
		})
	});
</script>
<?php echo $this->endSection() ?> ?>