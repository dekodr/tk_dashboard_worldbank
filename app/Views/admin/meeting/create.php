<?php echo $this->extend('base/template'); ?>

<?php echo $this->section('content') ?>
    <!-- Page Heading -->
    
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Meeting Schedule</h1>
	</div>

	<?php if(session()->getFlashdata('error')){?>
		<div class="card mb-4 py-3 border-left-danger">
			<div class="card-body">
				<?php print_r(session()->getFlashdata('error'))?>
			</div>
		</div>
	<?php }?>
	
    <!-- DataTales Example -->
    <div class="card shadow mb-4">
        <div class="card-header py-3">
			<h6 class="m-0 font-weight-bold text-primary">Form - Create New Meeting Schedule</h6>
        </div>
        <div class="card-body row">
            <div class="col-xl-8 col-lg-7">
                <form method="POST" class="user">
                    
                    <div class="form-group">
                        <input name="name" type="text" class="form-control form-control-user" id="name" aria-describedby="emailHelp" placeholder="Meeting Name" required>
                    </div>
                    <div class="form-group">
                        <input name="date" type="date" class="form-control form-control-user" id="date" aria-describedby="emailHelp" placeholder="Date" required>
                    </div>
                    <div class="form-group row">
                        <div class="col-sm-6 mb-3 mb-sm-0">
                            <input name="start_time" type="time" class="form-control form-control-user" id="start_time" placeholder="Start Time" required>
                        </div>
                        <div class="col-sm-6">
                            <input name="end_time" type="time" class="form-control form-control-user" id="end_time" placeholder="End Time" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <input name="participant" type="text" class="form-control form-control-user tagator" id="participant" aria-describedby="emailHelp" placeholder="Participant" required>
                    </div>
                    <div class="form-group">
                        <TextArea name="description" class="form-control form-control-user" id="description" aria-describedby="emailHelp" placeholder="Description..." required></TextArea>
                    </div>
                    
                    <hr>
                    <button type="submit" class="btn btn-primary btn-icon-split">
                        <span class="icon text-white-50">
                        <i class="fas fa-save"></i>
                        </span>
                        <span class="text">Save Data</span>
                    </button>
                </form>
            </div>

			<div class="col-xl-4 col-lg-5">
				<img style="width: 100%" src="<?php echo base_url('assets/img/bg-create-meeting.png')?>">
            </div>
        </div>
    </div>
<?php echo $this->endSection() ?> ?>
<?php echo $this->section('script') ?>
	<script type="text/javascript">
		// $(document).ready(function() {
		// 	$("#division").hashtags();
        // });
        $('#participant').tagator({
            autocomplete: <?php echo json_encode($participant);?>,
            useDimmer: true,             // dims the screen when result list is visible
            allowAutocompleteOnly: true,
        });
	</script>
<?php echo $this->endSection() ?> ?>
         