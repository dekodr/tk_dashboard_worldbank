<?php echo $this->extend('base/template'); ?>

<?php echo $this->section('content') ?>
<!-- Page Heading -->

<div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">User</h1>
</div>

<?php if (session()->getFlashdata('error')) { ?>
    <div class="card mb-4 py-3 border-left-danger">
        <div class="card-body">
            <?php print_r(session()->getFlashdata('error')) ?>
        </div>
    </div>
<?php } ?>

<!-- DataTales Example -->
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Form - Change Username dan password </h6>
    </div>
    <div class="card-body row">
        <div class="col-xl-8 col-lg-7">
            <form method="POST" class="user" accept-charset="utf-8" enctype="multipart/form-data" action="<?php echo base_url('admin/master/user/change_user') ?>">
                <input name="iduser" type="hidden" class="form-control form-control-user" id="iduser" placeholder="Full Name" value="<?= $getUser->id; ?>">

                <div class="form-group">
                    <label for="">Username</label>
                    <input name="username" type="text" class="form-control form-control-user" id="name" placeholder="username" value="<?= $usernm->username; ?>" required>
                </div>
                <!-- <div class="form-group">
                    <label for="">Password Lama</label>
                    <input name="passlama" type="text" class="form-control form-control-user" id="passlama" placeholder="password lama" value="<?= ($isEdit != 'edit') ? '' : $getUser->name; ?>" required>
                </div> -->
                <div class="form-group">
                    <label for="">Password Baru</label>
                    <input name="passbaru" type="password" class="form-control form-control-user" id="passbaru" placeholder="Password Baru" value="<?= ($isEdit != 'edit') ? '' : $getUser->name; ?>" required>
                </div>
                <hr>
                <button type="submit" class="btn btn-primary btn-icon-split">
                    <span class="icon text-white-50">
                        <i class="fas fa-save"></i>
                    </span>
                    <span class="text">Ubah Data</span>
                </button>
                <a href="<?php echo base_url('admin/master/user') ?>" class="btn btn-secondary">Kembali</a>
            </form>
        </div>
    </div>
</div>
<?php echo $this->endSection() ?> ?>
<?php echo $this->section('script') ?>
<script type="text/javascript">
    $(document).ready(function() {
        // var isEdit = <?= $isEdit; ?>;
        // var jns_user = <?= $getUser->position; ?>;
        // if (isEdit == 'edit') {
        //     if (jns_user == 'Fasilitator Teknik') {

        //     } else {
        //         $('#pos2').hide();
        //     }
        // } else {
        //     $('#pos2').hide();
        // }

        // $('#jns_user').change(function() {
        //     var jns_user = $('#jns_user').val();

        //     if (jns_user == 'korkot') {
        //         $('#infra').hide();
        //         $('#keu').hide();
        //         $('#korkot').hide();
        //         $('#ksm').show();
        //         $('#pos1').show();
        //         $('#pos2').hide();
        //     } else if (jns_user == 'fasilitator') {
        //         $('#infra').hide();
        //         $('#keu').hide();
        //         $('#korkot').show();
        //         $('#ksm').show();
        //         $('#pos1').hide();
        //         $('#pos2').show();
        //     } else {
        //         $('#pos1').show();
        //         $('#pos2').hide();
        //         $('#infra').show();
        //         $('#keu').show();
        //         $('#korkot').show();
        //         $('#ksm').show();
        //     }
        // });

    });
</script>
<?php echo $this->endSection() ?> ?>