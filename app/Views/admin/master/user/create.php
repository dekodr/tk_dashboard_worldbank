<?php echo $this->extend('base/template'); ?>

<?php echo $this->section('content') ?>
<!-- Page Heading -->

<div class="d-sm-flex align-items-center justify-content-between mb-4">
	<h1 class="h3 mb-0 text-gray-800">User</h1>
</div>

<?php if (session()->getFlashdata('error')) { ?>
	<div class="card mb-4 py-3 border-left-danger">
		<div class="card-body">
			<?php print_r(session()->getFlashdata('error')) ?>
		</div>
	</div>
<?php } ?>

<!-- DataTales Example -->
<div class="card shadow mb-4">
	<div class="card-header py-3">
		<h6 class="m-0 font-weight-bold text-primary">Form - Create User </h6>
	</div>
	<div class="card-body row">
		<div class="col-xl-8 col-lg-7">
			<form method="POST" class="user" accept-charset="utf-8" enctype="multipart/form-data" action="<?php echo base_url('admin/master/user/save') ?>">
				<div class="form-group">
					<label for="">Jenis User</label>
					<select class="form-control col-sm-12 rounded-pill" id="id_role" name="id_role">
						<option value="">- Pilih Role -</option>
						<?php foreach ($role as $key => $values) : ?>
							<option value="<?= $values->id; ?>"><?= $values->name_role; ?></option>
						<?php endforeach; ?>
					</select>
				</div>

				<input name="iduser" type="hidden" class="form-control form-control-user" id="iduser" placeholder="Full Name" value="<?= ($isEdit != 'edit') ? '' : $getUser->id; ?>" required>

				<div class="form-group">
					<label for="">Nama</label>
					<input name="name" type="text" class="form-control form-control-user" id="name" placeholder="Full Name" value="<?= ($isEdit != 'edit') ? '' : $getUser->name; ?>" required>
				</div>

				<div class="form-group" id="pos1">
					<label for="">Position</label>
					<input name="position1" type="text" class="form-control form-control-user" id="position1" value="<?= ($isEdit != 'edit') ? '' : $getUser->position; ?>" placeholder="Position">
				</div>
				<div class="form-group" id="pos2">
					<label for="">Jenis Fasilistator</label>
					<select class="form-control  col-sm-12 rounded-pill" id="position2" name="position2">
						<option value="">- Pilih Jenis Fasilitator -</option>
						<option value="Fasilitator Teknik" <?php ($isEdit == 'edit') ? ($getUser->position == 'Fasilitator Teknik' ? 'selected' : '') : '' ?>> Fasilitator Teknik </option>
						<option value="Fasilitator Ekonomi" <?php ($isEdit == 'edit') ? ($getUser->position == 'Fasilitator Ekonomi' ? 'selected' : '') : '' ?>> Fasilitator Ekonomi </option>
					</select>
				</div>

				<div class="form-group">
					<label for="">No. Telp</label>
					<input name="phone" type="number" class="form-control form-control-user" id="phone" value="<?= ($isEdit != 'edit') ? '' : $getUser->phone; ?>" placeholder="No. telp" required>
				</div>
				<div class="form-group">
					<label for="">Email</label>
					<input name="email" type="email" class="form-control form-control-user" id="email" value="<?= ($isEdit != 'edit') ? '' : $getUser->email; ?>" placeholder="Email" required>
				</div>

				<div class="form-group">
					<label for="">Username</label>
					<input name="username" type="text" class="form-control form-control-user" id="username" value="" placeholder="Username" required>
				</div>
				<div class="form-group">
					<label for="">Password</label>
					<input name="password" type="password" class="form-control form-control-user" id="password" value="" placeholder="Password" required>
				</div>

				

				<div class="form-group" id="korkot">
					<label for="">KORKOT</label>
					<select class="form-control col-sm-12 rounded-pill" id="korkot" name="korkot">
						<option value="">- Pilih Korkot -</option>
						<?php foreach ($korkot as $key => $value) : ?>
							<?php if (($value->id_boss == null && $value->id_boss2 == null) || ($value->id_boss == 0 && $value->id_boss2 == 0)) : ?>
								<option value="<?= $value->id; ?>" <?= ($getUser->id_boss == $value->id) ? 'selected' : '' ?>><?= $value->name; ?></option>
							<?php endif; ?>
						<?php endforeach; ?>
					</select>
				</div>

				<div class="form-group" id="provinsi">
					<label for="">Provinsi</label>
					<select class="form-control col-sm-12 rounded-pill" id="id_provinsi" name="id_provinsi">
						<option value="">- Pilih Provinsi -</option>
						<?php foreach ($province as $key => $values) : ?>
							<option value="<?= $values->id; ?>" ><?= $values->name; ?></option>
						<?php endforeach; ?>
					</select>
				</div>

				<div class="form-group" id="kabupaten">
					<label for="">Kabupaten</label>
					<select class="form-control col-sm-12 rounded-pill" id="id_kabupaten" name="id_kabupaten">
						<option value="">- Pilih Kabupaten -</option>
						<?php foreach ($district as $key => $values) : ?>
							<option value="<?= $values->id; ?>"><?= $values->name; ?></option>
						<?php endforeach; ?>
					</select>
				</div>

				<div class="form-group" id="kecamatan">
					<label for="">Kecamatan</label>
					<select class="form-control col-sm-12 rounded-pill" id="id_kecamatan" name="id_kecamatan">
						<option value="">- Pilih Kecamatan -</option>
						<?php foreach ($sub as $key => $values) : ?>
							<option value="<?= $values->id; ?>"><?= $values->name; ?></option>
						<?php endforeach; ?>
					</select>
				</div>

				<div class="form-group" id="kelurahan">
					<label for="">Kelurahan</label>
					<select class="form-control col-sm-12 rounded-pill" id="id_kelurahan" name="id_kelurahan">
						<option value="">- Pilih Kelurahan -</option>
						<?php foreach ($village as $key => $values) : ?>
							<option value="<?= $values->id; ?>"><?= $values->name; ?></option>
						<?php endforeach; ?>
					</select>
				</div>

				<div class="form-group" id="ksm">
					<label for="">KSM</label>
					<select class="form-control col-sm-12 rounded-pill" id="idksm" name="idksm">
						<option value="">- Pilih KSM -</option>
						<?php foreach ($ksm as $key => $values) : ?>
							<option value="<?= $values->id; ?>"><?= $values->name; ?></option>
						<?php endforeach; ?>
					</select>
				</div>

				<div class="form-group" id="fasil">
					<label for="">Fasilitator</label>
					<select class="form-control col-sm-12 rounded-pill" id="id_fasilitator" name="id_fasilitator">
						<option value="">- Pilih Fasilitator -</option>
						<?php foreach ($ksm1 as $key => $values) : ?>
							<option value="<?= $values->id; ?>"><?= $values->name; ?></option>
						<?php endforeach; ?>
					</select>
				</div>

				<div class="form-group" id="koordinator">
					<label for="">Koordinator Kota</label>
					<select class="form-control col-sm-12 rounded-pill" id="id_korkot" name="id_korkot">
						<option value="">- Pilih Koordinator Kota -</option>
						<?php foreach ($koordinator as $key => $values) : ?>
							<option value="<?= $values->id; ?>"><?= $values->name; ?></option>
						<?php endforeach; ?>
					</select>
				</div>

				<!-- <div class="col-sm-6">
					<input name="idksm" type="number" class="form-control form-control-user" id="idksm" value="<?= ($isEdit != 'edit') ? '' : $getUser->id_ksm; ?>" placeholder="No. ID KSM" required>
				</div> -->
				<hr>
				<button type="submit" class="btn btn-primary btn-icon-split">
					<span class="icon text-white-50">
						<i class="fas fa-save"></i>
					</span>
					<span class="text">Save Data</span>
				</button>
				<a href="<?php echo base_url('admin/master/user') ?>" class="btn btn-secondary">Kembali</a>
			</form>
		</div>
	</div>
</div>
<?php echo $this->endSection() ?> ?>
<?php echo $this->section('script') ?>
<script type="text/javascript">
	$(document).ready(function() {
		$('#pos2').hide();
		$('#korkot').hide();
		$('#provinsi').hide();
		$('#kabupaten').hide();
		$('#kecamatan').hide();
		$('#kelurahan').hide();
		$('#ksm').hide();
		$('#fasil').hide();
		$('#koordinator').hide();

		$("#username").blur(function() {
			var username = $('#username').val();
			var urll = '<?php echo base_url() ?>';

			$.ajax({
				url: urll + '/admin/master/user/check_username',
				method: 'POST',
				data: {
					username: username
				},
				dataType: 'json',
				success: function(data) {
					if (data != null) {
						alert("Username sudah pernah dipakai");
						document.getElementById('username').value = "";
					}
				}
			})
		})

		// $('#idksm').change(function(){
		// 	var ksm = $('#idksm').val();
		// 	var provinsi = $('#id_provinsi').val();
		// 	var city = $('#id_city').val();
		// 	// alert(city)
		// });

		$('#id_role').change(function() {
			var id_role = $('#id_role').val();
			document.getElementById('position1').value = "";

			// if (id_role == 5 || id_role == 'askot_mk' ||
			// 	id_role == 'askot_infra' || id_role == 'ta_mk' || id_role == 'ta_infra' || id_role == 'ta_monev') {
			if (id_role == 5 || id_role == 9 || id_role == 6) {
				$('#korkot').hide();
				$('#ksm').hide();
				$('#pos1').show();
				$('#pos2').hide();
				$('#senior').hide();
				$('#kecamatan').hide();
				$('#kelurahan').hide();
				$('#fasil').hide();
				$('#kabupaten').hide();
				$('#provinsi').show();
				$('#koordinator').show();
				document.getElementById('position1').value = "";
			} else if (id_role == 10) {
				$('#korkot').hide();
				$('#ksm').hide();
				$('#pos1').show();
				$('#pos2').hide();
				$('#senior').hide();
				$('#kecamatan').hide();
				$('#kelurahan').hide();
				$('#fasil').hide();
				$('#kabupaten').hide();
				$('#provinsi').show();
				$('#koordinator').hide();
				document.getElementById('position1').value = "";
			} else if (id_role == 3 || id_role == 4) {
				$('#korkot').hide();
				$('#ksm').hide();
				$('#pos1').hide();
				$('#kecamatan').hide();
				$('#kelurahan').hide();
				$('#koordinator').hide();
				$('#pos2').show();
				$('#provinsi').show();
				$('#kabupaten').show();
				$('#fasil').show();
				document.getElementById('position1').value = "";
			} else if (id_role == 1 || id_role == 12) {
				$('#korkot').hide();
				$('#ksm').hide();
				$('#pos1').hide();
				$('#pos2').hide();
				$('#senior').hide();
				$('#provinsi').hide();
				$('#kabupaten').hide();
				$('#kecamatan').hide();
				$('#kelurahan').hide();
				$('#fasil').hide();
				$('#koordinator').hide();
				document.getElementById('position1').value = "ADMIN";
			} else {
				$('#pos2').hide();
				$('#korkot').hide();
				$('#senior').hide();
				$('#fasil').hide();
				$('#koordinator').hide();
				$('#pos1').show();
				$('#ksm').show();
				$('#provinsi').show();
				$('#kabupaten').show();
				$('#kecamatan').show();
				$('#kelurahan').show();
				document.getElementById('position1').value = "";
			}
		});
		
		var urll = '<?php echo base_url() ?>';
		$("#id_provinsi").change(function() {
			var id_role = $('#id_role').val();
			if (id_role == 2 || id_role == 3 || id_role == 4) {
				var id_provinsi = $('#id_provinsi').val();
				$('#id_kabupaten').val('');

				$.ajax({
					url: urll + '/admin/master/user/getKabByProv',
					method: 'POST',
					data: {
						id_provinsi: id_provinsi
					},
					dataType: 'json',
					success: function(data) {

						var html = '';
						var i;
						html += '<option value="">- Pilih Kabupaten -</option>'
						for (i = 0; i < data.length; i++) {
							html += '<option value="' + data[i].id + '">' + data[i].name + '</option>';
						}

						$('#id_kabupaten').html(html);
					}
				});
				return false;
			} else if (id_role == 5 || id_role == 9 || id_role == 6) {
				var id_provinsi = $('#id_provinsi').val();
				$('#id_korkot').val('');

				$.ajax({
					url: urll + '/admin/master/user/getKorkotByProv',
					method: 'POST',
					data: {
						id_provinsi: id_provinsi
					},
					dataType: 'json',
					success: function(data) {

						var html = '';
						var i;
						html += '<option value="">- Pilih Koordinator Kota -</option>'
						for (i = 0; i < data.length; i++) {
							html += '<option value="' + data[i].id + '">' + data[i].name + '</option>';
						}

						$('#id_korkot').html(html);
					}
				});
				return false;
			}
		});

		$("#id_kabupaten").change(function() {
			var id_role = $('#id_role').val();
			if (id_role == 2) {
				var id_kabupaten = $('#id_kabupaten').val();
				$('#id_kecamatan').val('');

				$.ajax({
					url: urll + '/admin/master/user/getSubByDistrict',
					method: 'POST',
					data: {
						id_kabupaten: id_kabupaten
					},
					dataType: 'json',
					success: function(data) {

						var html = '';
						var i;
						html += '<option value="">- Pilih Kecamatan -</option>'
						for (i = 0; i < data.length; i++) {
							html += '<option value="' + data[i].id + '">' + data[i].name + '</option>';
						}

						$('#id_kecamatan').html(html);
					}
				});
				return false;				
			} else if (id_role == 3 || id_role == 4) {
				var id_kabupaten = $('#id_kabupaten').val();
				$('#id_fasilitator').val('');
				// alert(id_kabupaten)
				$.ajax({
					url: urll + '/admin/master/user/getKSMByDistrict',
					method: 'POST',
					data: {
						id_kabupaten: id_kabupaten
					},
					dataType: 'json',
					success: function(data) {
						// alert(id_kabupaten)
						var html = '';
						var i;
						html += '<option value="">- Pilih Fasilitator -</option>'
						for (i = 0; i < data.length; i++) {
							html += '<option value="' + data[i].id + '">' + data[i].name + '</option>';
						}

						$('#id_fasilitator').html(html);
					}
				});
				return false;
			}
		});

		$("#id_kecamatan").change(function() {
			var id_kecamatan = $('#id_kecamatan').val();
			$('#id_kelurahan').val('');

			$.ajax({
				url: urll + '/admin/master/user/getVilBySub',
				method: 'POST',
				data: {
					id_kecamatan: id_kecamatan
				},
				dataType: 'json',
				success: function(data) {

					var html = '';
					var i;
					html += '<option value="">- Pilih Kelurahan -</option>'
					for (i = 0; i < data.length; i++) {
						html += '<option value="' + data[i].id + '">' + data[i].name + '</option>';
					}

					$('#id_kelurahan').html(html);
				}
			});
			return false;
		});

		$("#id_kelurahan").change(function() {
			var id_kelurahan = $('#id_kelurahan').val();
			$('#idksm').val('');
			// alert(id_kelurahan)
			$.ajax({
				url: urll + '/admin/master/user/getKSMByVil',
				method: 'POST',
				data: {
					id_kelurahan: id_kelurahan
				},
				dataType: 'json',
				success: function(data) {

					var html = '';
					var i;
					html += '<option value="">- Pilih KSM -</option>'
					for (i = 0; i < data.length; i++) {
						html += '<option value="' + data[i].id + '">' + data[i].name + '</option>';
					}

					$('#idksm').html(html);
				}
			});
			return false;
		});
	});
</script>
<?php echo $this->endSection() ?> ?>