<?php echo $this->extend('base/template'); ?>

<?php echo $this->section('content') ?>
<!-- Page Heading -->

<div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">Kelurahan</h1>
    <a href="<?php echo base_url('admin/master/village/create') ?>" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i class="fas fa-plus fa-sm text-white-50"></i> Tambah Keluarahan Baru</a>
</div>

<?php if (session()->getFlashdata('success')) { ?>
    <div class="card mb-4 py-3 border-left-success">
        <div class="card-body">
            <?php print_r(session()->getFlashdata('success')) ?>
        </div>
    </div>
<?php } ?>
<!-- DataTales Example -->
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Datalist Kelurahan</h6>
    </div>
    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                    <tr>
                        <th width="14%">Code</th>
                        <th>Name</th>
                        <th width="13%">Action</th>
                    </tr>
                </thead>
                <?php echo ($table); ?>
                </tbody>
            </table>
        </div>
    </div>
</div>

<?php echo $this->endSection() ?> ?>

<?php echo $this->section('script') ?>
<script>
    // Call the dataTables jQuery plugin
    $(document).ready(function() {
        $('#dataTable').DataTable();
    });
</script>
<?php echo $this->endSection() ?> ?>