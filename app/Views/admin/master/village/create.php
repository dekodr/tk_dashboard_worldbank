<?php echo $this->extend('base/template'); ?>

<?php echo $this->section('content') ?>
<!-- Page Heading -->

<div class="d-sm-flex align-items-center justify-content-between mb-4">
	<h1 class="h3 mb-0 text-gray-800">Kelurahan</h1>
</div>

<?php if (session()->getFlashdata('error')) { ?>
	<div class="card mb-4 py-3 border-left-danger">
		<div class="card-body">
			<?php print_r(session()->getFlashdata('error')) ?>
		</div>
	</div>
<?php } ?>

<!-- DataTales Example -->
<div class="card shadow mb-4">
	<div class="card-header py-3">
		<h6 class="m-0 font-weight-bold text-primary">Form - <?= ($isEdit == 'edit') ? 'Edit' : 'Input'; ?> Kelurahan</h6>
	</div>
	<div class="card-body row">
		<div class="col-xl-8 col-lg-7">
			<form method="POST" class="user" accept-charset="utf-8" action="<?php echo base_url('admin/master/village/save') ?>">
				<input type="hidden" name="getKel" id="getKel" value="<?= $getKel->id; ?>">

				<div class="form-group" id="provinsi">
					<!-- <label for="exampleFormControlSelect1">Provinsi</label> -->
					<select class="form-control col-sm-12 rounded-pill" id="id_provinsi" name="id_provinsi">
						<option value="">- Pilih Provinsi -</option>
						<?php foreach ($province as $key => $values) : ?>
							<option value="<?= $values->id; ?>" <?= ($values->id == $getKel->id_province) ? 'selected' : '' ?>><?= $values->name; ?></option>
						<?php endforeach; ?>
					</select>
				</div>

				<div class="form-group" id="kabupaten">
					<!-- <label for="exampleFormControlSelect1">Kabupaten</label> -->
					<select class="form-control col-sm-12 rounded-pill" id="id_kabupaten" name="id_kabupaten">
						<option value="">- Pilih Kabupaten -</option>
						<?php foreach ($district as $key => $values) : ?>
							<option value="<?= $values->id; ?>" <?= ($values->id == $getKel->id_kabupaten) ? 'selected' : '' ?>><?= $values->name; ?></option>
						<?php endforeach; ?>
					</select>
				</div>

				<div class="form-group" id="kecamatan">
					<input type="hidden" id="idKec2" name="idKec2" value="<?= ($isEdit == '') ? '' : $getKel->id; ?>">
					<!-- <label for="exampleFormControlSelect1">Kecamatan</label> -->
					<select class="form-control col-sm-12 rounded-pill" id="id_kecamatan" name="id_kecamatan">
						<option value="">- Pilih Kecamatan -</option>
						<?php foreach ($sub as $key => $values) : ?>
							<option value="<?= $values->id; ?>" <?= ($values->id == $getKel->id_kecamatan) ? 'selected' : '' ?>><?= $values->name; ?></option>
						<?php endforeach; ?>
					</select>
				</div>

				<!-- <label for="exampleFormControlSelect1">Kecamatan</label> -->
				<div class="form-group row">
					<div class="col-sm-3 mb-3 mb-sm-0">
						<input name="code" type="text" class="form-control form-control-user" id="code" placeholder="Code Kelurahan" value="<?= ($isEdit == 'edit') ? $getKel->code : ''; ?>" required>
					</div>
					<div class="col-sm-8">
						<input name="name" type="text" class="form-control form-control-user" id="name" placeholder="Nama Kelurahan" value="<?= ($isEdit == 'edit') ? $getKel->name : ''; ?>" required>
					</div>
				</div>
				<hr>
				<button type="submit" class="btn btn-primary btn-icon-split">
					<span class="icon text-white-50">
						<i class="fas fa-save"></i>
					</span>
					<span class="text">Simpan Data</span>
				</button>
				<a href="<?php echo base_url('admin/master/village') ?>" class="btn btn-secondary">Kembali</a>
			</form>
		</div>
	</div>
</div>
<?php echo $this->endSection() ?> ?>
<?php echo $this->section('script') ?>
<script type="text/javascript">
	$(document).ready(function() {
		var urll = '<?php echo base_url() ?>';
		$("#id_provinsi").change(function() {
			var id_provinsi = $('#id_provinsi').val();
			$('#id_kabupaten').val('');

			$.ajax({
				url: urll + '/admin/master/user/getKabByProv',
				method: 'POST',
				data: {
					id_provinsi: id_provinsi
				},
				dataType: 'json',
				success: function(data) {

					var html = '';
					var i;
					html += '<option value="">- Pilih Kabupaten -</option>'
					for (i = 0; i < data.length; i++) {
						html += '<option value="' + data[i].id + '">' + data[i].name + '</option>';
					}

					$('#id_kabupaten').html(html);
				}
			});
			return false;
		});

		$("#id_kabupaten").change(function() {			
			var id_kabupaten = $('#id_kabupaten').val();
			$('#id_kecamatan').val('');

			$.ajax({
				url: urll + '/admin/master/user/getSubByDistrict',
				method: 'POST',
				data: {
					id_kabupaten: id_kabupaten
				},
				dataType: 'json',
				success: function(data) {

					var html = '';
					var i;
					html += '<option value="">- Pilih Kecamatan -</option>'
					for (i = 0; i < data.length; i++) {
						html += '<option value="' + data[i].id + '">' + data[i].name + '</option>';
					}

					$('#id_kecamatan').html(html);
				}
			});
			return false;
		});

		$("#id_kecamatan").change(function() {
			var id_kecamatan = $('#id_kecamatan').val();
			$('#id_kelurahan').val('');

			$.ajax({
				url: urll + '/admin/master/user/getVilBySub',
				method: 'POST',
				data: {
					id_kecamatan: id_kecamatan
				},
				dataType: 'json',
				success: function(data) {

					var html = '';
					var i;
					html += '<option value="">- Pilih Kelurahan -</option>'
					for (i = 0; i < data.length; i++) {
						html += '<option value="' + data[i].id + '">' + data[i].name + '</option>';
					}

					$('#id_kelurahan').html(html);
				}
			});
			return false;
		});
	});
</script>
<?php echo $this->endSection() ?> ?>