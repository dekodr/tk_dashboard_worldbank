<?php echo $this->extend('base/template'); ?>

<?php echo $this->section('content') ?>
<!-- Page Heading -->

<div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">KSM</h1>
</div>

<?php if (session()->getFlashdata('error')) { ?>
    <div class="card mb-4 py-3 border-left-danger">
        <div class="card-body">
            <?php print_r(session()->getFlashdata('error')) ?>
        </div>
    </div>
<?php } ?>

<!-- DataTales Example -->
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Form - Edit KSM </h6>
    </div>
    <div class="card-body row">
        <div class="col-xl-8 col-lg-7">
            <form method="POST" class="user" accept-charset="utf-8" enctype="multipart/form-data" action="<?php echo base_url('admin/master/ksm/save_update') ?>">
                <input name="id" type="hidden" class="form-control form-control-user" id="id" placeholder="Full Name" value="<?= $getKsm->id; ?>">
                <div class="form-group">
                    <label for="">Nama</label>
                    <input name="name" type="text" class="form-control form-control-user" id="name" placeholder="Full Name" value="<?= $getKsm->name; ?>" required>
                </div>
                <div class="form-group">
                    <label for="">Kode KSM</label>
                    <input name="kode_ksm" type="text" class="form-control form-control-user" id="kode_ksm" value="<?= $getKsm->kode_ksm; ?>" placeholder="Kode KSM" required>
                </div>
                
                <div class="form-group" id="provinsi">
                    <label for="">Provinsi</label>
                    <select class="form-control col-sm-12 rounded-pill" id="id_provinsi" name="id_provinsi">
                        <option value="">- Pilih Provinsi -</option>
                        <?php foreach ($province as $key => $values) : ?>
                            <option value="<?= $values->id; ?>" <?= ($getKsm->id_province == $values->id) ? 'selected' : '' ?>><?= $values->name; ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>

                <div class="form-group" id="kabupaten">
                    <label for="">Kabupaten</label>
                    <select class="form-control col-sm-12 rounded-pill" id="id_kabupaten" name="id_kabupaten">
                        <option value="">- Pilih Kabupaten -</option>
                        <?php foreach ($district as $key => $values) : ?>
                            <option value="<?= $values->id; ?>" <?= ($getKsm->id_kabupaten == $values->id) ? 'selected' : '' ?>><?= $values->name; ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>

                <div class="form-group" id="kecamatan">
                    <label for="">Kecamatan</label>
                    <select class="form-control col-sm-12 rounded-pill" id="id_kecamatan" name="id_kecamatan">
                        <option value="">- Pilih Kecamatan -</option>
                        <?php foreach ($sub as $key => $values) : ?>
                            <option value="<?= $values->id; ?>" <?= ($getKsm->id_kecamatan == $values->id) ? 'selected' : '' ?>><?= $values->name; ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>

                <div class="form-group" id="kelurahan">
                    <label for="">Kelurahan</label>
                    <select class="form-control col-sm-12 rounded-pill" id="id_kelurahan" name="id_kelurahan">
                        <option value="">- Pilih Kelurahan -</option>
                        <?php foreach ($kelurahan as $key => $values) : ?>
                            <option value="<?= $values->id; ?>" <?= ($getKsm->id_kelurahan == $values->id) ? 'selected' : '' ?>><?= $values->name; ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
                <hr>
                <button type="submit" class="btn btn-primary btn-icon-split">
                    <span class="icon text-white-50">
                        <i class="fas fa-save"></i>
                    </span>
                    <span class="text">Save Data</span>
                </button>
                <a href="<?php echo base_url('admin/master/ksm') ?>" class="btn btn-secondary">Kembali</a>
            </form>
        </div>
    </div>
</div>
<?php echo $this->endSection() ?> ?>
<?php echo $this->section('script') ?>
<script type="text/javascript">
    $(document).ready(function() {
        $("#id_provinsi").change(function() {
                var id_provinsi = $('#id_provinsi').val();
                $('#id_kabupaten').val('');

                $.ajax({
                    url: '/admin/master/ksm/getKabByProv',
                    method: 'POST',
                    data: {
                        id_provinsi: id_provinsi
                    },
                    dataType: 'json',
                    success: function(data) {

                        var html = '';
                        var i;
                        html += '<option value="">- Pilih Kabupaten -</option>'
                        for (i = 0; i < data.length; i++) {
                            html += '<option value="' + data[i].id + '">' + data[i].name + '</option>';
                        }

                        $('#id_kabupaten').html(html);
                    }
                });
                return false;
        });

        $("#id_kabupaten").change(function() {
                var id_kabupaten = $('#id_kabupaten').val();
                $('#id_kecamatan').val('');

                $.ajax({
                    url: '/admin/master/ksm/getSubByDistrict',
                    method: 'POST',
                    data: {
                        id_kabupaten: id_kabupaten
                    },
                    dataType: 'json',
                    success: function(data) {

                        var html = '';
                        var i;
                        html += '<option value="">- Pilih Kecamatan -</option>'
                        for (i = 0; i < data.length; i++) {
                            html += '<option value="' + data[i].id + '">' + data[i].name + '</option>';
                        }

                        $('#id_kecamatan').html(html);
                    }
                });
                return false;           
        });

        $("#id_kecamatan").change(function() {
            var id_kecamatan = $('#id_kecamatan').val();
            $('#id_kelurahan').val('');

            $.ajax({
                url: '/admin/master/ksm/getVilBySub',
                method: 'POST',
                data: {
                    id_kecamatan: id_kecamatan
                },
                dataType: 'json',
                success: function(data) {

                    var html = '';
                    var i;
                    html += '<option value="">- Pilih Kelurahan -</option>'
                    for (i = 0; i < data.length; i++) {
                        html += '<option value="' + data[i].id + '">' + data[i].name + '</option>';
                    }

                    $('#id_kelurahan').html(html);
                }
            });
            return false;
        });
    });
</script>
<?php echo $this->endSection() ?> ?>