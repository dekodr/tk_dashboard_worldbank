<?php echo $this->extend('base/template'); ?>

<?php echo $this->section('content') ?>
<!-- Page Heading -->

<div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">Role</h1>
</div>

<?php if (session()->getFlashdata('error')) { ?>
    <div class="card mb-4 py-3 border-left-danger">
        <div class="card-body">
            <?php print_r(session()->getFlashdata('error')) ?>
        </div>
    </div>
<?php } ?>

<!-- DataTales Example -->
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Form - Edit Role </h6>
    </div>
    <div class="card-body row">
        <div class="col-xl-8 col-lg-7">
            <form method="POST" class="user" accept-charset="utf-8" enctype="multipart/form-data" action="<?php echo base_url('admin/master/role/save_update') ?>">
                <input name="id" type="hidden" class="form-control form-control-user" id="id" placeholder="Full Name" value="<?= $getRole->id; ?>">
                <div class="form-group">
                    <label for="">Nama Role</label>
                    <input name="name_role" type="text" class="form-control form-control-user" id="name_role" value="<?= $getRole->name_role; ?>" placeholder="Nama Role" autocomplete="off" required>
                </div>
                <div class="form-group">
                    <label for="">Description</label>
                    <input name="description" type="text" class="form-control form-control-user" id="description" value="<?= $getRole->description; ?>" autocomplete="off" placeholder="Description">
                </div>
                <hr>
                <button type="submit" class="btn btn-primary btn-icon-split">
                    <span class="icon text-white-50">
                        <i class="fas fa-save"></i>
                    </span>
                    <span class="text">Save Data</span>
                </button>
                <a href="<?php echo base_url('admin/master/role') ?>" class="btn btn-secondary">Kembali</a>
            </form>
        </div>
    </div>
</div>
<?php echo $this->endSection() ?> ?>
<?php echo $this->section('script') ?>
<script type="text/javascript">
    $(document).ready(function() {});
</script>
<?php echo $this->endSection() ?> ?>