<?php echo $this->extend('base/template'); ?>

<?php echo $this->section('content') ?>
<!-- Page Heading -->

<div class="d-sm-flex align-items-center justify-content-between mb-4">
	<h1 class="h3 mb-0 text-gray-800">Fasilitator</h1>
</div>

<?php if (session()->getFlashdata('error')) { ?>
	<div class="card mb-4 py-3 border-left-danger">
		<div class="card-body">
			<?php print_r(session()->getFlashdata('error')) ?>
		</div>
	</div>
<?php } ?>

<!-- DataTales Example -->
<div class="card shadow mb-4">
	<div class="card-header py-3">
		<h6 class="m-0 font-weight-bold text-primary">Form - Create Fasilitator </h6>
	</div>
	<div class="card-body row">
		<div class="col-xl-8 col-lg-7">
			<form method="POST" class="user" accept-charset="utf-8" enctype="multipart/form-data" action="<?php echo base_url('admin/master/facilitator/save') ?>">
				<div class="form-group">
					<label for="">Nama</label>
					<input name="name" type="text" class="form-control form-control-user" id="name" placeholder="Full Name" value="" required>
				</div>
				<div class="form-group">
					<label for="">Kode Fasilitator</label>
					<input name="code" type="text" class="form-control form-control-user" id="code" value="" placeholder="Kode Fasilitator" required>
				</div>
				<div class="form-group" id="provinsi">
					<label for="">Provinsi</label>
					<select class="form-control col-sm-12 rounded-pill" id="id_provinsi" name="id_provinsi">
						<option value="">- Pilih Provinsi -</option>
						<?php foreach ($prov as $key => $values) : ?>
							<option value="<?= $values->id; ?>"><?= $values->name; ?></option>
						<?php endforeach; ?>
					</select>
				</div>
				<div class="form-group" id="kabupaten">
					<label for="">Kabupaten</label>
					<select class="form-control col-sm-12 rounded-pill" id="id_kabupaten" name="id_kabupaten">
						<option value="">- Pilih Kabupaten -</option>
						<?php foreach ($district as $key => $values) : ?>
							<option value="<?= $values->id; ?>"><?= $values->name; ?></option>
						<?php endforeach; ?>
					</select>
				</div>
				<label for="">KSM</label>
				<div class="form-group" id="id_ksm">
					<?php foreach ($ksm as $key => $value) { ?>
						<label for=""><?= $value->name ?></label>
						<input type="checkbox" name="id_ksm[]" value="<?= $value->id ?>" class="form-control form-control-user">
					<?php } ?>
				</div>
				<hr>
				<button type="submit" class="btn btn-primary btn-icon-split">
					<span class="icon text-white-50">
						<i class="fas fa-save"></i>
					</span>
					<span class="text">Save Data</span>
				</button>
				<a href="<?php echo base_url('admin/master/facilitator') ?>" class="btn btn-secondary">Kembali</a>
			</form>
		</div>
	</div>
</div>
<?php echo $this->endSection() ?> ?>
<?php echo $this->section('script') ?>
<script type="text/javascript">
	$(document).ready(function() {
		var urll = '<?php echo base_url() ?>';
		$("#id_provinsi").change(function() {
			var id_provinsi = $('#id_provinsi').val();
			$('#id_kabupaten').val('');

			$.ajax({
				url: urll + '/admin/master/facilitator/getKabByProv',
				method: 'POST',
				data: {
					id_provinsi: id_provinsi
				},
				dataType: 'json',
				success: function(data) {

					var html = '';
					var i;
					html += '<option value="">- Pilih Kabupaten -</option>'
					for (i = 0; i < data.length; i++) {
						html += '<option value="' + data[i].id + '">' + data[i].name + '</option>';
					}

					$('#id_kabupaten').html(html);
				}
			});
			return false;
		});

		$("#id_kabupaten").change(function() {
			var id_kabupaten = $('#id_kabupaten').val();
			$('#id_ksm').val('');

			$.ajax({
				url: urll + '/admin/master/facilitator/getKSMByKab',
				method: 'POST',
				data: {
					id_kabupaten: id_kabupaten
				},
				dataType: 'json',
				success: function(data) {
					// alert(id_kabupaten)
					var html = '';
					var i;
					for (i = 0; i < data.length; i++) {
						html += '<label for="">' + data[i].name + '</label>';
						html += '<input type="checkbox" name="id_ksm[]" value="' + data[i].id + '" class="form-control form-control-user">';
					}

					$('#id_ksm').html(html);
				}
			});
			return false;
		});
	});
</script>
<?php echo $this->endSection() ?> ?>