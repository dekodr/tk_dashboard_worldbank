<?php echo $this->extend('base/template'); ?>

<?php echo $this->section('content') ?>
<!-- Page Heading -->

<div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">Kabupaten</h1>
    <a href="<?php echo base_url('/admin/master/district/create') ?>" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i class="fas fa-plus fa-sm text-white-50"></i> Buat Kabupaten Baru</a>
</div>

<?php if (session()->getFlashdata('success')) { ?>
    <div class="alert alert-success">
        <div class="card-body">
            <?php print_r(session()->getFlashdata('success')) ?>
        </div>
    </div>
<?php } ?>
<!-- DataTales Example -->
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Datalist Kabupaten</h6>
    </div>
    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                    <tr>
                        <th>Name Province</th>
                        <th width="20%">Code</th>
                        <th>Name District</th>
                        <th width="20%">Action</th>
                    </tr>
                </thead>
                <!-- <tfoot>
                    <tr>
                        <th>Code</th>
                        <th>Name</th>
                        <th>Action</th>
                    </tr>
                </tfoot> -->
                <tbody>
                    <?php echo ($table); ?>

                </tbody>
            </table>
        </div>
    </div>
</div>

<?php echo $this->endSection() ?> ?>

<?php echo $this->section('script') ?>
<script>
    // Call the dataTables jQuery plugin
    $(document).ready(function() {
        $('#dataTable').DataTable();
    });
</script>
<?php echo $this->endSection() ?> ?>