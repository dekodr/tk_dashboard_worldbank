<?php echo $this->extend('base/template'); ?>

<?php echo $this->section('content') ?>
<?php
// print_r($this->session());die;
?>
<!-- Page Heading -->
<div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">Detail Grafik</h1>
    <!-- <a href="#" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i class="fas fa-download fa-sm text-white-50"></i> Generate Report</a> -->
</div>
<?php if (session()->getFlashdata('success')) { ?>
    <div class="card mb-4 py-3 border-left-success">
        <div class="card-body">
            <?php print_r(session()->getFlashdata('success')) ?>
        </div>
    </div>
<?php } ?>
<!-- Content Row -->
<div class="row">
    <!-- Pie Chart -->
    <?php if ($_SESSION['id_role'] != 2) : ?>
        <?php if ($_SESSION['id_role'] == 10 || $_SESSION['id_role'] == 11) { ?>
            <div class="col-xl-4 col-lg-5">
                <div class="card shadow mb-4">
                    <!-- Card Header - Dropdown -->
                    <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                        <h6 class="m-0 font-weight-bold text-primary">Laporan per Provinsi</h6>
                    </div>
                    <figure class="highcharts-figure">
                        <div id="provinsi-chart"></div>
                        <table id="datatable-provinsi" style="display: none;">
                            <thead>
                                <tr>
                                    <th></th>
                                    <th>Lap.Keuangan</th>
                                    <th>Lap. Infrastuktur</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($province as $key => $value) { ?>
                                    <tr>
                                        <th><?php echo $key ?> (<?php echo $value['keuangan'] + $value['infrastruktur']; ?>)</th>
                                        <td><?php echo $value['keuangan']; ?></td>
                                        <td><?php echo $value['infrastruktur']; ?></td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </figure>
                </div>
            </div>
        <?php } ?>
        <?php
        $arr = [5, 6, 7, 9, 10, 11];
        if ($_SESSION['id_role'] == in_array($_SESSION['id_role'], $arr)) { ?>
            <div class="col-xl-4 col-lg-5">
                <div class="card shadow mb-4">
                    <!-- Card Header - Dropdown -->
                    <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                        <h6 class="m-0 font-weight-bold text-primary">Laporan per Kabupaten</h6>
                    </div>
                    <figure class="highcharts-figure">
                        <div id="kabupaten-chart"></div>
                        <table id="datatable-kabupaten" style="display: none;">
                            <thead>
                                <tr>
                                    <th></th>
                                    <th>Lap.Keuangan</th>
                                    <th>Lap. Infrastuktur</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($kabupaten as $key => $value) { ?>
                                    <tr>
                                        <th><?php echo $key ?> (<?php echo $value['keuangan'] + $value['infrastruktur']; ?>)</th>
                                        <td><?php echo $value['keuangan']; ?></td>
                                        <td><?php echo $value['infrastruktur']; ?></td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </figure>
                </div>
            </div>
        <?php } ?>
        <div class="col-xl-4 col-lg-5">
            <div class="card shadow mb-4">
                <!-- Card Header - Dropdown -->
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                    <h6 class="m-0 font-weight-bold text-primary">Laporan per Kelurahan</h6>
                </div>
                <figure class="highcharts-figure">
                    <div id="container"></div>

                    <table id="datatable" style="display: none;">
                        <thead>
                            <tr>
                                <th></th>
                                <th>Lap.Keuangan</th>
                                <th>Lap. Infrastuktur</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($reportByType as $key => $value) { ?>
                                <tr>
                                    <th><?php echo $key ?> (<?php echo $value['keuangan'] + $value['infrastruktur']; ?>)</th>
                                    <td><?php echo $value['keuangan']; ?></td>
                                    <td><?php echo $value['infrastruktur']; ?></td>
                                </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </figure>
            </div>
        </div>
        <div class="col-xl-4 col-lg-7">
            <div class="card shadow mb-4">
                <div class="card-header py-3">
                    <h6 class="m-0 font-weight-bold text-primary">Laporan per KSM</h6>
                </div>
                <div class="card-body">
                    <div class="chart-pie pt-4 pb-2">
                        <canvas id="ksmChart"></canvas>
                    </div>
                </div>
            </div>
        </div>
    <?php endif ?>
</div>
<?php echo $this->endSection() ?> ?>
<?php echo $this->section('script'); ?>

<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/data.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="https://code.highcharts.com/modules/accessibility.js"></script>
<script>
    // Set new default font family and font color to mimic Bootstrap's default styling
    Chart.defaults.global.defaultFontFamily = 'Nunito', '-apple-system,system-ui,BlinkMacSystemFont,"Segoe UI",Roboto,"Helvetica Neue",Arial,sans-serif';
    Chart.defaults.global.defaultFontColor = '#858796';

    // Pie Chart KSM
    var ctx = document.getElementById("ksmChart");
    var ksm = <?= $ksm ?>;
    var ksmChart = new Chart(ctx, {
        type: 'pie',
        data: {
            labels: ksm.label,
            datasets: [{
                data: ksm.value,
                backgroundColor: ['#16a085', '#c0392b', '#2980b9', '#8e44ad', '#2c3e50', '#f39c12'],
                // hoverBackgroundColor: ['#2e59d9', '#17a673', '#2c9faf', '#36b9cc', '#36b9cc'],
                hoverBorderColor: "rgba(234, 236, 244, 1)",
            }],
        },
        options: {
            maintainAspectRatio: false,
            tooltips: {
                backgroundColor: "rgb(255,255,255)",
                bodyFontColor: "#858796",
                borderColor: '#dddfeb',
                borderWidth: 2,
                xPadding: 20,
                yPadding: 20,
                displayColors: true,
                caretPadding: 10,
            },
            legend: {
                display: true
            },
            cutoutPercentage: 60,
        },
    });

    Highcharts.chart('container', {
        data: {
            table: 'datatable'
        },
        chart: {
            type: 'column'
        },
        title: {
            text: 'Grafik Laporan'
        },
        yAxis: {
            allowDecimals: false,
            title: {
                text: 'Banyak Laporan'
            }
        },
        tooltip: {
            formatter: function() {
                return '<b>' + this.series.name + '</b><br/>' +
                    this.point.y + ' ' + this.point.name.toUpperCase();
            }
        }
    });

    Highcharts.chart('provinsi-chart', {
        data: {
            table: 'datatable-provinsi'
        },
        chart: {
            type: 'column'
        },
        title: {
            text: 'Grafik Laporan'
        },
        yAxis: {
            allowDecimals: false,
            title: {
                text: 'Banyak Laporan'
            }
        },
        tooltip: {
            formatter: function() {
                return '<b>' + this.series.name + '</b><br/>' +
                    this.point.y + ' ' + this.point.name.toUpperCase();
            }
        }
    });

    Highcharts.chart('kabupaten-chart', {
        data: {
            table: 'datatable-kabupaten'
        },
        chart: {
            type: 'column'
        },
        title: {
            text: 'Grafik Laporan'
        },
        yAxis: {
            allowDecimals: false,
            title: {
                text: 'Banyak Laporan'
            }
        },
        tooltip: {
            formatter: function() {
                return '<b>' + this.series.name + '</b><br/>' +
                    this.point.y + ' ' + this.point.name.toUpperCase();
            }
        }
    });
</script>
<?php echo $this->endSection() ?> ?>