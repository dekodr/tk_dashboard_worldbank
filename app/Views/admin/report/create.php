<?php echo $this->extend('base/template'); ?>

<?php echo $this->section('content') ?>
<!-- Page Heading -->

<div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">Report</h1>
</div>

<?php if (session()->getFlashdata('error')) { ?>
    <div class="card mb-4 py-3 border-left-danger">
        <div class="card-body">
            <?php print_r(session()->getFlashdata('error')) ?>
        </div>
    </div>
<?php } ?>

<!-- DataTales Example -->
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Form - Report <?= $ksm->name; ?></h6>
    </div>
    <div class="card-body row">
        <div class="col-xl-8 col-lg-7">
            <form method="POST" class="user" accept-charset="utf-8" enctype="multipart/form-data" action="<?php echo base_url('admin/report/reportCon/saveData') ?>">
                <div class="form-group row">
                    <div class="col-sm-6 mb-3 mb-sm-0">
                        <label for="">Pembuat Laporan</label>
                        <label><?= $_SESSION['name']; ?> - <?= $_SESSION['position']; ?></label>
                    </div>
                </div>
                <div class="form-group">
                    <label for="">Lampirkan Foto</label>
                    <p><input class="show-for-sr" type="file" id="upload_imgs" name="upload_imgs[]" accept="image/*" multiple required /></p>
                </div>
                <div class="form-group ">
                    <label for="exampleFormControlSelect1">Jenis Laporan</label>
                    <select class="form-control " id="jns_lap" name="jns_lap" required>
                        <option value="">- Pilih Jenis Laporan -</option>
                        <?php if ($_SESSION['position'] != 'Fasilitator Ekonomi') : ?>
                            <option value="infrastruktur">Laporan Infrastruktur</option>
                        <?php endif; ?>
                        <?php if ($_SESSION['position'] != 'Fasilitator Teknik') : ?>
                            <option value="keuangan">Laporan Man. Keuangan</option>
                        <?php endif; ?>
                    </select>
                </div>
                <div class="form-group ">
                    <label for="exampleFormControlSelect1">Judul</label>
                    <select class="form-control " id="jdl_lap" name="jdl_lap" required>
                        <option value="">- Pilih Judul -</option>
                    </select>
                </div>
                <div class="form-group " id="pkt_lap1">
                    <label>Paket Pekerjaan</label>
                    <select class="form-control" id="pkt_lap" name="pkt_lap">
                        <option value="">- Pilih Paket Pekerjaan -</option>
                        <?php foreach ($pkt_job as $pkt) : ?>
                            <option value="<?= $pkt->id; ?>"><?= $pkt->name; ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
                <div class="form-group" id="kegiatan">
                    <label for="">Kegiatan Pekerjaan</label>
                    <select class="form-control " id="kegiatanList" name="kegiatanList">
                        <option value="">- Pilih Kegiatan Pekerjaan -</option>
                    </select>
                </div>
                <div class="form-group" id="lokasidiv">
                    <label for="">Lokasi Pekerjaan</label>
                    <select class="form-control " id="lokasi" name="lokasi" required>
                        <option value="">- Pilih Lokasi Pekerjaan -</option>
                        <?php foreach ($loc as $loc) : ?>
                            <option value="<?= $loc->id; ?>"><?= $loc->name; ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
                <div class="form-group row" id="tgl1">
                    <div class="col-sm-6 mb-3 mb-sm-0">
                        <label for="">Tanggal Mulai</label>
                        <input class="form-control" type="date" value="" name="start_date" id="start_date" format="dd-mm-yyyy">
                    </div>
                    <div class="col-sm-6">
                        <label for="">Tanggal Selesai</label>
                        <input class="form-control" type="date" value="" name="end_date" id="end_date" format="dd-mm-yyyy">
                    </div>
                </div>
                <div class="form-group" id="lahan">
                    <label for="">Ketersediaan Hibah Lahan</label>
                    <select class="form-control " id="hibah" name="hibah">
                        <option value="">- Pilih Hibah Lahan -</option>
                        <option value="ya">YA</option>
                        <option value="tidak">TIDAK</option>
                    </select>
                </div>
                <div class="form-group " id="progressSlct">
                    <label for="">Progres Pekerjaan</label>
                    <select class="form-control " id="progress" name="progress">
                        <option value="">- Pilih Progres Pekerjaan -</option>
                        <option value="0">0</option>
                        <option value="25">25</option>
                        <option value="50">50</option>
                        <option value="75">75</option>
                        <option value="100">100</option>
                    </select>
                </div>
                <div class="form-group row" id="date_infra">
                    <div class="col-sm-6 mb-3 mb-sm-0">
                        <label for="">Tanggal Status Laporan</label>
                        <input class="form-control" type="date" value="" name="lap_date" id="lap_date" format="dd-mm-yyyy">
                    </div>
                </div>
                <div class="form-group row" id="date_keu">
                    <div class="col-sm-6 mb-3 mb-sm-0">
                        <label for="">Laporan Bulan</label>
                        <input class="form-control" type="month" value="" name="lap_date" id="lap_date" format="dd-mm-yyyy">
                    </div>
                </div>
                <div class="form-group">
                    <label for="" id="nmTerima" name="nmTerima"></label>
                    <input name="terima" class="form-control" id="terima" data-type="currency" placeholder="0.00">
                </div>
                <div class="form-group row" id="vol1">
                    <div class="col-sm-6">
                        <label for="">Volume Rencana</label>
                        <input name="rencana" class="form-control" id="rencana" placeholder="Vol. Rencana">
                    </div>
                    <div class="col-sm-6">
                        <label for="">Satuan</label>
                        <select class="form-control " id="satPlan" name="satPlan">
                            <option value="">- Satuan Volume -</option>
                            <option value="Meter">Meter</option>
                            <option value="Unit">Unit</option>
                        </select>
                    </div>
                </div>
                <div class="form-group row" id="vol2">
                    <div class="col-sm-6">
                        <label for="">Volume Realisasi</label>
                        <input name="volReal" class="form-control" id="volReal" placeholder="Vol. Realisasi">
                    </div>
                    <div class="col-sm-6">
                        <label for="">Satuan</label>
                        <label id="sat_real" readonly class="form-control-plaintext" name="sat_real"></label>
                    </div>
                </div>
                <div class="form-group">
                    <label for="">Deskripsi Laporan</label>
                    <textarea class="form-control" id="desc" name="desc" rows="3" placeholder="Deskripsi Laporan" required></textarea>
                </div>
                <div class="form-group" id="dok_name_input">
                    <label for="">Nama Dokumen</label>
                    <input type="text" class="form-control" id="dok_name_input" name="dok_name_input" placeholder="Nama Dokumen">
                </div>
                <div class="form-group " id="dok_name">
                    <label for="">Nama Dokumen</label>
                    <select class="form-control " id="php" name="dok_nameList" id="dok_nameList">
                        <option value="">- Pilih Nama Dokumen -</option>
                        <option value="Konsep Design">Konsep Design</option>
                        <option value="RAB">RAB</option>
                        <option value="Gambar Teknis">Gambar Teknis</option>
                        <option value="Surat Hibah">Surat Hibah</option>
                        <option value="SPPL">SPPL</option>
                    </select>
                </div>
                <div class="form-group">
                    <label for="">Upload Dokumen</label>
                    <p><input class="show-for-sr" type="file" id="upload_file[]" name="upload_file[]" multiple accept=".doc, .docx, application/msword, application/vnd.ms-excel, application/vnd.ms-powerpoint,
text/plain, application/pdf" required /><br>*ukuran file dibawah 20MB</p>

                </div>
                <hr>
                <button type="submit" class="btn btn-primary btn-icon-split">
                    <span class="icon text-white-50">
                        <i class="fas fa-save"></i>
                    </span>
                    <span class="text"> Simpan Data </span>
                </button>
                <a href="<?php echo base_url('admin/report') ?>" class="btn btn-secondary">Kembali</a>
            </form>
        </div>
    </div>
</div>
<?php echo $this->endSection() ?> ?>
<?php echo $this->section('script') ?>
<script type="text/javascript">
    $(document).ready(function() {
        $('#dok_name_input').hide();
        $('#date_keu').hide();
        $("#vol1").hide();
        $("#vol2").hide();

        // document.getElementById("rencana").disabled = true;
        // document.getElementById("satPlan").disabled = true;
        // document.getElementById("volReal").disabled = true;
        document.getElementById("terima").disabled = true;
        document.getElementById("start_date").disabled = true;
        document.getElementById("end_date").disabled = true;
        document.getElementById("pkt_lap").disabled = true;
        document.getElementById("progress").disabled = true;

        /* Hide Show Kegiatan */
        $('#jns_lap').change(function() {
            var jns_lap = $('#jns_lap').val();

            if (jns_lap == "keuangan") {
                var keu = '';

                // reset field
                $("#hibah").val("");
                $("#lokasi").val("");
                $("#kegiatanList").val("");
                $("#pkt_lap").val("");
                $("#progress").val("");

                $("#rencana").val("");
                $("#satPlan").val("");
                $("#volReal").val("");
                $('#sat_real').text("");
                $('#nmTerima').text("Saldo Terakhir (Rp)");

                // hide input
                $('#vol2').hide();
                $('#vol1').hide();
                $('#progressSlct').hide();
                $('#tgl1').hide();
                $('#pkt_lap1').hide();
                $('#lahan').hide();
                $('#lokasidiv').hide();
                $('#kegiatan').hide();
                $('#dok_name').hide();
                $('#date_infra').hide();
                $('#dok_name_input').show();
                $('#date_keu').show();

                $("#pkt_lap").removeAttr("required");
                $("#start_date").removeAttr("required");
                $("#end_date").removeAttr("required");
                $("#dok_nameList").removeAttr("required");
                $("#hibah").removeAttr("required");
                $("#lokasi").removeAttr("required");
                $("#kegiatan").removeAttr("required");
                $("#terima").removeAttr("required");

                // list judu;
                keu += '<option value="">- Pilih Judul -</option>'
                keu += '<option value="kas">Buku Kas</option>'
                keu += '<option value="bank">Buku Bank</option>'
                keu += '<option value="dkis">Buku DKIS</option>'
                keu += '<option value="rekening_bank">Buku Rekening Bank KSM</option>'
                keu += '<option value="lppu">LPPU</option>'

                $('#jdl_lap').html(keu);
            }

            if (jns_lap == "infrastruktur" || jns_lap == "") {
                var infra = '';

                // reset field
                $("#progress").val("");

                // enable input
                document.getElementById("pkt_lap").disabled = false;
                document.getElementById("start_date").disabled = false;
                document.getElementById("end_date").disabled = false;
                document.getElementById("progress").disabled = false;

                // required input
                $("#pkt_lap").attr("required", "");
                $("#start_date").attr("required", "");
                $("#end_date").attr("required", "");
                $("#dok_nameList").attr("required", "");
                $("#hibah").attr("required", "");
                $("#lokasi").attr("required", "");
                $("#kegiatan").attr("required", "");
                $("#terima").attr("required", "");

                // show input
                $('#vol2').hide();
                $('#vol1').hide();
                $('#progressSlct').show();
                $('#tgl1').show();
                $('#pkt_lap1').show();
                $('#lahan').show();
                $('#lokasidiv').show();
                $('#kegiatan').show();
                $('#dok_name').show();
                $('#dok_name_input').hide();
                $('#date_infra').show();
                $('#date_keu').hide();
                $('#nmTerima').text("Penerimaan Rekening Bank KSM (Rp)");

                infra += '<option value="">- Pilih Judul -</option>'
                infra += '<option value="foto_progress">Umum / Harian</option>'
                infra += '<option value="kemajuan_fisik">Kemajuan Fisik</option>'
                $('#jdl_lap').html(infra);
            }
        });
        /* Hide Show Kegiatan */

        /* Cek Judul */
        $('#jdl_lap').change(function() {
            var jdl_lap = $('#jdl_lap').val();
            var jns_lap = $('#jns_lap').val();

            if (jdl_lap == 'kas' || jdl_lap == 'bank' || jns_lap != 'keuangan') {
                document.getElementById("terima").disabled = false;

                $("#terima").attr("required", "");
            } else {
                $("#terima").val("");
                document.getElementById("terima").disabled = true;
            }
        });
        /* Cek Judul */

        /* satPlan */
        $("#satPlan").change(function() {
            var satPlan = $('#satPlan').val();
            $('#sat_real').text(satPlan);
        });
        /* satPlan */

        /* Progress */
        $("#progress").change(function() {
            var prog = $('#progress').val();
            var jns_lap = $('#jns_lap').val();

            if (prog == '100' && jns_lap != 'keuangan') {
                // document.getElementById("rencana").disabled = false;
                // document.getElementById("satPlan").disabled = false;
                // document.getElementById("volReal").disabled = false;

                $("#vol1").show();
                $("#vol2").show();

                $("#rencana").attr("required", "");
                $("#satPlan").attr("required", "");
                $("#volReal").attr("required", "");
            } else if (prog != '100' && jns_lap != 'keuangan') {
                $("#rencana").removeAttr("required", "");
                $("#satPlan").removeAttr("required", "");
                $("#volReal").removeAttr("required", "");
                $("#vol1").hide();
                $("#vol2").hide();
            }
        });
        /* progress */

        /* Kegiatan */
        $("#pkt_lap").change(function() {
            var pkt_lap = $('#pkt_lap').val();
            var urll = '<?php echo base_url() ?>';

            $.ajax({
                url: urll + '/admin/report/reportCon/getSubProjectById',
                method: 'POST',
                data: {
                    pkt_lap: pkt_lap
                },
                dataType: 'json',
                success: function(data) {

                    var html = '';
                    var i;
                    html += '<option value="">- Pilih Kegiatan -</option>'
                    for (i = 0; i < data.length; i++) {
                        html += '<option value="' + data[i].id + '">' + data[i].name + '</option>';
                    }

                    $('#kegiatanList').html(html);
                }
            });
            return false;
        });
        /* Kegiatan */

        /* Currency */
        $("input[data-type='currency']").on({
            keyup: function() {
                formatCurrency($(this));
            },
            blur: function() {
                formatCurrency($(this), "blur");
            }
        });

        function formatNumber(n) {
            // format number 1000000 to 1,234,567
            return n.replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ",")
        }

        function formatCurrency(input, blur) {
            // appends $ to value, validates decimal side
            // and puts cursor back in right position.

            // get input value
            var input_val = input.val();

            // don't validate empty input
            if (input_val === "") {
                return;
            }

            // original length
            var original_len = input_val.length;

            // initial caret position 
            var caret_pos = input.prop("selectionStart");

            // check for decimal
            if (input_val.indexOf(".") >= 0) {

                // get position of first decimal
                // this prevents multiple decimals from
                // being entered
                var decimal_pos = input_val.indexOf(".");

                // split number by decimal point
                var left_side = input_val.substring(0, decimal_pos);
                var right_side = input_val.substring(decimal_pos);

                // add commas to left side of number
                left_side = formatNumber(left_side);

                // validate right side
                right_side = formatNumber(right_side);

                // On blur make sure 2 numbers after decimal
                if (blur === "blur") {
                    right_side += "00";
                }

                // Limit decimal to only 2 digits
                right_side = right_side.substring(0, 2);

                // join number by .
                input_val = "" + left_side + "." + right_side;

            } else {
                // no decimal entered
                // add commas to number
                // remove all non-digits
                input_val = formatNumber(input_val);
                input_val = "" + input_val;

                // final formatting
                if (blur === "blur") {
                    input_val += ".00";
                }
            }

            // send updated string to input
            input.val(input_val);

            // put caret back in the right position
            var updated_len = input_val.length;
            caret_pos = updated_len - original_len + caret_pos;
            input[0].setSelectionRange(caret_pos, caret_pos);
        }

        /* Currency */
    });
</script>
<?php echo $this->endSection() ?>