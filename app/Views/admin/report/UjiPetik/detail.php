<?php echo $this->extend('base/template'); ?>

<?php echo $this->section('content') ?>
<!-- Page Heading -->

<div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">Detail Uji Petik</h1>
</div>

<?php if (session()->getFlashdata('error')) { ?>
    <div class="card mb-4 py-3 border-left-danger">
        <div class="card-body">
            <?php print_r(session()->getFlashdata('error')) ?>
        </div>
    </div>
<?php } ?>

<!-- DataTales Example -->
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Form - Detail Ujit Petik </h6>
    </div>
    <div class="card-body row">
        <div class="col-xl-8 col-lg-7">
            <form method="POST" class="user" accept-charset="utf-8" enctype="multipart/form-data" action="<?php echo base_url('admin/report/UjiPetik/approve') ?>">
                <input name="id" type="hidden" class="form-control form-control-user" id="id" value="<?= $getDetail->id; ?>">
                <div class="form-group">
                    <b><label for="">Jenis Kegiatan :</label></b>
                    <input type="text" readonly class="form-control-plaintext" id="staticEmail" value="<?= ($getDetail->jenis_kegiatan == 'Uji Petik' ? 'Uji Petik' : ($getDetail->jenis_kegiatan == 'Misi Dukungan Implementasi' ? 'Misi Dukungan Implementasi' : ($getDetail->jenis_kegiatan == 'Misi Teknis' ? 'Misi Teknis' : ''))); ?>">
                </div>
                <div class="form-group">
                    <b><label for="">Tingkat Kegiatan :</label></b>
                    <input type="text" readonly class="form-control-plaintext" id="staticEmail" value="<?= ($getDetail->tingkat_kegiatan == 'Nasional' ? 'Nasional' : ($getDetail->tingkat_kegiatan == 'KMP' ? 'KMP' : ($getDetail->tingkat_kegiatan == 'OSP' ? 'OSP' : ($getDetail->tingkat_kegiatan == 'KorKot' ? 'KorKot' : '')))); ?>">
                </div>
                <div class="form-group">
                    <b><label for="">Tanggal Mulai :</label></b>
                    <input type="text" readonly class="form-control-plaintext" id="staticEmail" value="<?= $getDetail->start_date; ?>">
                </div>
                <div class="form-group">
                    <b><label for="">Tanggal Selesai :</label></b>
                    <input type="text" readonly class="form-control-plaintext" id="staticEmail" value="<?= $getDetail->end_date; ?>">
                </div>
                <div class="form-group">
                    <b><label for="">Tema :</label></b>
                    <input type="text" readonly class="form-control-plaintext" id="staticEmail" value="<?= ($getDetail->tema == 'Semua' ? 'Semua' : ($getDetail->tema == 'Monev' ? 'Monev' : ($getDetail->tema == 'MIS' ? 'MIS' : ($getDetail->tema == 'Manajemen Keuangan' ? 'Manajemen Keuangan' : ($getDetail->tema == 'Infrastuktur' ? 'Infrastuktur' : ($getDetail->tema == 'Safeguard' ? 'Safeguard' : ($getDetail->tema == 'PIM' ? 'PIM' : ($getDetail->tema == 'Pelatihan' ? 'Pelatihan' : ($getDetail->tema == 'Sosialisasi' ? 'Sosialisasi' : ''))))))))); ?>">
                </div>
                <div class="form-group">
                    <b><label for="">Fokus Kegiatan :</label></b>
                    <input type="text" readonly class="form-control-plaintext" id="staticEmail" value="<?= $getDetail->fokus_kegiatan; ?>">
                </div>
                <div class="form-group">
                    <b><label for="">Lokasi Kegiatan :</label></b>
                    <input type="text" readonly class="form-control-plaintext" id="staticEmail" value="<?= $getDetail->lok_provinsi; ?>">
                </div>
                <div class="form-group">
                    <input type="text" readonly class="form-control-plaintext" id="staticEmail" value="<?= $getDetail->lok_kota; ?>">
                </div>
                <div class="form-group">
                    <input type="text" readonly class="form-control-plaintext" id="staticEmail" value="<?= $getDetail->lok_kecamatan; ?>">
                </div>
                <div class="form-group">
                    <input type="text" readonly class="form-control-plaintext" id="staticEmail" value="<?= $getDetail->lok_kelurahan; ?>">
                </div>
                <div class="form-group">
                    <b><label for="">Asal PIC :</label></b>
                    <input type="text" readonly class="form-control-plaintext" id="staticEmail" value="<?= $getDetail->pic_provinsi; ?>">
                </div>
                <div class="form-group">
                    <input type="text" readonly class="form-control-plaintext" id="staticEmail" value="<?= $getDetail->pic_kota; ?>">
                </div>
                <div class="form-group">
                    <input type="text" readonly class="form-control-plaintext" id="staticEmail" value="<?= $getDetail->pic_kelurahan; ?>">
                </div>
                <div class="form-group">
                    <input type="text" readonly class="form-control-plaintext" id="staticEmail" value="<?= $getDetail->pic_kecamatan; ?>">
                </div>
                <div class="form-group">
                    <b><label for="">Nama PIC :</label></b>
                    <input type="text" readonly class="form-control-plaintext" id="staticEmail" value="<?= $getDetail->nama_pic; ?>">
                </div>
                <div class="form-group">
                    <b><label for="">Topik Pendanaan :</label></b>
                    <input type="text" readonly class="form-control-plaintext" id="staticEmail" value="<?= ($getDetail->topik_pendanaan == 'skala_kawasan' ? 'Skala Kawasan' : ($getDetail->topik_pendanaan == 'skala_lingkungan' ? 'Skala Lingkungan' : ($getDetail->topik_pendanaan == 'kolaborasi' ? 'Kolaborasi' : ($getDetail->topik_pendanaan == 'padat_karya' ? 'Padat Karya' : ($getDetail->topik_pendanaan == 'bpm' ? 'BPM' : ''))))); ?>">
                </div>
                <div class="form-group">
                    <label for="">Lampiran File</label> <br>
                    <?php if ($file != null) : ?>
                        <?php foreach ($file as $key => $val) : ?>
                            <a href="<?= $val; ?>" class="btn btn-success " target="_blank">Document - <?= $key + 1; ?></a>
                        <?php endforeach; ?>
                    <?php else : ?>
                        No File Uploaded
                    <?php endif; ?>
                </div>
                <div class="form-group">
                    <b><label for="">Kategori Dokumen :</label></b>
                    <input type="text" readonly class="form-control-plaintext" id="staticEmail" value="<?= $getDetail->kategori_dokumen; ?>">
                </div>
                <div class="form-group">
                    <b><label for="">Catatan Hasil :</label></b>
                    <input type="text" readonly class="form-control-plaintext" id="staticEmail" value="<?= $getDetail->catatan_hasil; ?>">
                </div>
                <div class="form-group">
                    <b><label for="">Kesimpulan :</label></b>
                    <input type="text" readonly class="form-control-plaintext" id="staticEmail" value="<?= $getDetail->kesimpulan; ?>">
                </div>
                <div class="form-group">
                    <b><label for="">Rekomendasi :</label></b>
                    <input type="text" readonly class="form-control-plaintext" id="staticEmail" value="<?= $getDetail->rekomendasi; ?>">
                </div>
                <div class="form-group" id="revisiDiv">
                    <label for="">Catatan Revisi :</label> <br>
                    <textarea class="form-control" id="revisi" name="revisi" rows="3" placeholder="Catatan Revisi"></textarea>
                </div>
                <hr>
                <?php if ($getDetail->is_reject == 1) { ?>
                    <!-- <table class="table">
                        <thead class="thead-dark">
                            <tr>
                                <td colspan="5" align="center">Laporan Harus Di Revisi</td>
                            </tr>
                        </thead>
                    </table> -->
                <?php } ?>
                <?php if ($getDetail->is_approved == 1) { ?>
                    <!-- <table class="table">
                        <thead class="thead-dark">
                            <tr>
                                <td colspan="5" align="center">Laporan Sudah Di Setujui</td>
                            </tr>
                        </thead>
                    </table> -->
                <?php } ?>
                <?php if ((($id_role == 3 && $getDetail->id_role == 2) || ($id_role == 9 && $getDetail->id_role == 3) || ($id_role == 10 && $getDetail->id_role == 9)) && ($getDetail->is_approved == 0 && $getDetail->is_reject == 0)) { ?>
                    <!-- <button class="btn btn-primary">Setujui</button>
                    <a class="btn btn-danger" onclick="input_revisi()">Revisi</a> -->
                <?php } ?>
                <a href="<?php echo base_url('/uji-petik') ?>" class="btn btn-secondary">Kembali</a>
            </form>
        </div>
    </div>
</div>
<?php echo $this->endSection() ?> ?>
<?php echo $this->section('script') ?>
<script type="text/javascript">
    $(document).ready(function() {});
    $('#revisiDiv').hide();

    function input_revisi() {
        var rev = $('#revisi').val();

        console.log(rev);

        if (rev == '') {
            var i = confirm("Mohon isi catatan reivisi! ");
            if (i == true) {
                $('#revisiDiv').show();
            }
        } else {
            var j = confirm("Laporan akan di reivisi ?");
            var urll = '<?php echo base_url() ?>';
            if (j == true) {
                window.location = urll + '/admin/report/ujipetik/reject/' + <?= $getDetail->id; ?> + "/" + rev;
            }
        }
    }
</script>
<?php echo $this->endSection() ?> ?>