<?php echo $this->extend('base/template'); ?>

<?php echo $this->section('content') ?>
<!-- Page Heading -->

<div class="d-sm-flex align-items-center justify-content-between mb-4">
	<h1 class="h3 mb-0 text-gray-800">Kegiatan Supervisi</h1>
</div>

<?php if (session()->getFlashdata('error')) { ?>
	<div class="card mb-4 py-3 border-left-danger">
		<div class="card-body">
			<?php print_r(session()->getFlashdata('error')) ?>
		</div>
	</div>
<?php } ?>

<!-- DataTales Example -->
<div class="card shadow mb-4">
	<div class="card-header py-3">
		<h6 class="m-0 font-weight-bold text-primary">Form - Buat Kegiatan Baru </h6>
	</div>
	<div class="card-body row">
		<div class="col-xl-8 col-lg-7">
			<form method="POST" class="user" accept-charset="utf-8" enctype="multipart/form-data" action="<?php echo base_url('admin/report/UjiPetik/save') ?>">
				<div class="form-group" id="jns_kegiatan">
					<label for="">Jenis Kegiatan</label>
					<select class="form-control " id="jenis_kegiatan" name="jenis_kegiatan">
						<option value="">- Pilih Jenis Kegiatan -</option>
						<option value="Uji Petik">Uji Petik</option>
						<option value="Misi Dukungan Implementasi">Misi Dukungan Implementasi</option>
						<option value="Misi Teknis">Misi Teknis</option>
						<option value="Lainnya">Lainnya</option>
					</select>
				</div>
				<div class="form-group" id="tngkt_kegiatan">
					<label for="">Tingkat Kegiatan</label>
					<select class="form-control " id="tingkat_kegiatan" name="tingkat_kegiatan">
						<option value="">- Pilih Tingkat Kegiatan -</option>
						<option value="Nasional">Nasional</option>
						<option value="KMP">KMP</option>
						<option value="OSP">OSP</option>
						<option value="KorKot">KorKot</option>
					</select>
				</div>
				<div class="form-group row" id="tgl1">
					<div class="col-sm-6 mb-3 mb-sm-0">
						<label for="">Tanggal Mulai</label>
						<input class="form-control" type="date" value="" name="start_date" id="start_date" format="dd-mm-yyyy">
					</div>
					<div class="col-sm-6">
						<label for="">Tanggal Selesai</label>
						<input class="form-control" type="date" value="" name="end_date" id="end_date" format="dd-mm-yyyy">
					</div>
				</div>
				<div class="form-group" id="tema1">
					<label for="">Tema</label>
					<select class="form-control " id="tema" name="tema">
						<option value="">- Pilih Tema -</option>
						<option value="Semua">Semua</option>
						<option value="Monev">Monev</option>
						<option value="MIS">MIS</option>
						<option value="Manajemen Keuangan">Manajemen Keuangan</option>
						<option value="Infrastuktur">Infrastuktur</option>
						<option value="Safeguard">Safeguard</option>
						<option value="PIM">PIM</option>
						<option value="Pelatihan">Pelatihan</option>
						<option value="Sosialisasi">Sosialisasi</option>
					</select>
				</div>
				<div class="form-group">
					<label for="">Fokus Kegiatan</label>
					<textarea id="fokus_kegiatan" name="fokus_kegiatan" type="text" class="form-control" value="" autocomplete="off">
					</textarea>
				</div>
				<div class="form-group" id="provinsi">
					<label for="">Lokasi Kegiatan</label>
					<select class="form-control" id="id_provinsi" name="id_provinsi">
						<option value="">- Pilih Provinsi -</option>
						<?php foreach ($province as $key => $values) { ?>
							<option value="<?= $values->id; ?>"><?= $values->name; ?></option>
						<?php } ?>
					</select>
				</div>

				<div class="form-group" id="kabupaten">
					<select class="form-control" id="id_kabupaten" name="id_kabupaten">
						<option value="">- Pilih Kabupaten -</option>
						<?php foreach ($district as $key => $values) : ?>
							<option value="<?= $values->id; ?>"><?= $values->name; ?></option>
						<?php endforeach; ?>
					</select>
				</div>

				<!-- <div class="form-group" id="kecamatan">
					<select class="form-control" id="id_kecamatan" name="id_kecamatan">
						<option value="">- Pilih Kecamatan -</option>
						<?php foreach ($sub as $key => $values) : ?>
							<option value="<?= $values->id; ?>"><?= $values->name; ?></option>
						<?php endforeach; ?>
					</select>
				</div> -->

				<div class="form-group" id="kelurahan">
					<select class="form-control" id="id_kelurahan" name="id_kelurahan">
						<option value="">- Pilih Kelurahan -</option>
						<?php foreach ($village as $key => $values) : ?>
							<option value="<?= $values->id; ?>"><?= $values->name; ?></option>
						<?php endforeach; ?>
					</select>
				</div>

				<div class="form-group" id="provinsi_pic">
					<label for="">Asal PIC</label>
					<select class="form-control" id="id_provinsi_pic" name="tingkat_pic">
						<option value="">- Pilih Tingkat -</option>
						<option value="kmp">KMP</option>
						<option value="osp">OSP</option>
						<option value="korkot">Korkot</option>
						<option value="fasilitator">Tim Fasilitator</option>
						<?php foreach ($province as $key => $values) { ?>
							<!-- <option value="<?= $values->id; ?>"><?= $values->name; ?></option> -->
						<?php } ?>
					</select>
				</div>

				<div class="form-group" id="kabupaten_pic">
					<select class="form-control" id="id_kabupaten_pic" name="osp_pic">
						<option value="">- Pilih OSP -</option>
						<?php for ($i = 1; $i <= 11; $i++) { ?>
							<option value="OSP<?= $i; ?>">OSP <?= $i; ?></option>
						<?php } ?>
						<?php foreach ($district as $key => $values) : ?>
							<!-- <option value="<?= $values->id; ?>"><?= $values->name; ?></option> -->
						<?php endforeach; ?>
					</select>
				</div>

				<div class="form-group" id="kecamatan_pic">
					<select class="form-control" id="id_kecamatan_pic" name="korkot_pic">
						<option value="">- Pilih Korkot -</option>
						<?php for ($i = 1; $i <= 11; $i++) { ?>
							<option value="Korkot<?= $i; ?>">Korkot <?= $i; ?></option>
						<?php } ?>
						<?php foreach ($sub as $key => $values) : ?>
							<!-- <option value="<?= $values->id; ?>"><?= $values->name; ?></option> -->
						<?php endforeach; ?>
					</select>
				</div>

				<!-- <div class="form-group" id="kelurahan_pic">
					<select class="form-control" id="id_kelurahan_pic" name="id_kelurahan_pic">
						<option value="">- Pilih Kelurahan -</option>
						<?php foreach ($village as $key => $values) : ?>
							<option value="<?= $values->id; ?>"><?= $values->name; ?></option>
						<?php endforeach; ?>
					</select>
				</div> -->
				<div class="form-group">
					<input name="nama_pic" type="text" class="form-control" value="" placeholder="Nama PIC" autocomplete="off">
				</div>
				<label for="">Topik Pendanaan</label>
				<div class="form-group" id="topik_pendanaan">
					<label for="">Skala Kawasan</label>
					<input type="checkbox" name="topik_pendanaan[]" value="skala_kawasan" class="form-control form-control-user">
					<label for="">Skala Lingkungan</label>
					<input type="checkbox" name="topik_pendanaan[]" value="skala_lingkungan" class="form-control form-control-user">
					<label for="">Kolaborasi</label>
					<input type="checkbox" name="topik_pendanaan[]" value="kolaborasi" class="form-control form-control-user">
					<label for="">Padat Karya</label>
					<input type="checkbox" name="topik_pendanaan[]" value="padat_karya" class="form-control form-control-user">
					<label for="">BPM</label>
					<input type="checkbox" name="topik_pendanaan[]" value="bpm" class="form-control form-control-user">
				</div>
				<div class="form-group">
					<label for="">Upload Dokumen</label>
					<p><input class="show-for-sr" type="file" id="upload_dokumen[]" name="upload_dokumen[]" multiple accept=".doc, .docx, application/msword, application/vnd.ms-excel, application/vnd.ms-powerpoint,
text/plain, application/pdf" required /><br>*ukuran file dibawah 20MB</p>

				</div>
				<div class="form-group">
					<label for="">Kategori Dokumen</label>
					<input name="kategori_dokumen" type="text" class="form-control" value="" autocomplete="off">
				</div>
				<div class="form-group">
					<label for="">Catatan Hasil</label>
					<input name="catatan_hasil" type="text" class="form-control" value="" autocomplete="off">
				</div>
				<div class="form-group">
					<label for="">Kesimpulan</label>
					<input name="kesimpulan" type="text" class="form-control" value="" autocomplete="off">
				</div>
				<div class="form-group">
					<label for="">Rekomendasi</label>
					<input name="rekomendasi" type="text" class="form-control" value="" autocomplete="off">
				</div>
				<hr>
				<button type="submit" class="btn btn-primary btn-icon-split">
					<span class="icon text-white-50">
						<i class="fas fa-save"></i>
					</span>
					<span class="text">Save Data</span>
				</button>
				<a href="<?php echo base_url('uji-petik') ?>" class="btn btn-secondary">Kembali</a>
			</form>
		</div>
	</div>
</div>
<?php echo $this->endSection() ?> ?>
<?php echo $this->section('script') ?>
<script type="text/javascript">
	tinymce.init({
		selector: 'textarea#fokus_kegiatan',
		height: 300,
		menubar: false,
		plugins: [
			'advlist autolink lists link image charmap print preview anchor',
			'searchreplace visualblocks code fullscreen',
			'insertdatetime media table paste code help wordcount'
		],
		toolbar: 'undo redo | formatselect | ' +
			'bold italic backcolor | alignleft aligncenter ' +
			'alignright alignjustify | bullist numlist outdent indent | ' +
			'removeformat | help',
		content_style: 'body { font-family:Helvetica,Arial,sans-serif; font-size:14px }'
	});


	$(document).ready(function() {
		var urll = '<?php echo base_url() ?>';
		$("#id_provinsi").change(function() {
			var id_provinsi = $('#id_provinsi').val();
			$('#id_kabupaten').val('');

			$.ajax({
				url: urll + '/admin/report/UjiPetik/getKabByProv',
				method: 'POST',
				data: {
					id_provinsi: id_provinsi
				},
				dataType: 'json',
				success: function(data) {

					var html = '';
					var i;
					html += '<option value="">- Pilih Kabupaten -</option>'
					for (i = 0; i < data.length; i++) {
						html += '<option value="' + data[i].id + '">' + data[i].name + '</option>';
					}

					$('#id_kabupaten').html(html);
				}
			});
			return false;
		});

		$("#id_kabupaten").change(function() {
			var id_kabupaten = $('#id_kabupaten').val();

			$.ajax({
				url: urll + '/admin/report/UjiPetik/getSubByDistrict',
				method: 'POST',
				data: {
					id_kabupaten: id_kabupaten
				},
				dataType: 'json',
				success: function(data) {

					var html = '';
					var i;
					html += '<option value="">- Pilih Kelurahan -</option>'
					for (i = 0; i < data.length; i++) {
						html += '<option value="' + data[i].id + '">' + data[i].name + '</option>';
					}

					$('#id_kelurahan').html(html);
				}
			});
			return false;
		});

		$("#id_kecamatan").change(function() {
			var id_kecamatan = $('#id_kecamatan').val();
			$('#id_kelurahan').val('');

			$.ajax({
				url: urll + '/admin/report/UjiPetik/getVilBySub',
				method: 'POST',
				data: {
					id_kecamatan: id_kecamatan
				},
				dataType: 'json',
				success: function(data) {

					var html = '';
					var i;
					html += '<option value="">- Pilih Kelurahan -</option>'
					for (i = 0; i < data.length; i++) {
						html += '<option value="' + data[i].id + '">' + data[i].name + '</option>';
					}

					$('#id_kelurahan').html(html);
				}
			});
			return false;
		});

		// $("#id_provinsi_pic").change(function() {
		// 	var id_provinsi = $('#id_provinsi_pic').val();
		// 	$('#id_kabupaten_pic').val('');

		// 	$.ajax({
		// 		url: urll + '/admin/report/UjiPetik/getKabByProv',
		// 		method: 'POST',
		// 		data: {
		// 			id_provinsi: id_provinsi
		// 		},
		// 		dataType: 'json',
		// 		success: function(data) {

		// 			var html = '';
		// 			var i;
		// 			html += '<option value="">- Pilih Kabupaten -</option>'
		// 			for (i = 0; i < data.length; i++) {
		// 				html += '<option value="' + data[i].id + '">' + data[i].name + '</option>';
		// 			}

		// 			$('#id_kabupaten_pic').html(html);
		// 		}
		// 	});
		// 	return false;
		// });
		// $("#id_kabupaten_pic").change(function() {
		// 	var id_kabupaten = $('#id_kabupaten_pic').val();
		// 	$('#id_kecamatan_pic').val('');

		// 	$.ajax({
		// 		url: urll + '/admin/report/UjiPetik/getSubByDistrict',
		// 		method: 'POST',
		// 		data: {
		// 			id_kabupaten: id_kabupaten
		// 		},
		// 		dataType: 'json',
		// 		success: function(data) {

		// 			var html = '';
		// 			var i;
		// 			html += '<option value="">- Pilih Kecamatan -</option>'
		// 			for (i = 0; i < data.length; i++) {
		// 				html += '<option value="' + data[i].id + '">' + data[i].name + '</option>';
		// 			}

		// 			$('#id_kecamatan_pic').html(html);
		// 		}
		// 	});
		// 	return false;
		// });
		// $("#id_kecamatan_pic").change(function() {
		// 	var id_kecamatan = $('#id_kecamatan_pic').val();
		// 	$('#id_kelurahan_pic').val('');

		// 	$.ajax({
		// 		url: urll + '/admin/report/UjiPetik/getVilBySub',
		// 		method: 'POST',
		// 		data: {
		// 			id_kecamatan: id_kecamatan
		// 		},
		// 		dataType: 'json',
		// 		success: function(data) {

		// 			var html = '';
		// 			var i;
		// 			html += '<option value="">- Pilih Kelurahan -</option>'
		// 			for (i = 0; i < data.length; i++) {
		// 				html += '<option value="' + data[i].id + '">' + data[i].name + '</option>';
		// 			}

		// 			$('#id_kelurahan_pic').html(html);
		// 		}
		// 	});
		// 	return false;
		// });
	});
</script>
<?php echo $this->endSection() ?> ?>