<?php echo $this->extend('base/template'); ?>

<?php echo $this->section('content') ?>
<?php
// print_r($this->session());die;
?>
<!-- Page Heading -->
<div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">Dashboard Kegiatan Supervisi</h1>
    <!-- <a href="#" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i class="fas fa-download fa-sm text-white-50"></i> Generate Report</a> -->
</div>
<!-- Content Row -->
<a href="<?= base_url('uji-petik'); ?>" class="btn btn-primary btn-icon-split mt-3 mb-3" style="height: 40px;">
    <span class="icon text-white-50">
        <i class="fas fa-eye"></i>
    </span>
    <span class="text" style="color: white;">Datalist Kegiatan Supervisi</span>
</a> <br>
<div class="row">
    <!-- Earnings (Monthly) Card Example -->
    <div class="col-lg-8 mb-4">
        <!-- Project Card Example -->
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">
                    Laporan Jenis Kegiatan
                    <select name="tingkat" id="">
                        <option value="semua">Semua</option>
                        <?php
                        $provinsi = json_decode($jumlah_provinsi);
                        foreach ($provinsi as $key => $value) { ?>
                            <option value="<?= $value->name ?>"><?= $value->name; ?></option>
                        <?php } ?>
                    </select>
                </h6>
            </div>

            <div class="card-body" id="showJenisKegiatan">
                <?php foreach ($jenis_kegiatan as $key => $value) { ?>
                    <h4 class="small font-weight-bold"> <?= $key; ?> <span class="float-right"><?php echo ($value > 0) ? $value : 'N/A' ?></span></h4>
                <?php } ?>
            </div>
        </div>
    </div>
    <div class="col-xl-4 col-lg-7">
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">Laporan Jenis Kegiatan</h6>
            </div>
            <div class="card-body">
                <div class="chart-pie pt-4 pb-2" id="jenis_kegiatan">
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-xl-4 col-lg-7">
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">Laporan Tema Kegiatan</h6>
            </div>
            <div class="card-body">
                <div class="chart-pie pt-4 pb-2" id="tema_kegiatan">
                </div>
            </div>
        </div>
    </div>
    <div class="col-xl-4 col-lg-7">
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">Laporan Jumlah Lokasi (Provinsi)</h6>
            </div>
            <div class="card-body">
                <div class="chart-pie pt-4 pb-2" id="jumlahProvinsi">
                </div>
            </div>
        </div>
    </div>
    <div class="col-xl-4 col-lg-7">
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">Laporan Jumlah Lokasi (Kabupaten)</h6>
            </div>
            <div class="card-body">
                <div class="chart-pie pt-4 pb-2" id="jumlahKabupaten">
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-xl-4 col-lg-7">
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">Laporan Jumlah Lokasi (Kelurahan)</h6>
            </div>
            <div class="card-body">
                <div class="chart-pie pt-4 pb-2" id="jumlahKelurahan">
                </div>
            </div>
        </div>
    </div>
</div>

<?php echo $this->endSection() ?> ?>

<?php echo $this->section('script'); ?>

<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/data.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="https://code.highcharts.com/modules/accessibility.js"></script>
<script>
    Highcharts.chart('jenis_kegiatan', {
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
            type: 'pie'
        },
        title: {
            text: ''
        },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.y}</b>'
        },
        accessibility: {
            point: {
                valueSuffix: '%'
            }
        },
        series: [{
            name: 'Jumlah',
            colorByPoint: true,
            data: <?= $jenis_kegiatan_pie ?>
        }]
    });

    Highcharts.chart('tema_kegiatan', {
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
            type: 'pie'
        },
        title: {
            text: ''
        },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.y}</b>'
        },
        accessibility: {
            point: {
                valueSuffix: '%'
            }
        },
        series: [{
            name: 'Jumlah',
            colorByPoint: true,
            data: <?= $tema_kegiatan ?>
        }]
    });

    Highcharts.chart('jumlahProvinsi', {
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
            type: 'pie'
        },
        title: {
            text: ''
        },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.y}</b>'
        },
        accessibility: {
            point: {
                valueSuffix: '%'
            }
        },
        series: [{
            name: 'Jumlah',
            colorByPoint: true,
            data: <?= $jumlah_provinsi ?>
        }]
    });

    Highcharts.chart('jumlahKabupaten', {
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
            type: 'pie'
        },
        title: {
            text: ''
        },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.y}</b>'
        },
        accessibility: {
            point: {
                valueSuffix: '%'
            }
        },
        series: [{
            name: 'Jumlah',
            colorByPoint: true,
            data: <?= $jumlah_kabupaten ?>
        }]
    });

    Highcharts.chart('jumlahKelurahan', {
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
            type: 'pie'
        },
        title: {
            text: ''
        },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.y}</b>'
        },
        accessibility: {
            point: {
                valueSuffix: '%'
            }
        },
        series: [{
            name: 'Jumlah',
            colorByPoint: true,
            data: <?= $jumlah_kelurahan ?>
        }]
    });

    $("[name='tingkat']").on('change', function() {
        val = $(this).val();
        if (val == 'semua') {
            html = '';
            data = <?= json_encode($jenis_kegiatan) ?>;
            $.each(data, function(k, v) {
                html += '<h4 class="small font-weight-bold">' + k + '<span class="float-right">' + ((v > 0) ? v : '0') + '</span></h4>';
            })
            $('#showJenisKegiatan').html(html)
        } else {
            $.ajax({
                url: '<?= base_url('Admin/Report/UjiPetik/getJenisByProvince') ?>',
                method: 'post',
                data: {
                    tingkat: val
                },
                dataType: 'json',
                success: function(xhr) {
                    html = '';
                    $.each(xhr, function(k, v) {
                        html += '<h4 class="small font-weight-bold">' + k + '<span class="float-right">' + ((v > 0) ? v : '0') + '</span></h4>';
                    })
                    $('#showJenisKegiatan').html(html)
                }
            })
        }
    });
</script>
<?php echo $this->endSection() ?> ?>