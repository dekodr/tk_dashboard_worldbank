<?php echo $this->extend('base/template'); ?>

<?php echo $this->section('content') ?>
<!-- Page Heading -->

<div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">Uji Petik</h1>
</div>

<?php if (session()->getFlashdata('error')) { ?>
    <div class="card mb-4 py-3 border-left-danger">
        <div class="card-body">
            <?php print_r(session()->getFlashdata('error')) ?>
        </div>
    </div>
<?php } ?>

<!-- DataTales Example -->
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Form - Edit Ujit Petik </h6>
    </div>
    <div class="card-body row">
        <div class="col-xl-8 col-lg-7">
            <form method="POST" class="user" accept-charset="utf-8" enctype="multipart/form-data" action="<?php echo base_url('admin/report/UjiPetik/save_update/revisi') ?>">
                <input name="id" type="hidden" class="form-control form-control-user" id="id" value="<?= $getUjiPetik->id; ?>">
                <div class="form-group" id="jns_kegiatan">
                    <label for="">Jenis Kegiatan</label>
                    <select class="form-control " id="jenis_kegiatan" name="jenis_kegiatan">
                        <option value="">- Pilih Jenis Kegiatan -</option>
                        <option value="Uji Petik" <?= $getUjiPetik->jenis_kegiatan == 'Uji Petik' ? 'selected ' : ''; ?>>Uji Petik</option>
                        <option value="Misi Dukungan Implementasi" <?= $getUjiPetik->jenis_kegiatan == 'Misi Dukungan Implementasi' ? 'selected ' : ''; ?>>Misi Dukungan Implementasi</option>
                        <option value="Misi Teknis" <?= $getUjiPetik->jenis_kegiatan == 'Misi Teknis' ? 'selected ' : ''; ?>>Misi Teknis</option>
                    </select>
                </div>
                <div class="form-group" id="tngkt_kegiatan">
                    <label for="">Tingkat Kegiatan</label>
                    <select class="form-control " id="tingkat_kegiatan" name="tingkat_kegiatan">
                        <option value="">- Pilih Tingkat Kegiatan -</option>
                        <option value="Nasional" <?= $getUjiPetik->tingkat_kegiatan == 'Nasional' ? 'selected ' : ''; ?>>Nasional</option>
                        <option value="KMP" <?= $getUjiPetik->tingkat_kegiatan == 'KMP' ? 'selected ' : ''; ?>>KMP</option>
                        <option value="OSP" <?= $getUjiPetik->tingkat_kegiatan == 'OSP' ? 'selected ' : ''; ?>>OSP</option>
                        <option value="KorKot" <?= $getUjiPetik->tingkat_kegiatan == 'KorKot' ? 'selected ' : ''; ?>>KorKot</option>
                    </select>
                </div>
                <div class="form-group row" id="tgl1">
                    <div class="col-sm-6 mb-3 mb-sm-0">
                        <label for="">Tanggal Mulai</label>
                        <input class="form-control" type="date" value="<?= $getUjiPetik->start_date; ?>" name="start_date" id="start_date" format="dd-mm-yyyy">
                    </div>
                    <div class="col-sm-6">
                        <label for="">Tanggal Selesai</label>
                        <input class="form-control" type="date" value="<?= $getUjiPetik->end_date; ?>" name="end_date" id="end_date" format="dd-mm-yyyy">
                    </div>
                </div>
                <div class="form-group" id="tema1">
                    <label for="">Tema</label>
                    <select class="form-control " id="tema" name="tema">
                        <option value="">- Pilih Tema -</option>
                        <option value="Semua" <?= $getUjiPetik->tema == 'Semua' ? 'selected ' : ''; ?>>Semua</option>
                        <option value="Monev" <?= $getUjiPetik->tema == 'Monev' ? 'selected ' : ''; ?>>Monev</option>
                        <option value="MIS" <?= $getUjiPetik->tema == 'MIS' ? 'selected ' : ''; ?>>MIS</option>
                        <option value="Manajemen Keuangan" <?= $getUjiPetik->tema == 'Manajemen Keuangan' ? 'selected ' : ''; ?>>Manajemen Keuangan</option>
                        <option value="Infrastuktur" <?= $getUjiPetik->tema == 'Infrastuktur' ? 'selected ' : ''; ?>>Infrastuktur</option>
                        <option value="Safeguard" <?= $getUjiPetik->tema == 'Safeguard' ? 'selected ' : ''; ?>>Safeguard</option>
                        <option value="PIM" <?= $getUjiPetik->tema == 'PIM' ? 'selected ' : ''; ?>>PIM</option>
                        <option value="Pelatihan" <?= $getUjiPetik->tema == 'Pelatihan' ? 'selected ' : ''; ?>>Pelatihan</option>
                        <option value="Sosialisasi" <?= $getUjiPetik->tema == 'Sosialisasi' ? 'selected ' : ''; ?>>Sosialisasi</option>
                    </select>
                </div>
                <div class="form-group">
                    <label for="">Fokus Kegiatan</label>
                    <input name="fokus_kegiatan" type="text" class="form-control" value="<?= $getUjiPetik->fokus_kegiatan; ?>" autocomplete="off">
                </div>
                <div class="form-group" id="provinsi">
                    <label for="">Lokasi Kegiatan</label>
                    <select class="form-control col-sm-12 rounded-pill" id="id_provinsi" name="id_provinsi">
                        <option value="">- Pilih Provinsi -</option>
                        <?php foreach ($province as $key => $values) : ?>
                            <option value="<?= $values->id; ?>" <?= ($getUjiPetik->id_provinsi == $values->id) ? 'selected' : '' ?>><?= $values->name; ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>

                <div class="form-group" id="kabupaten">
                    <select class="form-control col-sm-12 rounded-pill" id="id_kabupaten" name="id_kabupaten">
                        <option value="">- Pilih Kabupaten -</option>
                        <?php foreach ($district as $key => $values) : ?>
                            <option value="<?= $values->id; ?>" <?= ($getUjiPetik->id_kabupaten == $values->id) ? 'selected' : '' ?>><?= $values->name; ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>

                <div class="form-group" id="kecamatan">
                    <select class="form-control col-sm-12 rounded-pill" id="id_kecamatan" name="id_kecamatan">
                        <option value="">- Pilih Kecamatan -</option>
                        <?php foreach ($sub as $key => $values) : ?>
                            <option value="<?= $values->id; ?>" <?= ($getUjiPetik->id_kecamatan == $values->id) ? 'selected' : '' ?>><?= $values->name; ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>

                <div class="form-group" id="kelurahan">
                    <select class="form-control col-sm-12 rounded-pill" id="id_kelurahan" name="id_kelurahan">
                        <option value="">- Pilih Kelurahan -</option>
                        <?php foreach ($kelurahan as $key => $values) : ?>
                            <option value="<?= $values->id; ?>" <?= ($getUjiPetik->id_kelurahan == $values->id) ? 'selected' : '' ?>><?= $values->name; ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>

                <div class="form-group" id="provinsi_pic">
                    <label for="">Asal PIC</label>
                    <select class="form-control col-sm-12 rounded-pill" id="id_provinsi_pic" name="id_provinsi_pic">
                        <option value="">- Pilih Provinsi -</option>
                        <?php foreach ($province as $key => $values) : ?>
                            <option value="<?= $values->id; ?>" <?= ($getUjiPetik->id_provinsi_pic == $values->id) ? 'selected' : '' ?>><?= $values->name; ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>

                <div class="form-group" id="kabupaten_pic">
                    <select class="form-control col-sm-12 rounded-pill" id="id_kabupaten_pic" name="id_kabupaten_pic">
                        <option value="">- Pilih Kabupaten -</option>
                        <?php foreach ($district as $key => $values) : ?>
                            <option value="<?= $values->id; ?>" <?= ($getUjiPetik->id_kabupaten_pic == $values->id) ? 'selected' : '' ?>><?= $values->name; ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>

                <div class="form-group" id="kecamatan_pic">
                    <select class="form-control col-sm-12 rounded-pill" id="id_kecamatan_pic" name="id_kecamatan_pic">
                        <option value="">- Pilih Kecamatan -</option>
                        <?php foreach ($sub as $key => $values) : ?>
                            <option value="<?= $values->id; ?>" <?= ($getUjiPetik->id_kecamatan_pic == $values->id) ? 'selected' : '' ?>><?= $values->name; ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>

                <div class="form-group" id="kelurahan_pic">
                    <select class="form-control col-sm-12 rounded-pill" id="id_kelurahan_pic" name="id_kelurahan_pic">
                        <option value="">- Pilih Kelurahan -</option>
                        <?php foreach ($kelurahan as $key => $values) : ?>
                            <option value="<?= $values->id; ?>" <?= ($getUjiPetik->id_kelurahan_pic == $values->id) ? 'selected' : '' ?>><?= $values->name; ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>

                <div class="form-group">
                    <label for="">Nama PIC</label>
                    <input name="nama_pic" type="text" class="form-control" value="<?= $getUjiPetik->nama_pic; ?>" autocomplete="off">
                </div>
                <label for="">Topik Pendanaan</label>
                <div class="form-group" id="topik_pendanaan">
                    <label for="">Skala Kawasan</label>
                    <input type="checkbox" name="topik_pendanaan[]" value="skala_kawasan" <?= $getUjiPetik->topik_pendanaan == 'Skala Kawasan' ? 'selected ' : ''; ?> class="form-control form-control-user">
                    <label for="">Skala Lingkungan</label>
                    <input type="checkbox" name="topik_pendanaan[]" value="skala_lingkungan" <?= $getUjiPetik->topik_pendanaan == 'Skala Lingkungan' ? 'selected ' : ''; ?> class="form-control form-control-user">
                    <label for="">Kolaborasi</label>
                    <input type="checkbox" name="topik_pendanaan[]" value="kolaborasi" <?= $getUjiPetik->topik_pendanaan == 'Kolaborasi' ? 'selected ' : ''; ?> class="form-control form-control-user">
                    <label for="">Padat Karya</label>
                    <input type="checkbox" name="topik_pendanaan[]" value="padat_karya" <?= $getUjiPetik->topik_pendanaan == 'Padat Karya' ? 'selected ' : ''; ?> class="form-control form-control-user">
                    <label for="">BPM</label>
                    <input type="checkbox" name="topik_pendanaan[]" value="bpm" <?= $getUjiPetik->topik_pendanaan == 'BPM' ? 'selected ' : ''; ?> class="form-control form-control-user">
                </div>
                <div class="form-group">
                    <label for="">Upload Dokumen</label>
                    <p><input type="hidden" readonly class="form-control-plaintext" id="upload_dokumen1" name="upload_dokumen1" value="<?= implode(",", json_decode($getUjiPetik->upload_dokumen)); ?>"></p>
                    <p><input class="show-for-sr" type="file" id="upload_dokumen[]" name="upload_dokumen[]" multiple accept=".doc, .docx, application/msword, application/vnd.ms-excel, application/vnd.ms-powerpoint,
text/plain, application/pdf" /><br>*ukuran file dibawah 20MB</p>

                </div>
                <div class="form-group">
                    <label for="">Kategori Dokumen</label>
                    <input name="kategori_dokumen" type="text" class="form-control" value="<?= $getUjiPetik->kategori_dokumen; ?>" autocomplete="off">
                </div>
                <div class="form-group">
                    <label for="">Catatan Hasil</label>
                    <input name="catatan_hasil" type="text" class="form-control" value="<?= $getUjiPetik->catatan_hasil; ?>" autocomplete="off">
                </div>
                <div class="form-group">
                    <label for="">Kesimpulan</label>
                    <input name="kesimpulan" type="text" class="form-control" value="<?= $getUjiPetik->kesimpulan; ?>" autocomplete="off">
                </div>
                <div class="form-group">
                    <label for="">Rekomendasi</label>
                    <input name="rekomendasi" type="text" class="form-control" value="<?= $getUjiPetik->rekomendasi; ?>" autocomplete="off">
                </div>
                <div class="form-group" id="revisiDiv">
                    <label for="">Catatan Revisi :</label> <br>
                    <textarea readonly class="form-control" id="revisi" rows="3" placeholder="Catatan Revisi"><?= $comment->comment; ?></textarea>
                </div>
                <hr>
                <button type="submit" class="btn btn-primary btn-icon-split">
                    <span class="icon text-white-50">
                        <i class="fas fa-save"></i>
                    </span>
                    <span class="text">Revisi Data</span>
                </button>
                <a href="<?php echo base_url('/uji-petik') ?>" class="btn btn-secondary">Kembali</a>
            </form>
        </div>
    </div>
</div>
<?php echo $this->endSection() ?> ?>
<?php echo $this->section('script') ?>
<script type="text/javascript">
    $(document).ready(function() {
        $("#id_provinsi").change(function() {
            var id_provinsi = $('#id_provinsi').val();
            $('#id_kabupaten').val('');

            $.ajax({
                url: '/admin/master/ksm/getKabByProv',
                method: 'POST',
                data: {
                    id_provinsi: id_provinsi
                },
                dataType: 'json',
                success: function(data) {

                    var html = '';
                    var i;
                    html += '<option value="">- Pilih Kabupaten -</option>'
                    for (i = 0; i < data.length; i++) {
                        html += '<option value="' + data[i].id + '">' + data[i].name + '</option>';
                    }

                    $('#id_kabupaten').html(html);
                }
            });
            return false;
        });

        $("#id_kabupaten").change(function() {
            var id_kabupaten = $('#id_kabupaten').val();
            $('#id_kecamatan').val('');

            $.ajax({
                url: '/admin/master/ksm/getSubByDistrict',
                method: 'POST',
                data: {
                    id_kabupaten: id_kabupaten
                },
                dataType: 'json',
                success: function(data) {

                    var html = '';
                    var i;
                    html += '<option value="">- Pilih Kecamatan -</option>'
                    for (i = 0; i < data.length; i++) {
                        html += '<option value="' + data[i].id + '">' + data[i].name + '</option>';
                    }

                    $('#id_kecamatan').html(html);
                }
            });
            return false;
        });

        $("#id_kecamatan").change(function() {
            var id_kecamatan = $('#id_kecamatan').val();
            $('#id_kelurahan').val('');

            $.ajax({
                url: '/admin/master/ksm/getVilBySub',
                method: 'POST',
                data: {
                    id_kecamatan: id_kecamatan
                },
                dataType: 'json',
                success: function(data) {

                    var html = '';
                    var i;
                    html += '<option value="">- Pilih Kelurahan -</option>'
                    for (i = 0; i < data.length; i++) {
                        html += '<option value="' + data[i].id + '">' + data[i].name + '</option>';
                    }

                    $('#id_kelurahan').html(html);
                }
            });
            return false;
        });
    });
</script>
<?php echo $this->endSection() ?> ?>