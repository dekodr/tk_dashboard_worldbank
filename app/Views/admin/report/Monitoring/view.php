<?php echo $this->extend('base/template'); ?>

<?php echo $this->section('content') ?>
<!-- Page Heading -->

<div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">Report</h1>
</div>

<?php if (session()->getFlashdata('error')) { ?>
    <div class="card mb-4 py-3 border-left-danger">
        <div class="card-body">
            <?php print_r(session()->getFlashdata('error')) ?>
        </div>
    </div>
<?php } ?>
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

<!-- <script src="jquery.js"></script> -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.3/jquery.js"></script>

<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.panzoom/3.2.2/jquery.panzoom.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/angular-mousewheel/1.0.5/mousewheel.js"></script>
<!-- <script src="custom.js"></script> -->
<style>
    div#page {
        margin-left: 1em;
        margin-right: 1em;
    }

    #options {
        margin: 1em 1em 0em 1em;
    }

    .carousel-control.left,
    .carousel-control.right {
        background: none !important;
    }

    #myCarousel img {
        width: 100%;
    }

    .optionBox,
    #optionHead1,
    #optionHead2 {
        text-align: center;
    }

    .carousel-inner {
        background-color: black;
    }

    .buttons {
        width: 6%;
        position: absolute;
        top: 2%;
        left: 4%;
        z-index: 99;
    }

    .buttons button {
        display: block;
        margin-bottom: 3px;

    }

    /* CSS REQUIRED */
    .state-icon {
        left: -5px;
    }

    .list-group-item-primary {
        color: rgb(255, 255, 255);
        background-color: rgb(66, 139, 202);
    }

    /* DEMO ONLY - REMOVES UNWANTED MARGIN */
    .well .list-group {
        margin-bottom: 0px;
    }

    @media screen and (min-width: 768px) {

        .carousel-control .glyphicon-chevron-left,
        .carousel-control .icon-prev {
            margin-left: -45px !important;
        }

        .carousel-control .glyphicon-chevron-right,
        .carousel-control .icon-next {
            margin-right: -45px !important;
        }

        #options .row:nth-child(1) {
            /* margin-bottom:4em; */
        }
    }
</style>

<!-- DataTales Example -->
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Form - Report <?= $ksm->name; ?></h6>
    </div>
    <div class="card-body row">
        <div class="col-xl-10 col-lg-7">
            <div class="form-group">
                <label for="" style="font-size: 14px;font-weight: bold;">Lampiran Foto</label> <br>
                <div class="w3-content" style="max-width:800px;height:400px;">
                    <div class="row">
                        <div class="col-md-12">
                            <div id="imgDiv">

                                <div class="buttons">
                                    <button type="button" class=" zoom-out btn btn-default">
                                        <span class="glyphicon glyphicon-minus"></span>
                                    </button>
                                    <button type="button" class=" zoom-in btn btn-default">
                                        <span class="glyphicon glyphicon-plus"></span>
                                    </button>
                                </div>

                                <div id="myCarousel" class="carousel slide" data-ride="carousel">
                                    <!-- Wrapper for slides -->
                                    <div class="carousel-inner" role="listbox">
                                        <?php foreach ($arrImgs as $key => $value) : ?>
                                            <div class="item <?php echo ($key == 0) ? 'active' : ''; ?> panzoom">
                                                <img src="<?= $value; ?>" alt="<?= $value; ?>" class="img-responsive" style="height:50vh;">
                                            </div>
                                        <?php endforeach; ?>
                                    </div>

                                    <!-- Left and right controls -->
                                    <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
                                        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                                        <span class="sr-only">Previous</span>
                                    </a>
                                    <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
                                        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                                        <span class="sr-only">Next</span>
                                    </a>
                                </div>

                            </div>

                            <div class="col-md-12" style="text-align:center;margin-top: 1em;margin-bottom: 1em;">
                                <button class="reset btn btn-primary">Reset Zoom</button>
                            </div>

                        </div>
                    </div>
                    <script>
                        (function() {
                            var $section = $('div').first();
                            $section.find('.panzoom').panzoom({
                                $zoomIn: $section.find(".zoom-in"),
                                $zoomOut: $section.find(".zoom-out"),
                                $zoomRange: $section.find(".zoom-range"),
                                $reset: $section.find(".reset")
                            });

                            $('#myCarousel').carousel({
                                pause: true,
                                interval: false
                            });

                        })();
                    </script>
                </div>
            </div>
            <hr>
            <form method="POST" class="user" accept-charset="utf-8" action="<?php echo base_url('Admin/Report/ReportMonitoring/saveProgress') ?>">
                <div class="form-group row">
                    <label for="staticEmail" class="col-sm-4 col-form-label" style="font-size: 14px;font-weight: bold;">Judul</label>
                    <div class="col-sm-5">
                        <input type="text" readonly class="form-control-plaintext" id="staticEmail" value="<?= ($getReport->title == 'foto_progress' ? 'Umum / Harian' : ($getReport->title == 'kemajuan_fisik' ? 'Kemajuan Fisik' : ($getReport->title == 'kas' ? 'Buku Kas' : ($getReport->title == 'bank' ? 'Buku Bank' : ($getReport->title == 'dkis' ? 'Buku DKIS' : ($getReport->title == 'rekening_bank' ? 'Buku Rekening Bank KSM' : ($getReport->title == 'lppu' ? 'LPPU' : ''))))))); ?>">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="staticEmail" class="col-sm-4 col-form-label" style="font-size: 14px;font-weight: bold;">Deskripsi</label>
                    <div class="col-sm-5">
                        <textarea name="" id="" class="form-control-plaintext" readonly><?= $getReport->description; ?></textarea>
                        <!-- <input type="text" readonly class="form-control-plaintext" id="staticEmail" value="<?= $getReport->description; ?>"> -->
                    </div>
                </div>
                <div class="form-group row" <?= ($getReport->type == 'keuangan') ? 'hidden' : ''; ?>>
                    <label for="staticEmail" class="col-sm-4 col-form-label" style="font-size: 14px;font-weight: bold;">Tanggal Mulai</label>
                    <div class="col-sm-5">
                        <input type="date" readonly class="form-control-plaintext" id="staticEmail" value="<?= $getReport->start_date; ?>">
                    </div>
                </div>
                <div class="form-group row" <?= ($getReport->type == 'keuangan') ? 'hidden' : ''; ?>>
                    <label for="staticEmail" class="col-sm-4 col-form-label" style="font-size: 14px;font-weight: bold;">Tanggal Selesai</label>
                    <div class="col-sm-5">
                        <input type="date" readonly class="form-control-plaintext" id="staticEmail" value="<?= $getReport->end_date; ?>">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="staticEmail" class="col-sm-4 col-form-label" style="font-size: 14px;font-weight: bold;">Jenis Laporan</label>
                    <div class="col-sm-5">
                        <input type="text" readonly class="form-control-plaintext" id="staticEmail" value="Laporan <?= ucwords($getReport->type); ?>">
                    </div>
                </div>
                <?php if ($getReport->type != 'keuangan') : ?>
                    <div class="form-group row">
                        <label class="col-sm-4 col-form-label" style="font-size: 14px;font-weight: bold;">Progress</label>
                        <div class="col-sm-5">
                            <input type="text" readonly class="form-control-plaintext" id="valProgress" value="<?= $getReport->progress; ?> %">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="staticEmail" class="col-sm-4 col-form-label" style="font-size: 14px;font-weight: bold;">Ketersediaan Hibah Lahan</label>
                        <div class="col-sm-5">
                            <input type="text" readonly class="form-control-plaintext" id="staticEmail" value="<?= $getReport->hibah; ?>">
                        </div>
                    </div>
                    <div class="form-group row" id="volRencana">
                        <label class="col-sm-4 col-form-label" style="font-size: 14px;font-weight: bold;">Vol. Rencana</label>
                        <div class="col-sm-5">
                            <input type="text" readonly class="form-control-plaintext" value="<?= $getReport->vol_plan; ?> <?= $getReport->vol_unit; ?>">
                        </div>
                    </div>
                    <div class="form-group row" id="volRealisasi">
                        <label class="col-sm-4 col-form-label" style="font-size: 14px;font-weight: bold;">Vol. Realisasi</label>
                        <div class="col-sm-5">
                            <input type="text" readonly class="form-control-plaintext" value="<?= $getReport->vol_real; ?> <?= $getReport->vol_unit; ?>">
                        </div>
                    </div>
                    <div class="form-group row" <?= ($getReport->type == 'keuangan') ? 'hidden' : ''; ?>>
                        <label for="staticEmail" class="col-sm-4 col-form-label" style="font-size: 14px;font-weight: bold;">Paket Pekerjaan</label>
                        <div class="col-sm-5">
                            <input type="text" readonly class="form-control-plaintext" id="staticEmail" value="<?= $getProject->name; ?>">
                        </div>
                    </div>
                    <div class="form-group row" <?= ($getReport->type == 'keuangan') ? 'hidden' : ''; ?>>
                        <label for="staticEmail" class="col-sm-4 col-form-label" style="font-size: 14px;font-weight: bold;">Kegiatan Pekerjaan</label>
                        <div class="col-sm-5">
                            <input type="text" readonly class="form-control-plaintext" id="staticEmail" value="<?= $getSubProject->name; ?>">
                        </div>
                    </div>
                    <div class="form-group row" <?= ($getReport->type == 'keuangan') ? 'hidden' : ''; ?>>
                        <label for="staticEmail" class="col-sm-4 col-form-label" style="font-size: 14px;font-weight: bold;">Lokasi Pekerjaan</label>
                        <div class="col-sm-5">
                            <input type="text" readonly class="form-control-plaintext" id="staticEmail" value="<?= $getLocProject->name; ?>">
                        </div>
                    </div>
                <?php endif; ?>

                <div class="form-group row">
                    <label for="staticEmail" class="col-sm-4 col-form-label" style="font-size: 14px;font-weight: bold;">Dana Diterima Rekening (Rp)</label>
                    <div class="col-sm-5">
                        <input type="text" readonly class="form-control-plaintext" id="staticEmail" value="<?= number_format($getReport->fund_utilization, 2, ",", "."); ?>">
                    </div>
                </div>

                <?php if ($getReport->is_approval == '4') : ?>
                    <div class="form-group row">
                        <label for="staticEmail" class="col-sm-4 col-form-label" style="font-size: 14px;font-weight: bold;">Catatan Revisi :</label>
                        <div class="col-sm-5">
                            <input type="text" readonly class="form-control-plaintext" id="staticEmail" value="<?= $revisi->comment; ?>">
                        </div>
                    </div>
                <?php endif; ?>

                <div class="form-group">
                    <label for="">Lampiran File</label> <br>
                    <?php if ($file != null) : ?>
                        <?php foreach ($file as $key => $val) : ?>
                            <a href="<?= $val; ?>" class="btn btn-success " target="_blank">Document <?= $getReport->doc_type; ?> - <?= $key + 1; ?></a>
                        <?php endforeach; ?>
                    <?php else : ?>
                        No File Uploaded
                    <?php endif; ?>
                </div>
                <div class="form-group" id="revisiDiv">
                    <label for="">Catatan Revisi :</label> <br>
                    <textarea class="form-control" id="revisi" name="revisi" rows="3" placeholder="Catatan Revisi"></textarea>
                </div>
                <?php if ($replyRep != null) : ?>
                    <?php if (($user->id_role == 9 && $getReport->is_approval == 0) || ($user->id_role == 10 && $getReport->is_approval == 2)) : ?>
                        <table class="table">
                            <thead class="thead-dark">
                                <tr>
                                    <td colspan="5" align="center">Progress Menunggu Persetujuan</td>
                                </tr>
                            </thead>
                        </table>
                    <?php endif; ?>
                    <?php if (($getReport->is_approval == '4') && ($_SESSION['id_role'] == 3 || $_SESSION['id_role'] == 4 || $_SESSION['id_role'] == 2) && ($getReport->id_user == $_SESSION['id'])) : ?>
                        <table class="table">
                            <thead class="thead-dark">
                                <tr>
                                    <td colspan="5" align="center">Laporan Harus Di Revisi</td>
                                </tr>
                            </thead>
                        </table>
                    <?php endif; ?>
                    <hr>
                    <table class="table">
                        <thead class="thead-dark">
                            <tr style="text-align: center;">
                                <th scope="col">Judul</th>
                                <th scope="col">Dana Diterima</th>
                                <th scope="col">Deskripsi</th>
                                <?php if ($getReport->type != 'keuangan') : ?>
                                    <th scope="col">Progres</th>
                                <?php endif; ?>
                                <th scope="col">Umum / Harian</th>
                                <th scope="col">Dokument Progres</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($replyRep as $key1 => $value1) : ?>
                                <tr style="text-align: center;">
                                    <td><?= ($value1->title == 'foto_progress' ? 'Umum / Harian' : ($value1->title == 'kemajuan_fisik' ? 'Kemajuan Fisik' : ($value1->title == 'kas' ? 'Buku Kas' : ($value1->title == 'bank' ? 'Buku Bank' : ($value1->title == 'dkis' ? 'Buku DKIS' : ($value1->title == 'rekening_bank' ? 'Buku Rekening Bank KSM' : ($value1->title == 'lppu' ? 'LPPU' : ''))))))); ?></td>
                                    <td><?= number_format($value1->fund_utilization, 2, ",", "."); ?></td>
                                    <td><textarea name="" id="" class="form-control-plaintext" readonly><?= $value1->description; ?></textarea></td>
                                    <?php if ($getReport->type != 'keuangan') : ?>
                                        <td><?= $value1->progress; ?> %</td>
                                    <?php endif; ?>
                                    <td>
                                        <!-- Button trigger modal -->
                                        <button type="button" class="btn btn-info" data-toggle="modal" data-target="#exampleModal<?= $key1; ?>">
                                            Gambar Progres
                                        </button>

                                        <!-- Modal -->
                                        <div class="modal fade" id="exampleModal<?= $key1; ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                            <div class="modal-dialog" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header  text-light bg-dark">
                                                        <h5 class="modal-title" id="exampleModalLabel" class="g">Gambar Progress</h5>
                                                    </div>
                                                    <div class="modal-body">
                                                        <?php foreach (json_decode($value1->images) as $key => $val) : ?>
                                                            <img src="<?= $val; ?>" alt="<?= $val; ?>" height="150 px" /> &nbsp;
                                                            <!-- <a href="<?= $value; ?>" class="btn btn-success " target="_blank"><?= $value; ?></a> -->
                                                        <?php endforeach; ?>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                    <td>
                                        <?php foreach (json_decode($value1->document) as $key => $val) : ?>
                                            <a href="<?= $val; ?>" class="btn btn-success " target="_blank">Document</a>
                                        <?php endforeach; ?>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>

                <?php else : ?>
                    <?php if (($user->id_role == 9 && $getReport->is_approval == '0') || ($user->id_role == 10 && $getReport->is_approval == '2')) : ?>
                        <table class="table">
                            <thead class="thead-dark">
                                <tr>
                                    <td colspan="5" align="center">Progress Menunggu Persetujuan / Belum Ada Riwayat Progress</td>
                                </tr>
                            </thead>
                        </table>
                    <?php endif; ?>
                    <?php if (($getReport->is_approval == '4') && ($getReport->id_user == $_SESSION['id'])) : ?>
                        <table class="table">
                            <thead class="thead-dark">
                                <tr>
                                    <td colspan="5" align="center">Laporan Harus Di Revisi</td>
                                </tr>
                            </thead>
                        </table>
                    <?php endif; ?>
                <?php endif; ?>

                <hr>
                <label for="staticEmail" class="col-sm-4 col-form-label"></label>
                <?php if (($user->id_role == 9 && $getReport->is_approval == 0) || ($user->id_role == 10 && $getReport->is_approval == 2)) : ?>
                    <button type="submit" class="btn btn-primary btn-icon-split" onclick="return confirm('Apakah akan approve project ?');">
                        <span class="icon text-white-50">
                            <i class="fas fa-save"></i>
                        </span>
                        <span class="text">Approve</span>
                    </button>
                    <div class="btn btn-danger btn-icon-split" onclick="input_revisi()">
                        <span class="icon text-white-50 ">
                            <i class="fas fa-trash"></i>
                        </span>
                        <a class="btn btn-danger ">Revisi</a>
                    </div>
                <?php endif; ?>

                <?php if (($getReport->is_publish == 't' && $getReport->is_approval == '1') && ($_SESSION['id_role'] == 3 || $_SESSION['id_role'] == 4 || $_SESSION['id_role'] == 2) && ($getReport->id_user == $_SESSION['id'])) : ?>
                    <button type="submit" class="btn btn-primary btn-icon-split">
                        <span class="icon text-white-50">
                            <i class="fas fa-save"></i>
                        </span>
                        <span class="text">Report Progress</span>
                    </button>
                <?php endif; ?>

                <?php if ($getReport->is_approval == 4 && $getReport->id_user == $user->id) : ?>
                    <div class="btn btn-info btn-icon-split">
                        <span class="icon text-white-50 ">
                            <i class="fas fa-info"></i>
                        </span>
                        <a href="<?= base_url('Admin/Report/ReportMonitoring/revisi_report/' . $getReport->idoffline) ?>" class="btn btn-info ">Lakukan Revisi</a>
                    </div>
                <?php endif; ?>

                <div class="btn btn-secondary btn-icon-split">
                    <a href="<?php echo base_url('report-monitoring') ?>">
                        <span class="icon text-white-50">
                            <i class="fas fa-undo-alt"></i>
                        </span>
                    </a>
                    <a href="<?php echo base_url('report-monitoring') ?>" class="btn btn-secondary ">Kembali</a>
                </div>
                <input type="hidden" readonly class="form-control-plaintext" id="idReport" name="idReport" value="<?= $getReport->idoffline; ?>">
            </form>
        </div>
    </div>
</div>

<script src="<?php echo base_url('assets/vendor/jquery/jquery.min.js'); ?>"></script>
<script type="text/javascript">
    var valueProgress = $('#valProgress').val();
    // alert(valueProgress);
    if (valueProgress != '100 %') {
        $('#volRencana').hide();
        $('#volRealisasi').hide();
    } else {
        $('#volRencana').show();
        $('#volRealisasi').show();
    }
</script>
<?php echo $this->endSection() ?> ?>
<?php echo $this->section('script') ?>
<script type="text/javascript">
    $('#revisiDiv').hide();
    // $('#volRencana').hide();
    // $('#volRealisasi').hide();
    var slideIndex = 1;
    // showDivs(slideIndex);

    function input_revisi() {
        var rev = $('#revisi').val();

        console.log(rev);

        if (rev == '') {
            var i = confirm("Mohon isi catatan reivisi! ");
            if (i == true) {
                $('#revisiDiv').show();
            }
        } else {
            var j = confirm("Laporan akan di reivisi ?");
            var urll = '<?php echo base_url() ?>';
            if (j == true) {
                window.location = urll + '/Admin/Report/ReportMonitoring/reject/' + <?= $getReport->idoffline; ?> + "/" + rev;
            }
        }
    }

    // function plusDivs(n) {
    //     showDivs(slideIndex += n);
    // }

    // function currentDiv(n) {
    //     showDivs(slideIndex = n);
    // }

    // function showDivs(n) {
    //     var i;
    //     var x = document.getElementsByClassName("mySlides");
    //     var dots = document.getElementsByClassName("demo");
    //     if (n > x.length) {
    //         slideIndex = 1
    //     }
    //     if (n < 1) {
    //         slideIndex = x.length
    //     }
    //     for (i = 0; i < x.length; i++) {
    //         x[i].style.display = "none";
    //     }
    //     for (i = 0; i < dots.length; i++) {
    //         dots[i].className = dots[i].className.replace(" w3-red", "");
    //     }
    //     x[slideIndex - 1].style.display = "block";
    //     dots[slideIndex - 1].className += " w3-red";
    // }

    $('.slider').each(function() {
        var $this = $(this);
        var $group = $this.find('.slide_group');
        var $slides = $this.find('.slide');
        var bulletArray = [];
        var currentIndex = 0;
        var timeout;

        function move(newIndex) {
            var animateLeft, slideLeft;

            advance();

            if ($group.is(':animated') || currentIndex === newIndex) {
                return;
            }

            bulletArray[currentIndex].removeClass('active');
            bulletArray[newIndex].addClass('active');

            if (newIndex > currentIndex) {
                slideLeft = '100%';
                animateLeft = '-100%';
            } else {
                slideLeft = '-100%';
                animateLeft = '100%';
            }

            $slides.eq(newIndex).css({
                display: 'block',
                left: slideLeft
            });
            $group.animate({
                left: animateLeft
            }, function() {
                $slides.eq(currentIndex).css({
                    display: 'none'
                });
                $slides.eq(newIndex).css({
                    left: 0
                });
                $group.css({
                    left: 0
                });
                currentIndex = newIndex;
            });
        }

        function advance() {
            clearTimeout(timeout);
            timeout = setTimeout(function() {
                if (currentIndex < ($slides.length - 1)) {
                    move(currentIndex + 1);
                } else {
                    move(0);
                }
            }, 4000);
        }

        $('.next_btn').on('click', function() {
            if (currentIndex < ($slides.length - 1)) {
                move(currentIndex + 1);
            } else {
                move(0);
            }
        });

        $('.previous_btn').on('click', function() {
            if (currentIndex !== 0) {
                move(currentIndex - 1);
            } else {
                move(3);
            }
        });

        $.each($slides, function(index) {
            var $button = $('<a class="slide_btn">&bull;</a>');

            if (index === currentIndex) {
                $button.addClass('active');
            }
            $button.on('click', function() {
                move(index);
            }).appendTo('.slide_buttons');
            bulletArray.push($button);
        });

        advance();
    });
    $(document).ready(function() {});
</script>
<?php echo $this->endSection() ?> ?>