<?php echo $this->extend('base/template'); ?>

<?php echo $this->section('content') ?>
<!-- Page Heading -->

<div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">Report Progress</h1>
</div>

<?php if (session()->getFlashdata('error')) { ?>
    <div class="card mb-4 py-3 border-left-danger">
        <div class="card-body">
            <?php print_r(session()->getFlashdata('error')) ?>
        </div>
    </div>
<?php } ?>

<!-- DataTales Example -->
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Form - Report <?= $ksm->name; ?> </h6>
    </div>
    <div class="card-body row">
        <div class="col-xl-8 col-lg-7">
            <form method="POST" class="user" accept-charset="utf-8" enctype="multipart/form-data" action="<?php echo base_url('/admin/report/reportCon/saveDataProgres') ?>">
                <div class="form-group">
                    <label for="">Lampirkan Foto</label>
                    <p><input class="show-for-sr" type="file" id="upload_imgs" name="upload_imgs[]" accept="image/*" multiple required /></p>
                </div>
                <div class="form-group row">
                    <label for="staticEmail" class="col-sm-4 col-form-label">Jenis Laporan</label>
                    <div class="col-sm-5">
                        <input type="text" readonly class="form-control-plaintext" id="jns_lap" name="jns_lap" value="Laporan <?= ucwords($getReport->type); ?>">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="staticEmail" class="col-sm-4 col-form-label">Judul</label>
                    <div class="col-sm-5">
                        <input type="text" readonly class="form-control-plaintext" value="<?= ($getReport->title == 'foto_progress' ? 'Umum / Harian' : ($getReport->title == 'kemajuan_fisik' ? 'Kemajuan Fisik' : ($getReport->title == 'kas' ? 'Buku Kas' : ($getReport->title == 'bank' ? 'Buku Bank' : ($getReport->title == 'dkis' ? 'Buku DKIS' : ($getReport->title == 'rekening_bank' ? 'Buku Rekening Bank KSM' : ($getReport->title == 'lppu' ? 'LPPU' : ''))))))); ?>">
                    </div>
                </div>

                <?php if ($getReport->type != 'keuangan') : ?>
                    <div class="form-group row">
                        <label for="staticEmail" class="col-sm-4 col-form-label">Paket Pekerjaan</label>
                        <div class="col-sm-5">
                            <input type="text" readonly class="form-control-plaintext" value="<?= $getProject->name; ?>">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="staticEmail" class="col-sm-4 col-form-label">Tanggal Mulai</label>
                        <div class="col-sm-5">
                            <input type="date" class="form-control-plaintext" name="start_date" value="<?= $getReport->start_date; ?>">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="staticEmail" class="col-sm-4 col-form-label">Tanggal Selesai</label>
                        <div class="col-sm-5">
                            <input type="date" class="form-control-plaintext" name="end_date" value="<?= $getReport->end_date; ?>">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="staticEmail" class="col-sm-4 col-form-label">Ketersediaan Hibah Lahan</label>
                        <div class="col-sm-5">
                            <input type="text" readonly class="form-control-plaintext" value="<?= $getReport->hibah; ?>">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="staticEmail" class="col-sm-4 col-form-label">Lokasi Pekerjaan</label>
                        <div class="col-sm-5">
                            <input type="text" readonly class="form-control-plaintext" value="<?= $getLocProject->name; ?>">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="staticEmail" class="col-sm-4 col-form-label">Kegiatan Pekerjaan</label>
                        <div class="col-sm-5">
                            <input type="text" readonly class="form-control-plaintext" value="<?= $getSubProject->name; ?>">
                        </div>
                    </div>
                    <div class="form-group" id="progressdiv">
                        <label for="">Progres Pekerjaan</label>
                        <select class="form-control col-sm-6" id="progress" name="progress">
                            <option value="">- Pilih Progres Pekerjaan -</option>
                            <option value="0">0</option>
                            <option value="25">25</option>
                            <option value="50">50</option>
                            <option value="75">75</option>
                            <option value="100">100</option>
                        </select>
                    </div>
                <?php endif; ?>

                <div class="form-group row">
                    <div class="col-sm-6 mb-3 mb-sm-0">
                        <?php if ($getReport->type != 'keuangan') : ?>
                            <label for="">Tanggal Status Laporan</label>
                            <input class="form-control" type="date" value="<?= $createdd; ?>" name="lap_date" id="lap_date" required>
                        <?php else : ?>
                            <label for="">Tanggal Bulan Laporan </label>
                            <input class="form-control" type="month" value="<?= $createdd; ?>" name="lap_date" id="lap_date" required>
                        <?php endif; ?>
                    </div>
                </div>
                <div class="form-group row">
                    <?php if ($getReport->type != 'keuangan') : ?>
                        <label for="staticEmail" class="col-sm-5 col-form-label">Penerimaan Rekening Bank KSM (Rp)</label>
                        <div class="col-sm-5">
                            <input type="text" readonly class="form-control-plaintext" id="staticEmail" value="<?= number_format($getReport->fund_utilization, 2, ",", "."); ?>">
                        </div>
                    <?php else : ?>
                        <label for="staticEmail" class="col-sm-5 col-form-label">Saldo Terakhir (Rp)</label>
                        <div class="col-sm-5">
                            <input type="text" readonly class="form-control-plaintext" id="staticEmail" value="<?= number_format($getReport->fund_utilization, 2, ",", "."); ?>">
                        </div>
                    <?php endif; ?>
                </div>
                <div class="col-sm-8 mb-3 mb-sm-0" id="vol">
                    <?php if ($getReport->progress != '100') : ?>
                        <div class="form-group row" id="vol1">
                            <div class="col-sm-6">
                                <label for="">Volume Rencana</label>
                                <input name="rencana" class="form-control" id="rencana" placeholder="Vol. Rencana">
                            </div>
                            <div class="col-sm-6">
                                <label for="">Satuan</label>
                                <select class="form-control " id="satPlan" name="satPlan">
                                    <option value="">- Satuan Volume -</option>
                                    <option value="Meter">Meter</option>
                                    <option value="Unit">Unit</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row" id="vol2">
                            <div class="col-sm-6">
                                <label for="">Volume Realisasi</label>
                                <input name="volReal" class="form-control" id="volReal" placeholder="Vol. Realisasi">
                            </div>
                            <div class="col-sm-6">
                                <label for="">Satuan</label>
                                <label id="sat_real" readonly class="form-control-plaintext" name="sat_real"></label>
                            </div>
                        </div>
                    <?php else : ?>
                        <div class="form-group row">
                            <label for="staticEmail" class="col-sm-5 col-form-label">Volume Rencana</label>
                            <div class="col-sm-5">
                                <input type="text" readonly class="form-control-plaintext" value="<?= $getReport->vol_plan; ?>  <?= $getReport->vol_unit; ?>">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="staticEmail" class="col-sm-5 col-form-label">Volume Realisasi</label>
                            <div class="col-sm-5">
                                <input type="text" readonly class="form-control-plaintext" value="<?= $getReport->vol_real; ?>  <?= $getReport->vol_unit; ?>">
                            </div>
                        </div>
                    <?php endif; ?>
                </div>
                <div class="form-group row">
                    <label for="staticEmail" class="col-sm-5 col-form-label">Deskripsi Laporan</label>
                    <textarea class="form-control" id="desc" name="desc" value="<?= $getReport->description; ?>" placeholder="Deskripsi Laporan" required></textarea>
                </div>
                <div class="form-group row">
                    <label for="staticEmail" class="col-sm-5 col-form-label">Nama Dokumen</label>
                    <div class="col-sm-5">
                        <input type="text" readonly class="form-control-plaintext" value="<?= $getReport->doc_type; ?>">
                    </div>
                </div>

                <div class="form-group row">
                    <label for="staticEmail" class="col-sm-5 col-form-label">Upload Dokumen</label>
                    <p><input class="show-for-sr" type="file" id="upload_file[]" name="upload_file[]" accept=".doc, .docx, application/msword, application/vnd.ms-excel, application/vnd.ms-powerpoint,
text/plain, application/pdf" required multiple /></p>
                </div>
                <input type="hidden" readonly class="form-control-plaintext" id="idReport" name="idReport" value="<?= $getReport->idoffline; ?>">
                <hr>
                <button type="submit" class="btn btn-primary btn-icon-split">
                    <span class="icon text-white-50">
                        <i class="fas fa-save"></i>
                    </span>
                    <span class="text">Simpan Data</span>
                </button>
                <a href="<?php echo base_url('report-monitoring') ?>" class="btn btn-secondary">Kembali</a>
            </form>
        </div>
    </div>
</div>
<?php echo $this->endSection() ?> ?>
<?php echo $this->section('script') ?>
<script type="text/javascript">
    $('#vol').hide();
    // $("#vol1").hide();
    // $("#vol2").hide();

    $(document).ready(function() {
        var jns = document.getElementById("jns_lap").value;

        // document.getElementById("rencana").disabled = true;
        // document.getElementById("satPlan").disabled = true;
        // document.getElementById("volReal").disabled = true;

        /* satPlan */
        $("#satPlan").change(function() {
            var satPlan = $('#satPlan').val();
            $('#sat_real').text(satPlan);
        });
        /* satPlan */

        /* Progress */
        $("#progress").change(function() {
            var prog = $('#progress').val();
            var jns_lap = $('#jns_lap').val();

            if (prog == '100' && jns_lap != 'keuangan') {
                // document.getElementById("rencana").disabled = false;
                // document.getElementById("satPlan").disabled = false;
                // document.getElementById("volReal").disabled = false;

                $("#vol").show();
                // $("#vol2").show();
                $("#rencana").attr("required", "");
                $("#satPlan").attr("required", "");
                $("#volReal").attr("required", "");
            } else if (prog != '100' && jns_lap != 'keuangan') {
                $("#rencana").removeAttr("required", "");
                $("#satPlan").removeAttr("required", "");
                $("#volReal").removeAttr("required", "");
                $("#vol1").hide();
                $("#vol2").hide();
            }
        });
        /* progress */
    });
</script>
<?php echo $this->endSection() ?>