<?php echo $this->extend('base/template'); ?>

<?php echo $this->section('content') ?>
<!-- Page Heading -->

<div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">Report</h1>
</div>

<?php if (session()->getFlashdata('error')) { ?>
    <div class="card mb-4 py-3 border-left-danger">
        <div class="card-body">
            <?php print_r(session()->getFlashdata('error')) ?>
        </div>
    </div>
<?php } ?>
<style>
    html,
    body {
        background: #F7F5E6;
        height: 100%;
        margin: 0;
        padding: 0;
        width: 100%;
    }

    .slider {
        margin: 0 auto;
        max-width: 940px;
    }

    .slide_viewer {
        height: 340px;
        overflow: hidden;
        position: relative;
    }

    .slide_group {
        height: 100%;
        position: relative;
        width: 100%;
    }

    .slide {
        display: none;
        height: 100%;
        position: absolute;
        width: 100%;
    }

    .slide:first-child {
        display: block;
    }

    .slide:nth-of-type(1) {
        background: #D7A151;
    }

    .slide:nth-of-type(2) {
        background: #F4E4CD;
    }

    .slide:nth-of-type(3) {
        background: #C75534;
    }

    .slide:nth-of-type(4) {
        background: #D1D1D4;
    }

    .slide_buttons {
        left: 0;
        position: absolute;
        right: 0;
        text-align: center;
    }

    a.slide_btn {
        color: #474544;
        font-size: 42px;
        margin: 0 0.175em;
        -webkit-transition: all 0.4s ease-in-out;
        -moz-transition: all 0.4s ease-in-out;
        -ms-transition: all 0.4s ease-in-out;
        -o-transition: all 0.4s ease-in-out;
        transition: all 0.4s ease-in-out;
    }

    .slide_btn.active,
    .slide_btn:hover {
        color: #428CC6;
        cursor: pointer;
    }

    .directional_nav {
        height: 340px;
        margin: 0 auto;
        max-width: 940px;
        position: relative;
        top: -340px;
    }

    .previous_btn {
        bottom: 0;
        left: 100px;
        margin: auto;
        position: absolute;
        top: 0;
    }

    .next_btn {
        bottom: 0;
        margin: auto;
        position: absolute;
        right: 100px;
        top: 0;
    }

    .previous_btn,
    .next_btn {
        cursor: pointer;
        height: 65px;
        opacity: 0.5;
        -webkit-transition: opacity 0.4s ease-in-out;
        -moz-transition: opacity 0.4s ease-in-out;
        -ms-transition: opacity 0.4s ease-in-out;
        -o-transition: opacity 0.4s ease-in-out;
        transition: opacity 0.4s ease-in-out;
        width: 65px;
    }

    .previous_btn:hover,
    .next_btn:hover {
        opacity: 1;
    }

    @media only screen and (max-width: 767px) {
        .previous_btn {
            left: 50px;
        }

        .next_btn {
            right: 50px;
        }
    }
</style>

<!-- DataTales Example -->
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Form - Revisi Report <?= $ksm->name; ?></h6>
    </div>
    <div class="card-body row">
        <div class="col-xl-8 col-lg-7">
            <p>
                <div class="form-group row">
                    <div class="col-sm-6 mb-3 mb-sm-0">
                        <label for="">Pembuat Laporan</label>
                        <label><?= $_SESSION['name']; ?> - <?= $_SESSION['position']; ?></label>
                    </div>
                </div>
                <div class="w3-content" style="max-width:800px;height:400px;">
                    <div class="slider">
                        <div class="slide_viewer">
                            <div class="slide_group">
                                <?php foreach ($arrImgs as $key => $value) : ?>
                                    <img src="<?= $value; ?>" alt="<?= $value; ?>" height="200 px" class="slide" /> <br>

                                    <!-- <a href="<?= $value; ?>" class="btn btn-success " target="_blank"><?= $value; ?></a> -->
                                <?php endforeach; ?>
                            </div>
                        </div>
                    </div>
                    <div class="slide_buttons">
                    </div>
                    <div class="directional_nav">
                        <div class="previous_btn" title="Previous">
                            <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="65px" height="65px" viewBox="-11 -11.5 65 66">
                                <g>
                                    <g>
                                        <path fill="#474544" d="M-10.5,22.118C-10.5,4.132,4.133-10.5,22.118-10.5S54.736,4.132,54.736,22.118
            c0,17.985-14.633,32.618-32.618,32.618S-10.5,40.103-10.5,22.118z M-8.288,22.118c0,16.766,13.639,30.406,30.406,30.406 c16.765,0,30.405-13.641,30.405-30.406c0-16.766-13.641-30.406-30.405-30.406C5.35-8.288-8.288,5.352-8.288,22.118z" />
                                        <path fill="#474544" d="M25.43,33.243L14.628,22.429c-0.433-0.432-0.433-1.132,0-1.564L25.43,10.051c0.432-0.432,1.132-0.432,1.563,0   c0.431,0.431,0.431,1.132,0,1.564L16.972,21.647l10.021,10.035c0.432,0.433,0.432,1.134,0,1.564    c-0.215,0.218-0.498,0.323-0.78,0.323C25.929,33.569,25.646,33.464,25.43,33.243z" />
                                    </g>
                                </g>
                            </svg>
                        </div>
                        <div class="next_btn" title="Next">
                            <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="65px" height="65px" viewBox="-11 -11.5 65 66">
                                <g>
                                    <g>
                                        <path fill="#474544" d="M22.118,54.736C4.132,54.736-10.5,40.103-10.5,22.118C-10.5,4.132,4.132-10.5,22.118-10.5  c17.985,0,32.618,14.632,32.618,32.618C54.736,40.103,40.103,54.736,22.118,54.736z M22.118-8.288  c-16.765,0-30.406,13.64-30.406,30.406c0,16.766,13.641,30.406,30.406,30.406c16.768,0,30.406-13.641,30.406-30.406 C52.524,5.352,38.885-8.288,22.118-8.288z" />
                                        <path fill="#474544" d="M18.022,33.569c 0.282,0-0.566-0.105-0.781-0.323c-0.432-0.431-0.432-1.132,0-1.564l10.022-10.035          L17.241,11.615c 0.431-0.432-0.431-1.133,0-1.564c0.432-0.432,1.132-0.432,1.564,0l10.803,10.814c0.433,0.432,0.433,1.132,0,1.564 L18.805,33.243C18.59,33.464,18.306,33.569,18.022,33.569z" />
                                    </g>
                                </g>
                            </svg>
                        </div>
                    </div><!-- End // .directional_nav -->
                    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
                </div>

            </p>
            <form method="POST" class="user" accept-charset="utf-8" enctype="multipart/form-data" action="<?php echo base_url('Admin/Report/ReportMonitoring/saveDataRevisi') ?>">
                <input type="hidden" id="repotid" name="repotid" value="<?= $report->idoffline; ?>">
                <div class="form-group">
                    <label for="" style="font-size: 14px;font-weight: bold;">Lampirkan Foto</label>
                    <p><input type="hidden" readonly class="form-control-plaintext" id="reportImgArr" name="reportImgArr" value="<?= implode(",", json_decode($report->images)); ?>"></p>
                    <p><input class="show-for-sr" type="file" id="upload_imgs" name="upload_imgs[]" accept="image/*" multiple /></p>
                </div>
                <div class="form-group ">
                    <label>Jenis Laporan : </label>
                    <input type="hidden" id="jns_lap" name="jns_lap" value="<?= $report->type; ?>">
                    <input type="hidden" id="jns_lap2" name="jns_lap2" value="<?= ($report->type == 'infrastruktur') ? 'Laporan Infrastruktur' : ($report->type == 'keuangan' ? 'Laporan Man. Keuangan' : ''); ?>">
                    <b><?= ($report->type == 'infrastruktur') ? 'Laporan Infrastruktur' : ($report->type == 'keuangan' ? 'Laporan Man. Keuangan' : ''); ?></b>
                </div>
                <div class="form-group ">
                    <label for="exampleFormControlSelect1">Judul</label>
                    <select class="form-control " id="jdl_lap" name="jdl_lap" required>
                        <option value="">- Pilih Judul -</option>
                        <?php if ($report->type != 'infrastruktur') : ?>
                            <option value="kas" <?= ($report->title == 'kas') ? 'selected' : ''; ?>>Buku Kas</option>
                            <option value="bank" <?= ($report->title == 'bank') ? 'selected' : ''; ?>>Buku Bank</option>
                            <option value="dkis" <?= ($report->title == 'dkis') ? 'selected' : ''; ?>>Buku DKIS</option>
                            <option value="rekening_bank" <?= ($report->title == 'rekening_bank') ? 'selected' : ''; ?>>Buku Rekening Bank KSM</option>
                            <option value="lppu" <?= ($report->title == 'lppu') ? 'selected' : ''; ?>>LPPU</option>
                        <?php else : ?>
                            <option value="foto_progress" <?= ($report->title == 'foto_progress') ? 'selected' : ''; ?>>Umum / Harian</option>
                            <option value="kemajuan_fisik" <?= ($report->title == 'kemajuan_fisik') ? 'selected' : ''; ?>>Kemajuan Fisik</option>
                        <?php endif; ?>

                    </select>
                </div>

                <?php if ($report->type == 'infrastruktur') : ?>
                    <div class="form-group " id="pkt_lap1">
                        <label>Paket Pekerjaan</label>
                        <select class="form-control" id="pkt_lap" name="pkt_lap">
                            <option value="">- Pilih Paket Pekerjaan -</option>
                            <?php foreach ($pkt_job as $pkt) : ?>
                                <option value="<?= $pkt->id; ?>" <?= ($report->id_project == $pkt->id) ? 'selected' : ''; ?>><?= $pkt->name; ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                    <div class="form-group" id="kegiatan">
                        <label for="">Kegiatan Pekerjaan</label>
                        <select class="form-control " id="kegiatanList" name="kegiatanList">
                            <option value="">- Pilih Kegiatan Pekerjaan -</option>
                            <?php foreach ($project as $keg) :  ?>
                                <option value="<?= $keg->id; ?>" <?= ($report->id_sub_project == $keg->id) ? 'selected' : ''; ?>><?= $keg->name; ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                    <div class="form-group" id="lokasidiv">
                        <label for="">Lokasi Pekerjaan</label>
                        <select class="form-control " id="lokasi" name="lokasi">
                            <option value="">- Pilih Lokasi Pekerjaan -</option>
                            <?php foreach ($loc as $loc) : ?>
                                <option value="<?= $loc->id; ?>" <?= ($report->id_project_loc == $loc->id ? 'selected' : ''); ?>><?= $loc->name; ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                    <div class="form-group row" id="tgl1">
                        <div class="col-sm-6 mb-3 mb-sm-0">
                            <label for="">Tanggal Mulai</label>
                            <input class="form-control" type="date" name="str_date" id="str_date" format="dd-mm-yyyy" value="<?= $report->start_date; ?>">
                        </div>
                        <div class="col-sm-6">
                            <label for="">Tanggal Selesai</label>
                            <input class="form-control" type="date" value="<?= $report->end_date; ?>" name=" end_date" id="end_date" format="dd-mm-yyyy">
                        </div>
                    </div>
                    <div class="form-group" id="lahan">
                        <label for="">Ketersediaan Hibah Lahan</label>
                        <select class="form-control " id="hibah" name="hibah">
                            <option value="">- Pilih Hibah Lahan -</option>
                            <option value="ya" <?= $report->hibah == 'ya' ? 'selected' : ''; ?>>YA</option>
                            <option value="tidak" <?= $report->hibah == 'tidak' ? 'selected' : ''; ?>>TIDAK</option>
                        </select>
                    </div>
                    <div class="form-group " id="progressSlct">
                        <label for="">Progres Pekerjaan</label>
                        <select class="form-control " id="progress" name="progress">
                            <option value="">- Pilih Progres Pekerjaan -</option>
                            <option value="0" <?= $report->progress == '0' ? 'selected' : ''; ?>>0</option>
                            <option value="25" <?= $report->progress == '25' ? 'selected' : ''; ?>>25</option>
                            <option value="50" <?= $report->progress == '50' ? 'selected' : ''; ?>>50</option>
                            <option value="75" <?= $report->progress == '75' ? 'selected' : ''; ?>>75</option>
                            <option value="100" <?= $report->progress == '100' ? 'selected' : ''; ?>>100</option>
                        </select>
                    </div>
                    <div class="form-group row" id="date_infra">
                        <div class="col-sm-6 mb-3 mb-sm-0">
                            <label for="">Tanggal Status Laporan</label>
                            <input class="form-control" type="date" value="<?= $report->status_date; ?>" name="lap_date" id="lap_date" format="dd-mm-yyyy">
                        </div>
                    </div>
                    <div class="form-group row" id="vol1">
                        <div class="col-sm-6">
                            <label for="">Volume Rencana</label>
                            <input name="rencana" class="form-control" id="rencana" value="<?= $report->vol_plan; ?>" placeholder="Vol. Rencana">
                        </div>
                        <div class="col-sm-6">
                            <label for="">Satuan</label>
                            <select class="form-control " id="satPlan" name="satPlan">
                                <option value="">- Satuan Volume -</option>
                                <option value="Meter" <?= $report->vol_unit == 'meter' ? 'selected ' : ''; ?>>Meter</option>
                                <option value="Unit" <?= $report->vol_unit == 'unit' ? 'selected ' : ''; ?>>Unit</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group row" id="vol2">
                        <div class="col-sm-6">
                            <label for="">Volume Realisasi</label>
                            <input name="volReal" class="form-control" id="volReal" value="<?= $report->vol_real; ?>" placeholder="Vol. Realisasi">
                        </div>
                        <div class="col-sm-6">
                            <label for="">Satuan</label>
                            <label id="sat_real" readonly class="form-control-plaintext" name="sat_real"><?= $report->vol_unit ?></label>
                        </div>
                    </div>
                <?php endif; ?>

                <?php if ($report->type == 'keuangan') : ?>
                    <div class="form-group row" id="date_keu">
                        <div class="col-sm-6 mb-3 mb-sm-0">
                            <label for="">Laporan Bulan </label>
                            <input class="form-control" type="month" name="lap_date" id="lap_date" value="<?= $report->progress_month; ?>">
                        </div>
                    </div>
                <?php endif; ?>

                <div class="form-group">
                    <label for="" id="nmTerima" name="nmTerima"><?= $report->type == 'keuangan' ? 'Saldo Terakhir (Rp)' : 'Penerimaan Rekening Bank KSM (Rp)'; ?></label>
                    <input name="terima" class="form-control" id="terima" data-type="currency" placeholder="0.00" value="<?= number_format($report->fund_utilization, 2, ",", "."); ?>">
                </div>

                <div class="form-group">
                    <label for="">Deskripsi Laporan </label>
                    <textarea class="form-control" id="desc" name="desc" rows="3" placeholder="Deskripsi Laporan" required><?= $report->description; ?></textarea>
                </div>

                <?php if ($report->type == 'keuangan') : ?>
                    <div class="form-group" id="dok_name_input">
                        <label for="">Nama Dokumen</label>
                        <input type="text" class="form-control" id="dok_name_input" name="dok_name_input" placeholder="Nama Dokumen" value="<?= $report->doc_type; ?>">
                    </div>
                <?php else : ?>
                    <div class="form-group " id="dok_name">
                        <label for="">Nama Dokumen</label>
                        <select class="form-control " id="php" name="dok_nameList" id="dok_nameList">
                            <option value="">- Pilih Nama Dokumen -</option>
                            <option value="Konsep Design" <?= $report->doc_type == 'Konsep Design' ? 'selected' : ''; ?>>Konsep Design</option>
                            <option value="RAB" <?= $report->doc_type == 'RAB' ? 'selected' : ''; ?>>RAB</option>
                            <option value="Gambar Teknis" <?= $report->doc_type == 'Gambar Teknis' ? 'selected' : ''; ?>>Gambar Teknis</option>
                            <option value="Surat Hibah" <?= $report->doc_type == 'Surat Hibah' ? 'selected' : ''; ?>>Surat Hibah</option>
                            <option value="SPPL" <?= $report->doc_type == 'SPPL' ? 'selected' : ''; ?>>SPPL</option>
                        </select>
                    </div>
                <?php endif; ?>

                <div class="form-group">
                    <label for="">Upload Dokumen</label>
                    <p><input type="hidden" readonly class="form-control-plaintext" id="reportFileArr" name="reportFileArr" value="<?= implode(",", json_decode($report->document)); ?>"></p>
                    <p> <?php foreach (json_decode($report->document) as $key => $val) : ?>
                            <a href="<?= $val; ?>" class="btn btn-success " target="_blank">Document <?= $report->doc_type; ?> - <?= $key + 1; ?></a>
                        <?php endforeach; ?>
                    </p>
                    <p><input class="show-for-sr" type="file" id="upload_file[]" name="upload_file[]" multiple accept=".doc, .docx, application/msword, application/vnd.ms-excel, application/vnd.ms-powerpoint,
text/plain, application/pdf" /><br>*ukuran file dibawah 20MB</p>

                </div>
                <hr>
                <button type="submit" class="btn btn-primary btn-icon-split">
                    <span class="icon text-white-50">
                        <i class="fas fa-save"></i>
                    </span>
                    <span class="text"> Simpan Data </span>
                </button>
                <a href="<?php echo base_url('report-monitoring') ?>" class="btn btn-secondary">Kembali</a>
            </form>
        </div>
    </div>
</div>
<script src="<?php echo base_url('assets/vendor/jquery/jquery.min.js'); ?>"></script>
<script type="text/javascript">
    var valueProgress = $('#progress').val();
    // alert(valueProgress);
    if (valueProgress != '100') {
        $('#vol1').hide();
        $('#vol2').hide();
    } else {
        $('#vol1').show();
        $('#vol2').show();
    }
</script>
<?php echo $this->endSection() ?> ?>
<?php echo $this->section('script') ?>
<script type="text/javascript">
    /* Cek Judul */
    $('.slider').each(function() {
        var $this = $(this);
        var $group = $this.find('.slide_group');
        var $slides = $this.find('.slide');
        var bulletArray = [];
        var currentIndex = 0;
        var timeout;

        function move(newIndex) {
            var animateLeft, slideLeft;

            advance();

            if ($group.is(':animated') || currentIndex === newIndex) {
                return;
            }

            bulletArray[currentIndex].removeClass('active');
            bulletArray[newIndex].addClass('active');

            if (newIndex > currentIndex) {
                slideLeft = '100%';
                animateLeft = '-100%';
            } else {
                slideLeft = '-100%';
                animateLeft = '100%';
            }

            $slides.eq(newIndex).css({
                display: 'block',
                left: slideLeft
            });
            $group.animate({
                left: animateLeft
            }, function() {
                $slides.eq(currentIndex).css({
                    display: 'none'
                });
                $slides.eq(newIndex).css({
                    left: 0
                });
                $group.css({
                    left: 0
                });
                currentIndex = newIndex;
            });
        }

        function advance() {
            clearTimeout(timeout);
            timeout = setTimeout(function() {
                if (currentIndex < ($slides.length - 1)) {
                    move(currentIndex + 1);
                } else {
                    move(0);
                }
            }, 4000);
        }

        $('.next_btn').on('click', function() {
            if (currentIndex < ($slides.length - 1)) {
                move(currentIndex + 1);
            } else {
                move(0);
            }
        });

        $('.previous_btn').on('click', function() {
            if (currentIndex !== 0) {
                move(currentIndex - 1);
            } else {
                move(3);
            }
        });

        $.each($slides, function(index) {
            var $button = $('<a class="slide_btn">&bull;</a>');

            if (index === currentIndex) {
                $button.addClass('active');
            }
            $button.on('click', function() {
                move(index);
            }).appendTo('.slide_buttons');
            bulletArray.push($button);
        });

        advance();
    });
    $('#jdl_lap').change(function() {
        var jdl_lap = $('#jdl_lap').val();
        var jns_lap = $('#jns_lap2').val();

        if ((jdl_lap == 'kas' || jdl_lap == 'bank') || jns_lap != 'Laporan Man. Keuangan') {
            document.getElementById("terima").disabled = false;
            $("#terima").attr("required", "");
        } else {
            $("#terima").val("");
            document.getElementById("terima").disabled = true;
        }
    });
    /* Cek Judul */

    /* Kegiatan */
    $("#pkt_lap").change(function() {
        var pkt_lap = $('#pkt_lap').val();
        var urll = '<?php echo base_url() ?>';

        $.ajax({
            url: urll + '/admin/report/reportCon/getSubProjectById',
            method: 'POST',
            data: {
                pkt_lap: pkt_lap
            },
            dataType: 'json',
            success: function(data) {

                var html = '';
                var i;
                html += '<option value="">- Pilih Kegiatan -</option>'
                for (i = 0; i < data.length; i++) {
                    html += '<option value="' + data[i].id + '">' + data[i].name + '</option>';
                }

                $('#kegiatanList').html(html);
            }
        });
        return false;
    });
    /* Kegiatan */

    /* Progress */
    $("#progress").change(function() {
        var prog = $('#progress').val();
        var jns_lap = $('#jns_lap').val();

        if (prog == '100' && jns_lap != 'keuangan') {
            // document.getElementById("rencana").disabled = false;
            // document.getElementById("satPlan").disabled = false;
            // document.getElementById("volReal").disabled = false;

            $("#vol1").show();
            $("#vol2").show();

            $("#rencana").attr("required", "");
            $("#satPlan").attr("required", "");
            $("#volReal").attr("required", "");
        } else if (prog != '100' && jns_lap != 'keuangan') {
            $("#rencana").removeAttr("required", "");
            $("#satPlan").removeAttr("required", "");
            $("#volReal").removeAttr("required", "");
            $("#vol1").hide();
            $("#vol2").hide();
        }
    });
    /* progress */
    var slideIndex = 1;
    showDivs(slideIndex);

    function input_revisi() {
        var rev = $('#revisi').val();

        console.log(rev);

        if (rev == '') {
            var i = confirm("Mohon isi catatan reivisi! ");
            if (i == true) {
                $('#revisiDiv').show();
            }
        } else {
            var j = confirm("Laporan akan di reivisi ?");
            var urll = '<?php echo base_url() ?>';
            if (j == true) {
                window.location = urll + '/admin/report/reportCon/reject/' + <?= $getReport->idoffline; ?> + "/" + rev;
            }
        }
    }

    function plusDivs(n) {
        showDivs(slideIndex += n);
    }

    function currentDiv(n) {
        showDivs(slideIndex = n);
    }

    function showDivs(n) {
        var i;
        var x = document.getElementsByClassName("mySlides");
        var dots = document.getElementsByClassName("demo");
        if (n > x.length) {
            slideIndex = 1
        }
        if (n < 1) {
            slideIndex = x.length
        }
        for (i = 0; i < x.length; i++) {
            x[i].style.display = "none";
        }
        for (i = 0; i < dots.length; i++) {
            dots[i].className = dots[i].className.replace(" w3-red", "");
        }
        x[slideIndex - 1].style.display = "block";
        dots[slideIndex - 1].className += " w3-red";
    }

    $(document).ready(function() {


        /* Hide Show Kegiatan */
        $('#jns_lap').change(function() {
            var jns_lap = $('#jns_lap').val();

            if (jns_lap == "keuangan") {
                var keu = '';

                // reset field
                $("#hibah").val("");
                $("#lokasi").val("");
                $("#kegiatanList").val("");
                $("#pkt_lap").val("");
                $("#progress").val("");

                $("#rencana").val("");
                $("#satPlan").val("");
                $("#volReal").val("");
                $('#sat_real').text("");
                $('#nmTerima').text("Saldo Terakhir (Rp)");

                // hide input
                $('#vol2').hide();
                $('#vol1').hide();
                $('#progressSlct').hide();
                $('#tgl1').hide();
                $('#pkt_lap1').hide();
                $('#lahan').hide();
                $('#lokasidiv').hide();
                $('#kegiatan').hide();
                $('#dok_name').hide();
                $('#date_infra').hide();
                $('#dok_name_input').show();
                $('#date_keu').show();

                // list judu;
                keu += '<option value="">- Pilih Judul -</option>'
                keu += '<option value="kas">Buku Kas</option>'
                keu += '<option value="bank">Buku Bank</option>'
                keu += '<option value="dkis">Buku DKIS</option>'
                keu += '<option value="rekening_bank">Buku Rekening Bank KSM</option>'
                keu += '<option value="lppu">LPPU</option>'

                $('#jdl_lap').html(keu);
            }

            if (jns_lap == "infrastruktur" || jns_lap == "") {
                var infra = '';

                // reset field
                $("#progress").val("");

                // enable input
                document.getElementById("pkt_lap").disabled = false;
                document.getElementById("str_date").disabled = false;
                document.getElementById("end_date").disabled = false;
                document.getElementById("progress").disabled = false;

                // required input
                $("#pkt_lap").attr("required", "");
                $("#str_date").attr("required", "");
                $("#end_date").attr("required", "");
                $("#dok_nameList").attr("required", "");
                $("#hibah").attr("required", "");
                $("#lokasi").attr("required", "");
                $("#kegiatan").attr("required", "");
                $("#terima").attr("required", "");

                // show input
                $('#vol2').show();
                $('#vol1').show();
                $('#progressSlct').show();
                $('#tgl1').show();
                $('#pkt_lap1').show();
                $('#lahan').show();
                $('#lokasidiv').show();
                $('#kegiatan').show();
                $('#dok_name').show();
                $('#dok_name_input').hide();
                $('#date_infra').show();
                $('#date_keu').hide();
                $('#nmTerima').text("Penerimaan Rekening Bank KSM (Rp)");

                infra += '<option value="">- Pilih Judul -</option>'
                infra += '<option value="foto_progress">Foto Progress</option>'
                infra += '<option value="kemajuan_fisik">Kemajuan Fisik</option>'
                $('#jdl_lap').html(infra);
            }
        });
        /* Hide Show Kegiatan */



        /* satPlan */
        $("#satPlan").change(function() {
            var satPlan = $('#satPlan').val();
            $('#sat_real').text(satPlan);
        });
        /* satPlan */
    });
</script>
<?php echo $this->endSection() ?>