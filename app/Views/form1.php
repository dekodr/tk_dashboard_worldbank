<?php echo $this->extend('base/template'); ?>

<?php echo $this->section('content') ?>
<!-- Page Heading -->

<div class="d-sm-flex align-items-center justify-content-between mb-4">
	<h1 class="h3 mb-0 text-gray-800">Complaint Handling</h1>
</div>

<?php if (session()->getFlashdata('error')) { ?>
	<div class="card mb-4 py-3 border-left-danger">
		<div class="card-body">
			<?php print_r(session()->getFlashdata('error')) ?>
		</div>
	</div>
<?php } ?>

<!-- DataTales Example -->
<div class="card shadow mb-4">
	<div class="card-header py-3">
		<h6 class="m-0 font-weight-bold text-primary">Form - Input Fasilitator</h6>
	</div>
	<div class="card-body row">
		<div class="col-xl-8 col-lg-7">
			<!-- FORM HERE -->

			<div class="wrapperBody">
				<nav class="form-steps">
					<div class="form-steps__item step-1 form-steps__item--completed">
						<div class="form-steps__item-content">
							<span class="form-steps__item-icon">1</span>
							<span class="form-steps__item-text">Registrasi</span>
						</div>
					</div>
			
					<div class="form-steps__item step-2">
						<div class="form-steps__item-content">
							<span class="form-steps__item-icon">2</span>
							<span class="form-steps__item-line"></span>
							<span class="form-steps__item-text">Data Pelapor/Penerima</span>
						</div>
					</div>
			
					<div class="form-steps__item step-3">
						<div class="form-steps__item-content">
							<span class="form-steps__item-icon">3</span>
							<span class="form-steps__item-line"></span>
							<span class="form-steps__item-text">Isi Pengaduan</span>
						</div>
					</div>
				</nav>
			
				<div class="container">

					<form action="#" method="post"  class="user" accept-charset="utf-8" id="raq_questions">
			
						<div class="question-container active_panel" id="question-1">
							<!-- step 1 -->
							<div class="question-title">Registrasi</div>
							
							<label for="exampleFormControlSelect1">Nomor Register (*)</label>
							<div class="form-group">
								<input name="name" type="text" class="form-control form-control-user" id="name" placeholder="Nomor Register (*)" value="">
							</div>
							<hr>
							
							<label for="exampleFormControlSelect1">Nama Prov (*)</label>
							<div class="form-group">
								<input name="name" type="text" class="form-control form-control-user" id="name" placeholder="Nama Prov (*)" value="">
							</div>
							<hr>
							
							<label for="exampleFormControlSelect1">Kode Prov (*)</label>
							<div class="form-group">
								<input name="name" type="text" class="form-control form-control-user" id="name" placeholder="Kode Prov (*)" value="">
							</div>
							<hr>
							
							<label for="exampleFormControlSelect1">Nama Kota/Kabupaten (*)</label>
							<div class="form-group">
								<input name="name" type="text" class="form-control form-control-user" id="name" placeholder="Nama Kota/Kabupaten (*)" value="">
							</div>
							<hr>
							
							<label for="exampleFormControlSelect1">Kode Kota/Kab (*)</label>
							<div class="form-group">
								<input name="name" type="text" class="form-control form-control-user" id="name" placeholder="Kode Kota/Kab (*)" value="">
							</div>
							<hr>
							
							<label for="exampleFormControlSelect1">Nama Kecamatan (*)</label>
							<div class="form-group">
								<input name="name" type="text" class="form-control form-control-user" id="name" placeholder="Nama Kecamatan (*)" value="">
							</div>
							<hr>
							
							<label for="exampleFormControlSelect1">Kode Kecamatan (*)</label>
							<div class="form-group">
								<input name="name" type="text" class="form-control form-control-user" id="name" placeholder="Kode Kecamatan (*)" value="">
							</div>
							<hr>
							
							<label for="exampleFormControlSelect1">Nama Kelurahan (*)</label>
							<div class="form-group">
								<input name="name" type="text" class="form-control form-control-user" id="name" placeholder="Nama Kelurahan (*)" value="">
							</div>
							<hr>
							
							<label for="exampleFormControlSelect1">Kode Kelurahan (*)</label>
							<div class="form-group">
								<input name="name" type="text" class="form-control form-control-user" id="name" placeholder="Kode Kelurahan (*)" value="">
							</div>
							<hr>
							
							<label for="exampleFormControlSelect1">OSP/OC</label>
							<div class="form-group">
								<input name="name" type="text" class="form-control form-control-user" id="name" placeholder="OSP/OC" value="">
							</div>
							<hr>
							
						</div>

						<div class="question-container" id="question-2">
							<!-- step 2 -->
							<label for="exampleFormControlSelect1">Data Penerima</label>

							<label for="exampleFormControlSelect1">Nama Penerima (*)</label>
							<div class="form-group">
								<input name="name" type="text" class="form-control form-control-user" id="name" placeholder="Nama Penerima (*)" value="">
							</div>
							<hr>
							<label for="exampleFormControlSelect1">Tanggal Pengaduan (*)</label>
							<div class="form-group">
								<input name="name" type="text" class="form-control form-control-user" id="name" placeholder="Tanggal Pengaduan (*)" value="">
							</div>
							<hr>
							<label for="exampleFormControlSelect1">Sumber (*)</label>
							<div class="form-group">
								<input name="name" type="text" class="form-control form-control-user" id="name" placeholder="Sumber (*)" value="">
							</div>
							<hr>
							<label for="exampleFormControlSelect1">Media (*)</label>
							<div class="form-group">
								<input name="name" type="text" class="form-control form-control-user" id="name" placeholder="Media (*)" value="">
							</div>
							<hr>
							<hr>

							<label for="exampleFormControlSelect1">Data Pengadu/Pelapor</label>

							<label for="exampleFormControlSelect1">Nama Pengadu/Pelapor (*)</label>
							<div class="form-group">
								<input name="name" type="text" class="form-control form-control-user" id="name" placeholder="Nama Pengadu/Pelapor (*)" value="">
							</div>
							<hr>
							<label for="exampleFormControlSelect1">Jenis Kelamin (*)</label>
							<div class="form-group">
								<input name="name" type="text" class="form-control form-control-user" id="name" placeholder="Jenis Kelamin (*)" value="">
							</div>
							<hr>
							<label for="exampleFormControlSelect1">Status Pengadu/Pelapor (*)</label>
							<div class="form-group">
								<input name="name" type="text" class="form-control form-control-user" id="name" placeholder="Status Pengadu/Pelapor (*)" value="">
							</div>
							<hr>
							<label for="exampleFormControlSelect1">Alamat Pengadu/Pelapor (*)</label>
							<div class="form-group">
								<input name="name" type="text" class="form-control form-control-user" id="name" placeholder="Alamat Pengadu/Pelapor (*)" value="">
							</div>
							<hr>
							<label for="exampleFormControlSelect1">Kode Pos</label>
							<div class="form-group">
								<input name="name" type="number" class="form-control form-control-user" id="name" placeholder="Kode Pos" value="">
							</div>
							<hr>
							<label for="exampleFormControlSelect1">Telepon</label>
							<div class="form-group">
								<input name="name" type="text" class="form-control form-control-user" id="name" placeholder="Telepon" value="">
							</div>
							<hr>
							<label for="exampleFormControlSelect1">Email</label>
							<div class="form-group">
								<input name="name" type="email" class="form-control form-control-user" id="name" placeholder="Email" value="">
							</div>
							<hr>
						</div>

						<div class="question-container" id="question-3">
							<!-- step 3 -->

							<div class="question-title">Isi Pengaduan</div>
							
							<label for="exampleFormControlSelect1">Isi Pengaduan (*)</label>
							<div class="form-group">
								<input name="name" type="text" class="form-control form-control-user" id="name" placeholder="Isi Pengaduan (*)" value="">
							</div>
							<hr>

							<label for="exampleFormControlSelect1">Kata Kunci Pengaduan (*)</label>
							<div class="form-group">
								<input name="name" type="text" class="form-control form-control-user" id="name" placeholder="Kata Kunci Pengaduan (*)" value="">
							</div>
							<hr>
							
						</div>
			
					</form>
			
			
				</div>
			
				<div class="button-bar">
					<input type="button" value="Back" id="raq_back">
					<input type="button" value="Next" id="raq_next">
				</div>
				<div id="overlay">
					<div class="thankyou">
						<h3>Thank You!</h3>
						<p>Thank you, your form is submited</p>
						<input type="button" id="start-over" value="Start Over">
					</div>
				</div>
			</div>
		</div>
	</div>
</div>


<!-- custom script and style -->
<?php echo $this->endSection() ?> ?>
<?php echo $this->section('script') ?>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/173252/velocity.js"></script>
<script src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/173252/snap_svg.js"></script>
<script>
	/*
		IMPORTANT
		because we don't use Babel here, we have to stick
		to old syntax to let older browsers run this code
	*/
	$(function() {

		makeButtonDisable('#raq_back');

		focusOnCurrentAnswer();

		var counter = 1;
		var numberOfQuestions = 3;
		$('#raq_next').click(function(){
			if( !validate() )
				return false;

			makeButtonEnable('#raq_back');
			
			// check if the user is in the last question
			if( $(this).attr('isSubmit') == 'true' ) {
				submitForm();
			}
			
			showElement('#question-' + ++counter);

			nextNavigation(counter);
			changeQuestionTitle(counter);
			
			if( counter == numberOfQuestions ) {
				// show submit button
				showSubmitButton();
			}
			focusOnCurrentAnswer();
		});
		$('#raq_back').click(function() {
			removeError();

			if( $('#raq_back').hasClass('disabled') )
				return false;
			
			if( $('#raq_next').attr('isSubmit') == 'true' )
				showNextButton();

			showElement('#question-' + --counter);

			backNavigation(counter);
			changeQuestionTitle(counter);

			if( counter == 1 )
				makeButtonDisable('#raq_back');
		});

		// By doing this users can use enter key instead of clicking the mouse
		accessibility();

		pageReload();
	});


	function fadeAll() {
		$('form .question-container').each(function(){
			$(this).hide();
		});
	}
	function fadeIn(theElement) {
		$(theElement).fadeIn(700);
	}
	function showElement(theElement) {
		fadeAll();
		fadeIn(theElement);
	}
	function showSubmitButton() {
		$('#raq_next').prop('value', 'Submit');
		$('#raq_next').addClass('submit');
		$('#raq_next').attr('isSubmit', 'true');
	}
	function showNextButton() {
		$('#raq_next').prop('value', 'Next');
		$('#raq_next').removeClass('submit');
		$('#raq_next').attr('isSubmit', 'false');
	}
	function makeButtonDisable(button) {
		$(button).addClass('disabled');
	}
	function makeButtonEnable(button) {
		$(button).removeClass('disabled');
	}
	function nextNavigation(currentQuestion) {
		$('.form-steps__item.step-' + currentQuestion).addClass('form-steps__item--active');
		$('.form-steps__item.step-' + (currentQuestion-1) + ' .form-steps__item-text').after('<div class="tick"></div>');
	}
	function backNavigation(currentQuestion) {
		$('.form-steps__item.step-' + (currentQuestion + 1) ).removeClass('form-steps__item--active');
		$('.form-steps__item.step-' + currentQuestion + ' .tick' ).remove();
	}
	function validate() {
		if( $('input[type="text"]:visible').val().length <= 0 ) {
			$('input[type="text"]:visible').addClass('error');
			if( !$('.error-text').length )
				$('input.error').after('<p class="error-text">This Field is Required</p>');
			return false;
		}
		else {
			$('input[type="text"]:visible').removeClass('error');
			$('.error-text').remove();
			return true;
		}
	}
	function changeQuestionTitle(counter) {
		// $('.question-title').text('Question ' + counter);
	}
	function showThankyou() {
		$('#overlay').show();
	}
	function showThankyou() {
		$('#overlay').show();
	}
	function focusOnCurrentAnswer() {
		$('input[type="text"]:visible').focus();
	}
	function accessibility() {
		$('input[type="text"]').keypress(function(event){
			var keycode = (event.keyCode ? event.keyCode : event.which);
			if(keycode == '13')
				$('#raq_next').click();
		});
	}
	function removeError() {
		$('input[type="text"]:visible').removeClass('error');
		$('.error-text').remove();
	}
	function submitForm() {
		/*
			Submit, But do not refresh the page
			to show the thank you message
		*/
		$('form').submit(function(e) {
			e.preventDefault();
		});
		showThankyou();
		return false;
	}
	function pageReload() {
		$('#start-over').click(function(){
			window.location.reload();
		});
	}
	function printLegacy() {
		console.log(
			"%c Mostafa Ghanbari %c \nWith Respect",
			"background-image: linear-gradient(90deg, red, blue);background-size: 210px 210px;background-position: 0% 0%;min-height: 100vh;font-size: 16px;color:#FFF;margin: 5px 0;padding:5px; padding-right: 10px;border-radius: 5px;line-height: 26px;",
			""
		);
	}
</script>
<?php echo $this->endSection() ?> ?>
<?php echo $this->section('css') ?>
<style>
	/****************************
		Global Styling
	*****************************/
	.wrapperBody {
		/* max-width: 600px; */
		margin: auto;

		-webkit-font-smoothing: antialiased;
		-moz-osx-font-smoothing: grayscale;

		-webkit-text-stroke: 0.45px;
		-webkit-text-stroke: 0.45px rgba(0, 0, 0, 0.1);
		text-shadow: #fff 0px 1px 1px;
	}
	.wrapperBody .form-steps {
		display: block;
		width: 100%;
		position: relative;
		margin: 40px 0;
	}
	.wrapperBody .form-steps:after {
		content: "";
		display: table;
		clear: both;
	}
	.wrapperBody .form-steps__item {
		padding: 0;
		position: relative;
		display: block;
		float: left;
		width: 20%;
		text-align: center;
	}
	.wrapperBody .form-steps__item-content {
		display: inline-block;
	}
	.wrapperBody .form-steps__item-icon {
		background: #e4e5e5;
		color: #8191ab;
		display: block;
		border-radius: 100%;
		border: 1px solid;
		border-color: rgb(129, 145, 171) !important;
		text-align: center;
		width: 25px;
		height: 25px;
		line-height: 25px;
		margin: 0 auto 10px auto;
		position: relative;
		font-size: 13px;
		font-weight: 700;
		z-index: 2;
	}
	.wrapperBody .form-steps__item-text {
		font-size: 13px;
		color: #8191ab;
		font-weight: 500;
	}
	.wrapperBody .form-steps__item-line {
		display: inline-block;
		height: 3px;
		width: 100%;
		background: #cfd8dc;
		position: absolute;
		left: -50%;
		top: 12px;
		z-index: 1;
	}
	.wrapperBody .form-steps__item--active .form-steps__item-icon {
		background: #00aeef;
		color: #ffffff;
	}
	.wrapperBody .form-steps__item--active .form-steps__item-line {
		-webkit-transition: background-color .5s ease;
		-moz-transition: background-color .5s ease;
		-o-transition: background-color .5s ease;
		transition: background-color .5s ease;
	}
	.wrapperBody .form-steps__item--active .form-steps__item-icon {
		-webkit-transition: background-color 1s ease;
		-moz-transition: background-color 1s ease;
		-o-transition: background-color 1s ease;
		transition: background-color 1s ease;
	}
	.wrapperBody .form-steps__item--completed .form-steps__item-text {
		color: #4f5e77;
	}
	.wrapperBody .form-steps__item--active .form-steps__item-text {
		color: #4f5e77;
	}
	.wrapperBody .form-steps__item--active .form-steps__item-line {
		background: #00aeef;
	}
	.wrapperBody .form-steps__item--completed .form-steps__item-icon {
		background: #00aeef;
		color: #fff;
		background-size: 10px;
		background-repeat: no-repeat;
		background-position: center center;
		width: 25px;
		height: 25px;
		line-height: 25px;
	}
	.wrapperBody .form-steps__item--completed .form-steps__item-line {
		background: #00aeef;
	}


	.wrapperBody .container {
		/* border: 1px solid #dddddd; */
		background: #fff;
		border-radius: 4px;
		width: 85%;
		/* height: 230px; */
		margin: auto;
		padding: 30px 40px;
		margin-bottom: 70px;
		position: relative;
		/* box-shadow: 0 3px 10px -2px rgba(0, 0, 0, 0.17); */
	}

	.wrapperBody input[type="text"] {
		z-index: 1;
		margin-top: 10px;
		/* font-size: .8em;
		outline: 0;
		-webkit-tap-highlight-color: rgba(255,255,255,0);
		text-align: left;
		line-height: 1.2em;
		padding: .5em;
		background: #fff;
		border: 1px solid rgba(34,36,38,.15);
		color: rgba(0,0,0,.87);
		border-radius: .28571429rem;
		-webkit-box-shadow: none;
		box-shadow: none; */
	}
	.wrapperBody input[type="text"]:focus {
		border-color: #85b7d9;
		background: #fff;
		color: rgba(0,0,0,.8);
		-webkit-box-shadow: none;
		box-shadow: none;
	}

	.wrapperBody input[type="button"] {
		box-shadow: 0 0 0 0 rgba(34,36,38,.15) inset;
		font-size: 1rem;
		cursor: pointer;
		min-height: 1em;
		outline: 0;
		border: none;
		background: #e0e1e2 none;
		color: rgba(0,0,0,.6);
		margin: 0 .25em 0 0; padding: .788em 1.5em .788em;
		font-weight: 700;
		line-height: 1em;
		font-style: normal;
		text-align: center;
		border-radius: .285rem;
		-webkit-box-shadow: 0 0 0 1px transparent inset, 0 0 0 0 rgba(34,36,38,.15) inset;
		box-shadow: 0 0 0 1px transparent inset, 0 0 0 0 rgba(34,36,38,.15) inset;
	}
	input.error,
	input[type="text"].error:focus {
		background-color: #fff6f6;
		border-color: #e0b4b4;
		color: #9f3a38;
	}
	p.error-text {
		position: absolute;
		bottom: -23px;
		left: 24px;
		color: rgba(255, 0, 0, .7);
		font-size: .6em;
	}
	#raq_next {
		background-color: #2185d0;
		color: #fff;
		text-shadow: none;
		background-image: none;
	}

	#raq_next.submit {
		background-color: #21ba45;
	}
	#raq_back.disabled {
		color: #bbbbbb;
		cursor: unset;
	}


	.question-container {
		display: none;
	}
	.active_panel {
		display: block;
	}
	.question-container {
		position: relative;
		font-weight: 500;
		font-size: 1.2rem;
		color: #434343;
		line-height: 30px;
		margin: 0;
	}

	.button-bar {
		position: absolute;
		bottom: 25px;
	}

	.tick {
		position: absolute;
		top: 5px;
		left: 50px;
		box-shadow: inset 3px -3px 0 green;
		height: 8px;
		transform: rotate(-50deg);
		width: 20px;
		margin: 50px auto;
		transition: all 1s;
	}

	.question-title {
		font-weight: 700;
		font-size: 2rem;
		color: #2785d0;
		line-height: 30px;
		margin-bottom: 20px;
	}

	#overlay {
		position: fixed; /* Sit on top of the page content */
		display: none;
		width: 100%;
		height: 100%;
		top: 0; 
		left: 0;
		right: 0;
		bottom: 0;
		background-color: rgba(0,0,0,0.5);
		z-index: 2;
		cursor: pointer;
	}

	/****************************
		Thank You Box
	*****************************/
	.thankyou {
		margin: auto;
		width: 260px;
		height: 160px;
		margin-top: 190px;
		background: #fff;
		padding: 15px 20px;
		line-height: 25px;
		border-radius: 4px;
		text-align: center;
		box-shadow: -1px 5px 32px 7px rgba(0, 0, 0, 0.5);
	}
	.thankyou input {
		margin-top: 40px;
	}
	.thankyou h3 {
		font-size: 3em;
		font-weight: 700;
		color: #21ba45;
		line-height: 50px;
	}

</style>
<?php echo $this->endSection() ?> ?>