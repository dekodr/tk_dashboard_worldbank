<?php

namespace App\Models;

use CodeIgniter\Database\ConnectionInterface;
use CodeIgniter\Model;

class DistrictModel extends Model
{
    protected $table            = 'ms_kabupaten';
    protected $primaryKey       = 'id';
    protected $allowedFields    = ['id', 'id_province', 'code', 'name', 'delete'];


    public function get_data($id = null)
    {
        $query = "select a.*,b.name province from ms_kabupaten a left join ms_provinsi b on b.id=a.id_province ";
        if ($id !== null) {
            $query .= " WHERE a.id = " . $id;
            $data = $this->query($query)->getRow();
            return $data;
        } else {
            $query .= " WHERE a.delete = 0 ";
            $data = $this->query($query)->getResult();
            return $data;
        }
    }

    public function insert_district($_param)
    {
        //saving data
        $this->save($_param);
        return TRUE;
    }

    public function getKabByProvinceId($prov_id)
    {
        $query = $this->where(['id_province' => $prov_id, 'delete' => '0'])->get()->getResult();
        return $query;
    }
}
