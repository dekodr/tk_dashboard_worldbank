<?php

namespace App\Models;

use CodeIgniter\Model;

class GraphModel extends Model
{
	protected $table = 'ms_report';
	protected $primaryKey = 'idoffline';
	protected $allowedFields = [
		'title', 'fund_utilization', 'description', 'is_publish', 'delete', 'is_approval', 'id_user',
		'start_date', 'end_date', 'type', 'progress', 'hibah', 'vol_plan',
		'vol_real', 'doc_type', 'document', 'images', 'id_project', 'id_sub_project', 'id_project_loc',
		'status_date', 'vol_unit', 'progress_month'
	];
	protected $session = null;
	protected $db;

	public function __construct()
	{
		$this->session = session();
		$this->db = \Config\Database::connect();
	}

	public function get_report($id = null)
	{
		if ($id !== null) {
			$data = $this->where(['idoffline' => $id])->get()->getRow();
			return $data;
		} else {
			$data = $this->where(['delete' => '0'])->get()->getResult();
			return ($data);
		}
	}

	public function report_ksm()
	{
		$list_ksm   = $this->list_ksm();
		$data       = array();

		// print_r($list_ksm);
		foreach ($list_ksm as $key => $value) {
			$total = $this
				->join('ms_user', 'ms_user.id = ms_report.id_user', 'LEFT')
				->join('ms_ksm', 'ms_ksm.id = ms_user.id_ksm', 'LEFT')
				// ->where(['ms_report.delete' => '0', 'ms_ksm.id' => $value->id])
				->where('ms_report.delete', '0')
				->where('ms_ksm.id', $value->id)
				->countAllResults();

			$data['label'][] = $value->name;
			$data['value'][] = $total;
		}

		return (json_encode($data));
	}

	public function report_province()
	{
		$list_province   = $this->list_province();
		$data       = array();

		foreach ($list_province as $key => $value) {
			$total = $this
				->join('ms_user', 'ms_user.id = ms_report.id_user', 'LEFT')
				->join('ms_ksm', 'ms_ksm.id = ms_user.id_ksm', 'LEFT')
				->join('ms_kelurahan', 'ms_kelurahan.id = ms_ksm.id_kelurahan', 'LEFT')
				->join('ms_kabupaten', 'ms_kabupaten.id = ms_kelurahan.id_kabupaten', 'LEFT')
				->where('ms_report.delete', '0')
				->where('ms_kabupaten.id_province', $value->id)
				->where('ms_report.type', 'keuangan')
				->countAllResults();

			$total_infra = $this
				->join('ms_user', 'ms_user.id = ms_report.id_user', 'LEFT')
				->join('ms_ksm', 'ms_ksm.id = ms_user.id_ksm', 'LEFT')
				->join('ms_kelurahan', 'ms_kelurahan.id = ms_ksm.id_kelurahan', 'LEFT')
				->join('ms_kabupaten', 'ms_kabupaten.id = ms_kelurahan.id_kabupaten', 'LEFT')
				->where('ms_report.delete', '0')
				->where('ms_kabupaten.id_province', $value->id)
				->where('ms_report.type', 'infrastruktur')
				->countAllResults();

			// if($total > 0){
			$data[$value->name]['keuangan'] = $total;
			$data[$value->name]['infrastruktur'] = $total_infra;
			// }
		}
		// print_r($total);die;
		foreach ($data as $key => $value) {
			if ($value['keuangan'] == '' && $value['infrastruktur'] == '') {
				unset($data[$key]);
			}
		}

		return $data;
	}

	public function report_kabupaten()
	{
		$list_kabupaten   = $this->list_kabupaten();
		$data       = array();

		foreach ($list_kabupaten as $key => $value) {
			$total = $this
				->join('ms_user', 'ms_user.id = ms_report.id_user', 'LEFT')
				->join('ms_ksm', 'ms_ksm.id = ms_user.id_ksm', 'LEFT')
				->join('ms_kelurahan', 'ms_kelurahan.id = ms_ksm.id_kelurahan', 'LEFT')
				->join('ms_kabupaten', 'ms_kabupaten.id = ms_kelurahan.id_kabupaten', 'LEFT')
				->where('ms_report.delete', '0')
				->where('ms_kabupaten.id', $value->id)
				->where('ms_report.type', 'keuangan')
				->countAllResults();

			$total_infra = $this
				->join('ms_user', 'ms_user.id = ms_report.id_user', 'LEFT')
				->join('ms_ksm', 'ms_ksm.id = ms_user.id_ksm', 'LEFT')
				->join('ms_kelurahan', 'ms_kelurahan.id = ms_ksm.id_kelurahan', 'LEFT')
				->join('ms_kabupaten', 'ms_kabupaten.id = ms_kelurahan.id_kabupaten', 'LEFT')
				->where('ms_report.delete', '0')
				->where('ms_kabupaten.id', $value->id)
				->where('ms_report.type', 'infrastruktur')
				->countAllResults();

			// if($total > 0){
			$data[$value->name]['keuangan'] = $total;
			$data[$value->name]['infrastruktur'] = $total_infra;
			// }
		}
		// print_r($total);die;
		foreach ($data as $key => $value) {
			if ($value['keuangan'] == '' && $value['infrastruktur'] == '') {
				unset($data[$key]);
			}
		}

		return $data;
	}

	public function report_kel()
	{
		$list_kel   = $this->list_kel();
		$data       = array();

		foreach ($list_kel as $key => $value) {
			$total = $this
				->join('ms_user', 'ms_user.id = ms_report.id_user', 'LEFT')
				->join('ms_ksm', 'ms_ksm.id = ms_user.id_ksm', 'LEFT')
				->where('ms_report.delete', '0')
				->like('ms_ksm.kode_ksm', $value->code, 'after')
				->countAllResults();
			// print_r($total);

			if ($total > 0) {
				$data['label'][] = $value->name;
				$data['value'][] = $total;
			}
		}

		return json_encode($data);
	}

	public function reportByType()
	{
		$list_kel   = $this->list_kel();
		$data       = array();
		// print_r($list_kel);die;
		foreach ($list_kel as $key => $value) {
			$total = $this
				->join('ms_user', 'ms_user.id = ms_report.id_user', 'LEFT')
				->join('ms_ksm', 'ms_ksm.id = ms_user.id_ksm', 'LEFT')
				->where('ms_report.delete', '0')
				->like('ms_ksm.kode_ksm', $value->code, 'after')
				->where('ms_report.type', 'keuangan')
				->countAllResults();

			$total_infra = $this
				->join('ms_user', 'ms_user.id = ms_report.id_user', 'LEFT')
				->join('ms_ksm', 'ms_ksm.id = ms_user.id_ksm', 'LEFT')
				->where('ms_report.delete', '0')
				->like('ms_ksm.kode_ksm', $value->code, 'after')
				->where('ms_report.type', 'infrastruktur')
				->countAllResults();

			// if($total > 0){
			$data[$value->name]['keuangan'] = $total;
			$data[$value->name]['infrastruktur'] = $total_infra;
			// }
		}
		// print_r($total);die;
		foreach ($data as $key => $value) {
			if ($value['keuangan'] == '' && $value['infrastruktur'] == '') {
				unset($data[$key]);
			}
		}

		return $data;
	}

	public function report_project()
	{

		$list_project   = $this->list_project();
		$data           = array();

		foreach ($list_project as $key => $value) {
			$total = $this
				->join('ms_user', 'ms_user.id = ms_report.id_user', 'LEFT')
				->join('ms_ksm', 'ms_ksm.id = ms_user.id_ksm', 'LEFT')
				->where('ms_report.delete', '0')
				// ->where('ms_ksm.id is not null')
				->where('ms_report.id_project', $value->id)
				// ->groupBy('ms_report.id_project')
				->countAllResults();
			$data[$value->name] = $total;
		}

		return ($data);
	}

	public function total_report()
	{
		$id_role = $this->session->id_role;
		$id = $this->session->id;

		$base_query = "	SELECT 
							a.*
						FROM 
							ms_report a
						LEFT JOIN
							ms_user b ON b.id=a.id_user
						JOIN
							ms_ksm c on c.id = b.id_ksm
						WHERE
							a.delete = 0 AND a.report_type::int != 2";

		if ($id_role == 2) {
			$base_query .= " AND a.id_user = " . $id;
		}

		$q_total_infra = $base_query;
		$q_total_infra .= " AND a.type = 'infrastruktur' ";

		$q_total_eco = $base_query;
		$q_total_eco .= " AND a.type = 'keuangan' ";

		$q_total_eco_approve = $base_query;
		$q_total_eco_approve .= " a.is_publish is TRUE AND a.type = 'keuangan' ";

		$data['total']		= count($this->db->query($base_query)->getResult());

		$data['total_infra'] = count($this->db->query($q_total_infra)->getResult());

		$data['total_eco']	= count($this->db->query($q_total_eco)->getResult());

		$data['total_eco_approve']	= count($this->db->query($q_total_eco_approve)->getResult());

		$data['total_ksm']	= count($this->list_ksm());

		return $data;
	}

	public function progress()
	{
	}

	public function list_province()
	{
		$db = \Config\Database::connect();
		$data = $db->table('ms_provinsi')
			->where('ms_provinsi.delete', '0')
			->get()->getResult();
		return $data;
	}

	public function list_kabupaten()
	{
		$db = \Config\Database::connect();
		$data = $db->table('ms_kabupaten')
			->where('delete', '0')
			->get()->getResult();
		return $data;
	}

	public function list_ksm()
	{
		$db = \Config\Database::connect();
		$data = $db->table('ms_ksm')
			->where('ms_ksm.delete', '0')
			->get()->getResult();
		return $data;
	}

	public function list_kel()
	{
		$db = \Config\Database::connect();
		$data = $db->table('ms_kelurahan')
			->get()->getResult();
		return $data;
	}

	public function list_project()
	{
		$db = \Config\Database::connect();
		$data = $db->table('tb_project')
			->get()->getResult();
		return $data;
	}

	public function filter_dashboard($param)
	{
		$start_date = $param['start_date'];
		$end_date = $param['end_date'];

		$data['total']			= $this->join('ms_user', 'ms_user.id = ms_report.id_user', 'LEFT')
			->join('ms_ksm', 'ms_ksm.id = ms_user.id_ksm', 'LEFT')
			->where('ms_report.delete', '0')
			->where('ms_report.start_date >=', $start_date)
			->where('ms_report.end_date <=', $end_date)
			// ->where('ms_ksm.id is not null')
			->countAllResults();

		$data['total_infra']	= $this->join('ms_user', 'ms_user.id = ms_report.id_user', 'LEFT')
			->join('ms_ksm', 'ms_ksm.id = ms_user.id_ksm', 'LEFT')
			->where('ms_report.delete', '0')
			->where('ms_report.start_date >=', $start_date)
			->where('ms_report.end_date <=', $end_date)
			// ->where('ms_ksm.id is not null')
			->where('ms_report.type', 'infrastruktur')
			->countAllResults();

		$data['total_eco']		= $this->join('ms_user', 'ms_user.id = ms_report.id_user', 'LEFT')
			->join('ms_ksm', 'ms_ksm.id = ms_user.id_ksm', 'LEFT')
			->where('ms_report.delete', '0')
			// ->where('ms_ksm.id is not null')
			->where('ms_report.start_date >=', $start_date)
			->where('ms_report.end_date <=', $end_date)
			->where('ms_report.type', 'keuangan')
			->countAllResults();

		$data['total_eco_approve']		= $this->join('ms_user', 'ms_user.id = ms_report.id_user', 'LEFT')
			->join('ms_ksm', 'ms_ksm.id = ms_user.id_ksm', 'LEFT')
			->where('ms_report.delete', '0')
			->where('ms_report.start_date >=', $start_date)
			->where('ms_report.end_date <=', $end_date)
			// ->where('ms_ksm.id is not null')
			->where('ms_report.is_publish is TRUE')
			->where('ms_report.type', 'keuangan')
			->countAllResults();

		$data['total_ksm']		= count($this->list_ksm());

		$list_ksm   = $this->list_ksm();

		// print_r($list_ksm);
		foreach ($list_ksm as $key => $value) {
			$total = $this
				->join('ms_user', 'ms_user.id = ms_report.id_user', 'LEFT')
				->join('ms_ksm', 'ms_ksm.id = ms_user.id_ksm', 'LEFT')
				->where('ms_report.start_date >=', $start_date)
				->where('ms_report.end_date <=', $end_date)
				// ->where(['ms_report.delete' => '0', 'ms_ksm.id' => $value->id])
				->where('ms_report.delete', '0')
				->where('ms_ksm.id', $value->id)
				->countAllResults();

			$a['label'][] = $value->name;
			$a['value'][] = $total;
		}

		$data['ksm'] = $a;

		$list_kel   = $this->list_kel();

		foreach ($list_kel as $key => $value) {
			$total = $this
				->join('ms_user', 'ms_user.id = ms_report.id_user', 'LEFT')
				->join('ms_ksm', 'ms_ksm.id = ms_user.id_ksm', 'LEFT')
				->where('ms_report.delete', '0')
				->where('ms_report.start_date >=', $start_date)
				->where('ms_report.end_date <=', $end_date)
				->like('ms_ksm.kode_ksm', $value->code, 'after')
				->countAllResults();

			if ($total > 0) {
				$b['label'][] = $value->name;
				$b['value'][] = $total;
			}
		}

		$data['kel'] = $b;

		$list_project   = $this->list_project();

		foreach ($list_project as $key => $value) {
			$total = $this
				->join('ms_user', 'ms_user.id = ms_report.id_user', 'LEFT')
				->join('ms_ksm', 'ms_ksm.id = ms_user.id_ksm', 'LEFT')
				->where('ms_report.delete', '0')
				->where('ms_report.start_date >=', $start_date)
				->where('ms_report.end_date <=', $end_date)
				// ->where('ms_ksm.id is not null')
				->where('ms_report.id_project', $value->id)
				// ->groupBy('ms_report.id_project')
				->countAllResults();
			$c[$value->name] = $total;
		}

		$data['project'] = $c;

		return $data;
	}
}
