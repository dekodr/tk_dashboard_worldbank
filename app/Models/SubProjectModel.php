<?php

namespace App\Models;

use CodeIgniter\Model;

class SubProjectModel extends Model
{
    protected $table = 'tb_sub_project';
    protected $allowedFields = ['name', 'code', 'name', 'delete'];

    public function get_sub_project($id = null)
    {

        if ($id !== null) {
            $data = $this->where(['id' => $id])->get()->getRow();
            return $data;
        } else {
            $data = $this->where(['delete' => '0'])->get()->getResult();

            return ($data);
        }
    }

    public function getSubByProjectId($project_id)
    {
        $query = $this->where(['id_project' => $project_id, 'delete' => '0'])->get()->getResult();
        return $query;
    }
}
