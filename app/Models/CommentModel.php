<?php

namespace App\Models;

use CodeIgniter\Database\ConnectionInterface;
use CodeIgniter\Model;

class CommentModel extends Model
{
    protected $table            = 'tr_comment';
    protected $primaryKey       = 'id';
    protected $allowedFields    = ['id_report', 'id_user', 'comment', 'entry_date', 'delete', 'type'];


    public function get_data($id = null)
    {
        if ($id !== null) {

            $data = $this->where(['id' => $id])->get()->getRow();
            return $data;
        } else {
            $data = $this->where("delete == 0 AND id != " . null)->get()->getResult();
            return $data;
        }
    }

    public function get_data_by_reportid_userid($idrpt, $iduser, $type = null)
    {
        if ($type == null) {
            $q = " and type is null";
            $ord = "ASC";
        } else {
            $q = " and type = " . $type;
            $ord = "DESC";
        }

        $data = $this->where("id_report = " . $idrpt . " and id_user = " . $iduser . $q)->orderBy('id', $ord)->get()->getRow();
        return $data;
    }
}
