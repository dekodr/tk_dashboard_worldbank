<?php

namespace App\Models;

use CodeIgniter\Model;

class ReportReplyModel extends Model
{
    protected $table = 'ms_report_reply';
    protected $primaryKey = 'idoffline';
    protected $allowedFields = [
        'title', 'id_replydata', 'fund_utilization', 'description', 'entry_date', 'edit_date',
        'is_publish', 'id_replypost', 'delete',  'id_user', 'images', 'progress', 'document', 'progress_month'
    ];

    public function get_report_reply($id = null)
    {
        if ($id !== null) {

            $data = $this->where(['id' => $id])->get()->getRow();
            return $data;
        } else {
            $data = $this->where(['delete' => '0'])->get()->getResult();

            return ($data);
        }
    }

    public function get_replyByReport($id = null)
    {
        if ($id !== null) {

            $data = $this->where(['id_replydata' => $id])->get()->getResult();
            return $data;
        } else {
            $data = $this->where(['delete' => '0'])->get()->getResult();

            return ($data);
        }
    }
}
