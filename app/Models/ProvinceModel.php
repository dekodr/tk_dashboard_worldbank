<?php

namespace App\Models;

use CodeIgniter\Database\ConnectionInterface;
use CodeIgniter\Model;

class ProvinceModel extends Model
{
    protected $table            = 'ms_provinsi';
    protected $primaryKey       = 'id';
    protected $allowedFields    = ['id', 'name', 'delete', 'code'];


    public function get_data($id = null)
    {
        if ($id !== null) {

            $data = $this->where(['id' => $id])->get()->getRow();
            return $data;
        } else {
            $data = $this->where(['delete' => '0'])->get()->getResult();
            return $data;
        }
    }

    public function getProvince()
    {
        $data = $this->where(['delete' => '0'])
            ->orderBy('name', 'asc')
            ->get()->getResult();

        // print_r($options);die;
        return $data;
    }
}
