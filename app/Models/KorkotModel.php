<?php

namespace App\Models;

use CodeIgniter\Database\ConnectionInterface;
use CodeIgniter\Model;

class KorkotModel extends Model
{
    protected $table = 'ms_korkot';
    protected $primaryKey = 'id';
    protected $allowedFields = ['id_provinsi', 'id_kabupaten', 'name', 'entry_stamp', 'edit_stamp', 'delete', 'code'];


    public function get_data($id = null)
    {
        if ($id !== null) {

            $data = $this->where(['id' => $id])->get()->getRow();
            return $data;
        } else {
            $data = $this->where(['delete' => 0])->get()->getResult();

            return ($data);
        }
    }

    public function getKorkotByProvId($prov_id)
    {
        $query = $this->where(['id_provinsi' => $prov_id, 'delete' => 0])->get()->getResult();
        return $query;
    }
}
