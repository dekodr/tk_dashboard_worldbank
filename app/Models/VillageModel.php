<?php

namespace App\Models;

use CodeIgniter\Database\ConnectionInterface;
use CodeIgniter\Model;

class VillageModel extends Model
{
    protected $table            = 'ms_kelurahan';
    protected $primaryKey       = 'id';
    protected $allowedFields    = ['code', 'name', 'delete', 'id_kecamatan', 'id_kabupaten'];


    public function get_data($id = null)
    {
        $query = "select a.*, b.id_province from ms_kelurahan a left join ms_kabupaten b on b.id=a.id_kabupaten";
        if ($id !== null) {
            $query .= " WHERE a.id = " . $id;
            $data = $this->query($query)->getRow();
            return $data;
        } else {
            $query .= " WHERE a.delete = 0";
            $data = $this->query($query)->getResult();
            return $data;
        }
    }

    public function insert_($_param)
    {
        //saving data
        $this->save($_param);
        return TRUE;
    }

    public function getVilBySubId($kec_id)
    {
        $query = $this->where(['id_kecamatan' => $kec_id, 'delete' => '0'])->get()->getResult();
        return $query;
    }
}
