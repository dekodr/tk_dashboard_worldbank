<?php

namespace App\Models;

use CodeIgniter\Model;

class UsernameModel extends Model
{
    protected $table = 'tr_username';

    public function get_username($id = null)
    {
        if ($id !== null) {
            $data = $this->where(['username' => $id, 'is_active' => 't'])->get()->getRow();
            return $data;
        } else {
            $data = $this->where(['delete' => '0'])->get()->getResult();

            return ($data);
        }
    }

    public function get_usernameByUser($user = null)
    {
        $data = $this->where(['upper(username)' => strtoupper($user), 'is_active' => 't'])->get()->getRow();

        return $data;
    }

    public function get_usernameByUserId($userId)
    {
        $data = $this->where(['id_user' => $userId, 'is_active' => 't'])->get()->getRow();
        return $data;
    }
}
