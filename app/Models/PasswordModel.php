<?php

namespace App\Models;

use CodeIgniter\Model;

class PasswordModel extends Model
{
    protected $table = 'tr_password';

    public function get_pass($id = null)
    {
        if ($id !== null) {
            $data = $this->where(['id_user' => $id, 'is_active' => 't'])->get()->getRow();
            return $data;
        } else {
            $data = $this->where(['delete' => '0'])->get()->getResult();

            return ($data);
        }
    }

    public function get_passByIdUser($id)
    {
        $data = $this->where("id_user = " . $id . " AND is_active = t")->get()->getResult();
        return $data;
    }
}
