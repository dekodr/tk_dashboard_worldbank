<?php

namespace App\Models;

// use CodeIgniter\Database\ConnectionInterface;
use CodeIgniter\Model;

class UjiPetikModel extends Model
{
    protected $db;
    protected $session          = null;
    protected $table            = 'ms_uji_petik';
    protected $primaryKey       = 'id';
    protected $allowedFields    = ['id', 'jenis_kegiatan', 'tingkat_kegiatan', 'start_date', 'end_date', 'tema', 'fokus_kegiatan', 'id_provinsi', 'id_kabupaten', 'id_kecamatan', 'id_kelurahan', 'topik_pendanaan', 'upload_dokumen', 'kategori_dokumen', 'catatan_hasil', 'kesimpulan', 'rekomendasi', 'delete', 'entry_date', 'edit_date', 'id_provinsi_pic', 'id_kabupaten_pic', 'id_kecamatan_pic', 'id_kelurahan_pic', 'nama_pic', 'id_user', 'is_approved', 'is_reject', 'tingkat_pic', 'osp_pic', 'korkot_pic'];

    public function __construct()
    {
        $this->session = session();
        $this->db = \Config\Database::connect();
    }

    public function get_data($id = null)
    {
        $id_user = $this->session->id;
        $id_role = $this->session->id_role;
        $id_city = $this->session->id_city;
        $id_province = $this->session->id_province;
        // echo $id_role . '-' . $id_user;
        // die;
        if ($id !== null) {
            $data = $this->where(['id' => $id])->get()->getRow();
        } else {
            if ($id_role == 2) {
                $data = $this->select('ms_uji_petik.*, ms_provinsi.name provinsi, ms_kabupaten.name kabupaten')
                    ->where(['delete' => '0', 'id_user' => $id_user])
                    ->join('ms_provinsi', 'ms_provinsi.id=ms_uji_petik.id_provinsi', 'LEFT')
                    ->join('ms_kabupaten', 'ms_kabupaten.id=ms_uji_petik.id_kabupaten', 'LEFT')
                    ->get()->getResult();
            } elseif ($id_role == 3 || $id_role == 4 || $id_role == 6) {
                $q = " SELECT a.*,b.id_ksm ksm FROM ms_user a JOIN ms_facilitator b ON a.id_fasilitator=b.id WHERE a.id = ? ";
                $facilitator = $this->query($q, [$id_user])->getRowArray();
                // print_r($facilitator);
                // die;
                $query = " 	SELECT 
                                a.* ,
                                d.name kelurahan,
                                e.name kota,
                                f.name provinsi
                            FROM 
                                ms_uji_petik a 
                            JOIN 
                                ms_user b ON b.id=a.id_user
                            LEFT JOIN 
                                ms_ksm c ON c.id=b.id_ksm
                            LEFT JOIN
                                ms_kelurahan d ON d.id=c.id_kelurahan
                            lEFT JOIN
                                ms_kabupaten e ON e.id=d.id_kabupaten
                            LEFT JOIN
                                ms_provinsi f ON f.id=e.id_province
                            WHERE
                                a.delete = 0 OR (a.id_user = " . $id_user . ") AND b.id_ksm IN(" . $facilitator['ksm'] . ") ORDER BY a.id DESC";
                $data = $this->query($query)->getResult();
            } else if ($id_role == 9) {
                $data = $this->select('ms_uji_petik.*, ms_provinsi.name provinsi, ms_kabupaten.name kabupaten')
                    ->join('ms_user', 'ms_user.id=ms_uji_petik.id_user', 'LEFT')
                    ->join('ms_provinsi', 'ms_provinsi.id=ms_uji_petik.id_provinsi', 'LEFT')
                    ->join('ms_kabupaten', 'ms_kabupaten.id=ms_uji_petik.id_kabupaten', 'LEFT')
                    ->where(['ms_user.id_city' => $id_city])
                    ->get()
                    ->getResult();
            } else {
                $data = $this->select('ms_uji_petik.*, ms_provinsi.name provinsi, ms_kabupaten.name kabupaten')
                    ->join('ms_user', 'ms_user.id=ms_uji_petik.id_user', 'LEFT')
                    ->join('ms_provinsi', 'ms_provinsi.id=ms_uji_petik.id_provinsi', 'LEFT')
                    ->join('ms_kabupaten', 'ms_kabupaten.id=ms_uji_petik.id_kabupaten', 'LEFT')
                    ->where(['ms_user.id_province' => $id_province])
                    ->get()
                    ->getResult();
            }
        }

        return $data;
    }

    public function getDetail($id = null)
    {
        $query = "
                select 
                    a.*, 
                    b.name lok_provinsi,
                    c.name lok_kota,
                    d.name lok_kelurahan,
                    e.name lok_kecamatan,
                    f.name pic_provinsi,
                    g.name pic_kota,
                    h.name pic_kelurahan,
                    i.name pic_kecamatan,
                    j.id_role  
                from 
                    ms_uji_petik a 
                left join 
                    ms_provinsi b on b.id=a.id_provinsi
                left join 
                    ms_kabupaten c on c.id=a.id_kabupaten
                left join 
                    ms_kelurahan d on d.id=a.id_kelurahan
                left join 
                    ms_kecamatan e on e.id=a.id_kecamatan
                left join 
                    ms_provinsi f on f.id=a.id_provinsi_pic
                left join 
                    ms_kabupaten g on g.id=a.id_kabupaten_pic
                left join 
                    ms_kelurahan h on h.id=a.id_kelurahan_pic
                left join 
                    ms_kecamatan i on i.id=a.id_kecamatan_pic
                left join
                    ms_user j on j.id=a.id_user";
        if ($id !== null) {
            $query .= " WHERE a.id = " . $id;
            $data = $this->query($query)->getRow();
            return $data;
        } else {
            $query .= " WHERE a.del = 0";
            $data = $this->query($query)->getResult();
            return $data;
        }
    }

    public function get_data_export($id = null)
    {
        $id_user = $this->session->id;
        $id_role = $this->session->id_role;
        $id_city = $this->session->id_city;
        $id_province = $this->session->id_province;
        // echo $id_role . '-' . $id_user;
        // die;
        if ($id !== null) {
            $data = $this->where(['id' => $id])->get()->getRow();
        } else {
            if ($id_role == 2) {
                $data = $this->select('ms_uji_petik.*, ms_provinsi.name provinsi, ms_kabupaten.name kabupaten, ms_kecamatan.name kecamatan, ms_kelurahan.name kelurahan')
                    ->where(['delete' => '0', 'id_user' => $id_user])
                    ->join('ms_provinsi', 'ms_provinsi.id=ms_uji_petik.id_provinsi', 'LEFT')
                    ->join('ms_kabupaten', 'ms_kabupaten.id=ms_uji_petik.id_kabupaten', 'LEFT')
                    ->join('ms_kecamatan', 'ms_kecamatan.id=ms_uji_petik.id_kecamatan', 'LEFT')
                    ->join('ms_kelurahan', 'ms_kelurahan.id=ms_uji_petik.id_kelurahan', 'LEFT')
                    ->get()
                    ->getResult();
            } elseif ($id_role == 3 || $id_role == 4 || $id_role == 6) {
                $q = " SELECT a.*,b.id_ksm ksm FROM ms_user a JOIN ms_facilitator b ON a.id_fasilitator=b.id WHERE a.id = ? ";
                $facilitator = $this->query($q, [$id_user])->getRowArray();
                // print_r($facilitator);
                // die;
                $query = " 	SELECT 
                                a.* ,
                                d.name kelurahan,
                                e.name kabupaten,
                                f.name provinsi,
                                g.name kecamatan
                            FROM 
                                ms_uji_petik a 
                            JOIN 
                                ms_user b ON b.id=a.id_user
                            LEFT JOIN 
                                ms_ksm c ON c.id=b.id_ksm
                            LEFT JOIN
                                ms_kelurahan d ON d.id=c.id_kelurahan
                            lEFT JOIN
                                ms_kabupaten e ON e.id=d.id_kabupaten
                            LEFT JOIN
                                ms_provinsi f ON f.id=e.id_province
                            LEFT JOIN
                                ms_kecamatan g ON g.id=.id_kecamatan
                            WHERE
                                a.delete = 0 OR (a.id_user = " . $id_user . ") AND b.id_ksm IN(" . $facilitator['ksm'] . ") ORDER BY a.id DESC";
                $data = $this->query($query)->getResult();
            } else if ($id_role == 9) {
                $data = $this->select('ms_uji_petik.*, ms_provinsi.name provinsi, ms_kabupaten.name kabupaten, ms_kecamatan.name kecamatan, ms_kelurahan.name kelurahan')
                    ->join('ms_user', 'ms_user.id=ms_uji_petik.id_user', 'LEFT')
                    ->join('ms_provinsi', 'ms_provinsi.id=ms_uji_petik.id_provinsi', 'LEFT')
                    ->join('ms_kabupaten', 'ms_kabupaten.id=ms_uji_petik.id_kabupaten', 'LEFT')
                    ->join('ms_kecamatan', 'ms_kecamatan.id=ms_uji_petik.id_kecamatan', 'LEFT')
                    ->join('ms_kelurahan', 'ms_kelurahan.id=ms_uji_petik.id_kelurahan', 'LEFT')
                    ->where(['ms_user.id_city' => $id_city])
                    ->get()
                    ->getResult();
            } else {
                $data = $this->select('ms_uji_petik.*, ms_provinsi.name provinsi, ms_kabupaten.name kabupaten, ms_kecamatan.name kecamatan, ms_kelurahan.name kelurahan')
                    ->join('ms_user', 'ms_user.id=ms_uji_petik.id_user', 'LEFT')
                    ->join('ms_provinsi', 'ms_provinsi.id=ms_uji_petik.id_provinsi', 'LEFT')
                    ->join('ms_kabupaten', 'ms_kabupaten.id=ms_uji_petik.id_kabupaten', 'LEFT')
                    ->join('ms_kecamatan', 'ms_kecamatan.id=ms_uji_petik.id_kecamatan', 'LEFT')
                    ->join('ms_kelurahan', 'ms_kelurahan.id=ms_uji_petik.id_kelurahan', 'LEFT')
                    ->where(['ms_user.id_province' => $id_province])
                    ->get()
                    ->getResult();
            }
        }

        return $data;
    }

    public function getSubDistrictByCity($id_kabupaten)
    {
        $q = $this->db->table('ms_kelurahan');
        $data = $q->where([
            'ms_kelurahan.delete' => 0,
            'id_kabupaten' => $id_kabupaten
        ])->join('ms_kecamatan', 'ms_kecamatan.id=ms_kelurahan.id_kecamatan', 'inner')
            ->select('ms_kelurahan.*')
            ->groupBy('ms_kelurahan.id')
            ->get()
            ->getResultArray();

        return $data;
    }

    public function get_jumlah_jenis_kegiatan($for = 'pie')
    {
        $list = [
            'Uji Petik',
            'Misi Dukungan Implementasi',
            'Misi Teknis',
            'Lainnya'
        ];

        $data = [];

        if ($for == 'pie') {
            foreach ($list as $key => $value) {
                $data[] = [
                    'name' => $value,
                    'y' => $this->where(['delete' => 0, 'jenis_kegiatan' => $value])->countAllResults()
                ];
            }
            return json_encode($data);
        } else {
            foreach ($list as $key => $value) {
                $data[$value] = $this->where(['delete' => 0, 'jenis_kegiatan' => $value])->countAllResults();
            }
            return $data;
        }
    }

    public function get_tema_kegiatan()
    {
        $list = [
            'Semua',
            'Monev',
            'MIS',
            'Manajemen Keuangan',
            'Infrastuktur',
            'Safeguard',
            'PIM',
            'Pelatihan',
            'Sosialisasi'
        ];

        $data = [];

        foreach ($list as $key => $value) {
            $data[] = [
                'name' => $value,
                'y' => $this->where(['delete' => 0, 'tema' => $value])->countAllResults()
            ];
        }
        return json_encode($data);
    }

    public function get_total_provinsi()
    {
        $q = "  SELECT
                    COUNT(id_provinsi) as total,
                    b.name
                FROM
                    ms_uji_petik a
                LEFT JOIN
                    ms_provinsi b ON b.id=a.id_provinsi
                WHERE
                    a.delete = 0
                GROUP BY a.id_provinsi,b.name";
        $q = $this->query($q)->getResult();

        $data = [];
        foreach ($q as $key => $value) {
            $data[] = [
                'name' => $value->name,
                'y' => (int)$value->total
            ];
        }

        return json_encode($data);
    }

    public function get_total_kabupaten()
    {
        $q = "  SELECT
                    COUNT(id_kabupaten) as total,
                    b.name
                FROM
                    ms_uji_petik a
                LEFT JOIN
                    ms_kabupaten b ON b.id=a.id_kabupaten
                WHERE
                    a.delete = 0
                GROUP BY a.id_kabupaten,b.name";
        $q = $this->query($q)->getResult();

        $data = [];
        foreach ($q as $key => $value) {
            $data[] = [
                'name' => $value->name,
                'y' => (int)$value->total
            ];
        }

        return json_encode($data);
    }

    public function get_total_kelurahan()
    {
        $q = "  SELECT
                    COUNT(id_kelurahan) as total,
                    b.name
                FROM
                    ms_uji_petik a
                LEFT JOIN
                    ms_kelurahan b ON b.id=a.id_kelurahan
                WHERE
                    a.delete = 0
                GROUP BY a.id_kelurahan,b.name";
        $q = $this->query($q)->getResult();

        $data = [];
        foreach ($q as $key => $value) {
            $data[] = [
                'name' => $value->name,
                'y' => (int)$value->total
            ];
        }

        return json_encode($data);
    }

    public function getJenisByProvince($province)
    {
        $list = [
            'Uji Petik',
            'Misi Dukungan Implementasi',
            'Misi Teknis',
            'Lainnya'
        ];

        $data = [];

        foreach ($list as $key => $value) {
            $data[$value] = $this->join('ms_provinsi', 'ms_provinsi.id=ms_uji_petik.id_provinsi')->where(['jenis_kegiatan' => $value, 'ms_provinsi.name' => $province])->countAllResults();
        }

        return $data;
    }
}
