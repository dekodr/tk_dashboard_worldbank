<?php

namespace App\Models;

use CodeIgniter\Database\ConnectionInterface;
use CodeIgniter\Model;

class KsmModel extends Model
{
    protected $table            = 'ms_ksm';
    protected $primaryKey       = 'id';
    protected $allowedFields    = ['id', 'name', 'id_kelurahan', 'kode_ksm', 'entry_date', 'edit_date', 'delete'];


    public function get_data($id = null)
    {
        $query = "select 
                    a.*, 
                    b.name kelurahan,
                    b.id_kecamatan,
                    b.id_kabupaten,
                    c.id_province 
                  from 
                    ms_ksm a 
                  left join 
                    ms_kelurahan b on b.id=a.id_kelurahan
                  left join 
                    ms_kabupaten c on c.id=b.id_kabupaten";
        if ($id !== null) {
            $query .= " WHERE a.id = " . $id;
            $data = $this->query($query)->getRow();
            return $data;
        } else {
            $query .= " WHERE a.delete = 0";
            $data = $this->query($query)->getResult();
            return $data;
        }
    }

    public function getKSMByVilId($kel_id)
    {
        $query = "select a.*, b.name kelurahan from ms_ksm a left join ms_kelurahan b on b.id=a.id_kelurahan";
        $query = $this->where(['id_kelurahan' => $kel_id])->get()->getResult();
        return $query;
    }

    public function getKSMByKabupatenId($kel_id)
    {
        $query = "select 
                    a.*,
                    b.name kelurahan,
                    c.id kecamatan,
                    d.id kabupaten,
                    e.id provinsi
                from 
                    ms_ksm a 
                left join 
                    ms_kelurahan b on b.id=a.id_kelurahan
                left join
                    ms_kecamatan c on c.id=b.id_kecamatan
                left join
                    ms_kabupaten d on d.id=c.id_kabupaten
                left join
                    ms_provinsi e on e.id=d.id_province";
        $query .= " WHERE a.delete = 0 AND d.id = " . $kel_id;
        $data = $this->query($query)->getResult();
        return $data;
    }
}
