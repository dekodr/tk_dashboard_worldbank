<?php

namespace App\Models;

use CodeIgniter\Model;

class ProjectLocModel extends Model
{
    protected $table = 'tb_project_loc';
    protected $allowedFields = ['name', 'delete'];

    public function get_project_loc($id = null)
    {
        if ($id !== null) {
            $data = $this->where(['id' => $id])->get()->getRow();
            return $data;
        } else {
            $data = $this->where(['delete' => '0'])->get()->getResult();

            return ($data);
        }
    }
}
