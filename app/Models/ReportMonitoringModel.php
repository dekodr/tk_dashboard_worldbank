<?php

namespace App\Models;

use CodeIgniter\Model;

class ReportMonitoringModel extends Model
{
    protected $table = 'ms_report';
    protected $db;
    protected $primaryKey = 'idoffline';
    protected $session = null;
    protected $allowedFields = [
        'title', 'fund_utilization', 'description', 'is_publish', 'delete', 'is_approval', 'id_user',
        'start_date', 'end_date', 'type', 'progress', 'hibah', 'vol_plan',
        'vol_real', 'doc_type', 'document', 'images', 'id_project', 'id_sub_project', 'id_project_loc',
        'status_date', 'vol_unit', 'progress_month', 'entry_date', 'edit_date', 'report_type'
    ];
    public function __construct()
    {
        $this->session = session();
        $this->db = \Config\Database::connect();
    }

    public function getList($id = "")
    {
        $id_role = $this->session->id_role;
        $id_user = $this->session->id;
        $id_city = $this->session->id_city;
        $id_province = $this->session->id_province;

        if ($id == "") {
            if ($id_role == 3 || $id_role == 4) {
                $data = $this->where(['id_user' => $id_user, 'report_type' => '2'])->get()->getResult();
            } else if ($id_role == 9) {
                $data = $this->select('ms_report.*')
                    ->join('ms_user', 'ms_user.id=ms_report.id_user', 'LEFT')
                    ->where(['ms_user.id_city' => $id_city, 'report_type' => '2'])
                    ->get()
                    ->getResult();
            } else {
                $data = $this->select('ms_report.*')
                    ->join('ms_user', 'ms_user.id=ms_report.id_user', 'LEFT')
                    ->where(['ms_user.id_province' => $id_province, 'report_type' => '2'])
                    ->get()
                    ->getResult();
            }
        } else {
            $data = $this->where(['idoffline' => $id])->get()->getRow();
        }
        // echo $this->getLastQuery();
        // print_r($data);
        // die;
        return $data;
    }
}
