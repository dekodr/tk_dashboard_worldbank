<?php

namespace App\Models;

use CodeIgniter\Database\ConnectionInterface;
use CodeIgniter\Model;

class SubdistrictModel extends Model
{
    protected $table            = 'ms_kecamatan';
    protected $primaryKey       = 'id';
    protected $allowedFields    = ['code', 'name', 'id_kabupaten', 'delete'];


    public function get_data($id = null)
    {
        $query = "select a.*, b.id_province from ms_kecamatan a left join ms_kabupaten b on b.id=a.id_kabupaten";
        if ($id !== null) {
            $query .= " WHERE a.id = " . $id;
            $data = $this->query($query)->getRow();
            return $data;
        } else {
            $query .= " WHERE a.delete = 0";
            $data = $this->query($query)->getResult();
            return $data;
        }
    }

    public function create_data($data)
    {
        echo "model";
        die;
        $this->insertBatch($data);
        print_r($data);
    }

    public function insert_subDistrict($_param)
    {
        //saving data
        $this->save($_param);
        return TRUE;
    }

    public function getSubByDistrictId($kab_id)
    {
        $query = $this->where(['id_kabupaten' => $kab_id, 'delete' => '0'])->get()->getResult();
        return $query;
    }
}
