<?php

namespace App\Models;

use CodeIgniter\Model;

class ReportModel extends Model
{
	protected $db;
	protected $session = null;
	protected $table = 'ms_report';
	protected $primaryKey = 'idoffline';
	protected $allowedFields = [
		'title', 'fund_utilization', 'description', 'is_publish', 'delete', 'is_approval', 'id_user',
		'start_date', 'end_date', 'type', 'progress', 'hibah', 'vol_plan',
		'vol_real', 'doc_type', 'document', 'images', 'id_project', 'id_sub_project', 'id_project_loc',
		'status_date', 'vol_unit', 'progress_month', 'entry_date', 'edit_date'
	];

	public function __construct()
	{
		$this->session = session();
		$this->db = \Config\Database::connect();
	}

	public function get_report($id = null)
	{
		if ($id !== null) {
			$data = $this->where(['idoffline' => $id])->get()->getRow();
			return $data;
		} else {
			$data = $this->where(['delete' => '0'])->get()->getResult();
			return ($data);
		}
	}

	public function get_report_approve()
	{
		$data = $this->where(['delete' => '0', 'is_approval' => '0'])->get()->getResult();
		return ($data);
	}

	public function get_index_kel($role)
	{
		$id_user = $this->session->id;
		$id_city = $this->session->id_city;

		if ($role == 3 || $role == 4 || $role == 6) {
			$q = " SELECT * FROM ms_user JOIN ms_facilitator ON ms_user.id_fasilitator=ms_facilitator.id WHERE ms_user.id = ? ";
			$facilitator = $this->db->query($q, [$id_user])->getRowArray();

			$query = " 	SELECT 
							a.* ,
							d.name kelurahan,
							e.name kota,
							f.name provinsi
						FROM 
							ms_report a 
						JOIN 
							ms_user b ON b.id=a.id_user
						LEFT JOIN 
							ms_ksm c ON c.id=b.id_ksm
						LEFT JOIN
							ms_kelurahan d ON d.id=c.id_kelurahan
						lEFT JOIN
							ms_kabupaten e ON e.id=d.id_kabupaten
						LEFT JOIN
							ms_provinsi f ON f.id=e.id_province
						WHERE
							a.delete = 0 AND b.id_ksm IN(" . $facilitator['id_ksm'] . ") ORDER BY a.idoffline DESC";
			$data = $this->db->query($query)->getResult();
		} else {
			$data = $this->select('ms_report.*')
				->join('ms_user', 'ms_user.id=ms_report.id_user', 'LEFT')
				->where(['ms_user.id_city' => $id_city, 'report_type =' => null])
				->get()
				->getResult();
		}

		return $data;

		// if ($id_boss != null) {

		// 	$data = $this->where("
		// 	id_user = " . $id_boss . "
		// 	OR id_user IN (SELECT ID FROM ms_user WHERE id_boss = " . $id_boss . ")
		// 	OR id_user IN (SELECT ID FROM ms_user WHERE id_boss IN ( SELECT ID FROM ms_user WHERE id_boss = " . $id_boss . " ))
		// 	OR id_user IN (SELECT ID FROM ms_user where id_boss IN (SELECT ID FROM ms_user WHERE id_boss IN ( SELECT ID FROM ms_user WHERE id_boss = " . $id_boss . " )))
		// 	OR id_user IN (SELECT ID FROM ms_user where id_boss IN (SELECT ID FROM ms_user where id_boss IN (SELECT ID FROM ms_user WHERE id_boss IN ( SELECT ID FROM ms_user WHERE id_boss = " . $id_boss . " ))))
		// 	OR id_user IN (SELECT ID FROM ms_user WHERE id_boss2 = " . $id_boss . ")
		// 	OR id_user IN (SELECT ID FROM ms_user WHERE id_boss2 IN ( SELECT ID FROM ms_user WHERE id_boss2 = " . $id_boss . " ))
		// 	OR id_user IN (SELECT ID FROM ms_user where id_boss2 IN (SELECT ID FROM ms_user WHERE id_boss2 IN ( SELECT ID FROM ms_user WHERE id_boss2 = " . $id_boss . " )))
		// 	OR id_user IN (SELECT ID FROM ms_user where id_boss2 IN (SELECT ID FROM ms_user where id_boss2 IN (SELECT ID FROM ms_user WHERE id_boss2 IN ( SELECT ID FROM ms_user WHERE id_boss2 = " . $id_boss . " ))))            
		// 	AND id_user NOTNULL
		// 	AND is_approval = '0' 
		// 	AND is_publish = 'f' ")->get()->getResult();
		// }
	}

	public function getList($id_user = null)
	{
		//$data = $this->db->query('SELECT * FROM ms_report WHERE  id_user =' . $id_user . 'LIMIT 1')->getResult();
		if ($id_user > 0 && $this->session->id_role != 12) {
			$data = $this->where('ms_report.delete', '0')->where("id_user IN (" . $id_user . ") ")->get()->getResult();
			return $data;
		} else if ($id_user == 'KORKOT 01 - OSP- 2 - JATENG') {
			$data = $this->where('ms_report.delete', '0')->get()->getResult();
			return $data;
		} else if ($id_user == 'TF 01 - KORKOT 01 - OSP- 2 - JATENG') {
			$data = $this->whereIn('ms_report.id_user', ['241', '244', '245', '248'])->get()->getResult();
			return $data;
		} else if ($id_user == 'TF 15 - KORKOT 01 - OSP- 2 - JATENG') {
			$data = $this->whereIn('ms_report.id_user', ['249', '250', '251', '252', '253', '254'])->get()->getResult();
			return $data;
		} elseif ($this->session->id_role == 12) {
			$query = " 	SELECT 
							a.* ,
							d.name kelurahan,
							e.name kota,
							f.name provinsi,
							g.name_role
						FROM 
							ms_report a 
						JOIN 
							ms_user b ON b.id=a.id_user
						LEFT JOIN 
							ms_ksm c ON c.id=b.id_ksm
						LEFT JOIN
							ms_kelurahan d ON d.id=c.id_kelurahan
						lEFT JOIN
							ms_kabupaten e ON e.id=d.id_kabupaten
						LEFT JOIN
							ms_provinsi f ON f.id=e.id_province
						LEFT JOIN
							ms_role g ON g.id=b.id_role
						WHERE
							a.delete = 0 ORDER BY a.idoffline DESC";
			$data = $this->db->query($query)->getResult();
			return $data;
		}
	}

	public function getLoc($id_ksm)
	{
		$query = "	SELECT
						b.id_project_loc,
						a.id,
						a.name
					FROM
						tb_project_loc_ksm b
					INNER JOIN
						tb_project_loc a ON a.id=b.id_project_loc
					WHERE
						(b.delete = 0 OR b.delete IS NULL) AND b.id_ksm = ?
					GROUP BY 
						b.id_project_loc,a.id,a.name";
		$query = $this->query($query, [$id_ksm]);
		return $query->getResult();
	}
}
