<?php

namespace App\Models;

use CodeIgniter\Database\ConnectionInterface;
use CodeIgniter\Model;

class FacilitatorModel extends Model
{
    protected $table = 'ms_facilitator';
    protected $allowedFields = ['id_ksm', 'name', 'entry_stamp', 'edit_stamp', 'del', 'code', 'id_kabupaten'];


    public function get_data($id = null)
    {
        $query = "select a.*, b.id_province from ms_facilitator a left join ms_kabupaten b on b.id=a.id_kabupaten";
        if ($id !== null) {
            $query .= " WHERE a.id = " . $id;
            $data = $this->query($query)->getRow();
            return $data;
        } else {
            $query .= " WHERE a.del = 0";
            $data = $this->query($query)->getResult();
            return $data;
        }
    }

    public function getKSMByDistrictId($kab_id)
    {
        $query = $this->where(['id_kabupaten' => $kab_id, 'del' => '0'])->get()->getResult();
        return $query;
    }

    // public function update_status($data_)
    // {
    //     $payload['id'] = $data_['id'];
    //     $payload['is_account'] = $data_['is_account'];
    //     // print_r($payload);die;
    //     $this->updatedb('ms_facilitator', $payload, $payload['id']);
    // }

    // private function updatedb($table, $payload, $id)
    // {
    //     $db         = db_connect('default');
    //     $builder    = $db->table('ms_facilitator');
    //     $builder->where('id', $id);
    //     $builder->update($payload);
    // }
}
