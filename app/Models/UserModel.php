<?php

namespace App\Models;

use CodeIgniter\Database\ConnectionInterface;
use CodeIgniter\Model;

class UserModel extends Model
{
    protected $table = 'ms_user';
    protected $allowedFields = ['id_ksm', 'name', 'email', 'id_boss', 'position', 'phone', 'delete', 'id_boss2', 'id_role', 'id_province', 'id_city', 'id_fasilitator', 'id_korkot'];

    public function get_user($id = null)
    {
        if ($id !== null) {

            $data = $this->select('ms_user.*,ms_role.id id_role, ms_role.name_role')->where(['ms_user.id' => $id])->join('ms_role', 'ms_role.id=ms_user.id_role', 'left')->get()->getRow();
            return $data;
        } else {
            $data = $this->where(['delete' => '0'])->get()->getResult();

            return ($data);
        }
    }

    public function insert_user($_param)
    {
        $_param['entry_stamp']  = date("Y-m-d h:i:s");

        //saving data
        $this->insert($_param);
        return TRUE;
    }

    public function get_participant()
    {
        $data = $this->where(['del' => 0])->get()->getResult();

        //Getting User Participant
        foreach ($data as $key => $value)
            $participant[] = $value->name . " (" . $value->number_id . ")";

        $tags = array();
        foreach ($data as $key => $value) {
            $split = explode(" ", $value->division);
            foreach ($split as $split_) {
                array_push($tags, $split_);
            }
        }

        $tags   = array_unique($tags);
        $return = array_merge($participant, $tags);

        return ($return);
    }


    public function create_batch($data)
    {

        $db         = \Config\Database::connect();
        $projectDB  = $db->table('tb_project');
        $sub_projectDB  = $db->table('tb_sub_project');

        foreach ($data as $key => $value) {
            $project = $projectDB->like(['code' => $value['code']])->get()->getRow();

            foreach ($value['sub_project'] as $key_ => $value_) {
                # code...

                $sub_project[$value_['code']]['id_project'] = $project->id;
                $sub_project[$value_['code']]['code']       = $value_['code'];
                $sub_project[$value_['code']]['name']       = $value_['name'];
            }
        }

        print_r($sub_project);

        die;
        // $sub_projectDB->insertBatch($sub_project);
    }

    public function getLogin($email, $password)
    {
        if ($email !== null && $password != null) {
            $data = $this->where(['email' => $email])->get()->getRow();
            return $data;
        } else {
            return null;
        }
    }

    public function getUserByIdBoss($id_boss)
    {
        $data = $this->where(['id_boss' => $id_boss])->get()->getRow();
        return $data;
    }

    public function getPositionById($id)
    {
        $data = $this->where(['id' => $id])->get()->getRow();
        return $data->position;
    }

    public function getkorkot()
    {
        $data = $this->where("
        (id_boss is null AND id_boss2 is NULL AND delete = '0') OR 
        (id_boss = 0 AND id_boss2 = 0 AND delete = '0')")->get()->getResult();
        return $data;
    }
}
