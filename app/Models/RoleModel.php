<?php

namespace App\Models;

use CodeIgniter\Database\ConnectionInterface;
use CodeIgniter\Model;

class RoleModel extends Model
{
    protected $table            = 'ms_role';
    protected $primaryKey       = 'id';
    protected $allowedFields    = ['id', 'name_role', 'description', 'entry_date', 'edit_date', 'delete'];


    public function get_data($id = null)
    {
        if ($id !== null) {

            $data = $this->where(['id' => $id])->get()->getRow();
            return $data;
        } else {
            $data = $this->where(['delete' => '0'])->get()->getResult();
            return $data;
        }
    }

    public function getRole(){
        $data = $this->where(['delete' => '0'])
                        ->orderBy('name_role', 'asc')
                        ->get()->getResult();

        // print_r($options);die;
        return $data;
    }
}
