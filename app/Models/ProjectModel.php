<?php

namespace App\Models;

use CodeIgniter\Model;

class ProjectModel extends Model
{
    protected $table = 'tb_project';
    protected $allowedFields = ['code', 'name', 'delete'];

    public function get_project($id = null)
    {
        if ($id !== null) {
            $data = $this->where(['id' => $id])->get()->getRow();
            return $data;
        } else {
            $data = $this->where(['delete' => '0'])->get()->getResult();

            return ($data);
        }
    }
}
