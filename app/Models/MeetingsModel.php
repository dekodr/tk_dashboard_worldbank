<?php

namespace App\Models;

use CodeIgniter\Database\ConnectionInterface;
use CodeIgniter\Model;
use App\Models\UserModel;

class MeetingsModel extends Model
{
    protected $UserModel;
    protected $table            = 'ms_meeting';
    protected $allowedFields    = ['id', 'name', 'date', 'start_time', 'end_time', 'description', 'participant', 'entry_stamp', 'edit_stamp', 'del'];
    protected $table2           = 'tr_atendee_person';
    protected $allowedFields2    = ['id', 'id_user', 'id_meeting'];
    protected $table3              = 'tr_atendee_group';
    protected $allowedFields3    = ['id', 'id_group', 'id_meeting'];


    public function __construct()
    {
        $this->UserModel        = new UserModel();
    }
    
    public function get_meeting(){
        $data = $this->where(['del' => 0])->get()->getResult();
        return ($data);
    }

    public function insert_meeting($_param){
        $_param['entry_stamp']  = date("Y-m-d h:i:s");
        $participant            = explode(",", $_param['participant']);
        UNSET($_param['participant']);

        $this->insert($_param); // save meeting detail
        $id_meeting = $this->insertID(); //id meeting

        echo $id_meeting;

        foreach ($participant as $value) {
            
            //Get usr id by numbr_id
            if(strpos($value, "#") !== 0){
                $value = preg_match_all("/\(([^)]+)\)/", $value, $matches);
                $value = $this->UserModel->get_user($matches[1][0]);
                $value = $value->id; //id_user
                
                $db = db_connect('default'); 
                $builder = $db->table('tr_atendee_person');
                $builder->insert(array('id_meeting'=>$id_meeting, 'id_user'=>$value));
            }else{
                $db = db_connect('default'); 
                $builder = $db->table('tr_atendee_group');
                print_r($builder->insert(array('id_meeting'=>$id_meeting, 'id_group'=>$value)));
            }
        }

        //saving data
        return TRUE;
    }



    
    public function meetingLIST($number_id){
        $number_id = (int)$number_id;
        // echo $number_id;

        $getUser    = $this->UserModel->get_user($number_id);
        $getTag    = explode(' ', $getUser->division);   


        $meetingID  = $this
                        ->select('ms_meeting.*')
                        ->where(['ms_user.number_id' => $number_id])
                        ->join('tr_atendee_person', 'tr_atendee_person.id_meeting = ms_meeting.id', "left")
                        ->join('tr_atendee_group', 'tr_atendee_group.id_meeting = ms_meeting.id', "left")
                        ->join('ms_user', 'ms_user.id = tr_atendee_person.id_user', "left")
                        ->orderBy('ms_meeting.id', 'DESC')
                        ->groupBy('id');

        // $meetingTAG = $meetingID->orLike(['tr_atendee_group.id_group' => "#IT"]);

        foreach ($getTag as $tag) {
            # code...
            // echo $tag;
            $meetingID->orLike(['tr_atendee_group.id_group' => $tag]);
        }

        $meetingID = $meetingID->get()->getResult();


        $data['user']       = $getUser;
        $data['meeting']    = $meetingID;

       // print_r($data);
        //print_r($this->getLastQuery());
        return ($data);
    }
}
