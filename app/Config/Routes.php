<?php

namespace Config;

// Create a new instance of our RouteCollection class.
$routes = Services::routes();

// Load the system's routing file first, so that the app and ENVIRONMENT
// can override as needed.
if (file_exists(SYSTEMPATH . 'Config/Routes.php')) {
	require SYSTEMPATH . 'Config/Routes.php';
}

/**
 * --------------------------------------------------------------------
 * Router Setup
 * --------------------------------------------------------------------
 */
$routes->setDefaultNamespace('App\Controllers');
$routes->setDefaultController('Main');
$routes->setDefaultMethod('index');
$routes->setTranslateURIDashes(false);
$routes->set404Override();
$routes->setAutoRoute(true);

/*
 *
 * --------------------------------------------------------------------
 * Route Definitions
 * --------------------------------------------------------------------
 */

// We get a performance increase by specifying the default
// route since we don't have to scan directories.
$routes->get('/', 'Main::index');
$routes->get('/main/login', 'Main::login');

// Dashboard
$routes->get('/admin', 'Admin\Dashboard::index');

// Master Provinsi
$routes->get('/admin/master/province', 'Admin/Master/Province::index');
$routes->get('/admin/master/province/create', 'Admin/Master/Province::create');

// Master Kabupaten
$routes->get('/admin/master/district', 'Admin/Master/District::index');
$routes->get('/admin/master/district/create', 'Admin/Master/District::create');

// Master Kecamatan
$routes->get('/admin/master/subdistrict', 'Admin/Master/Subdistrict::index');
$routes->get('/admin/master/subdistrict/create', 'Admin/Master/Subdistrict::create');

// Village Kelurahan
$routes->get('/admin/master/village', 'Admin/Master/Village::index');
$routes->get('/admin/master/village/create', 'Admin/Master/Village::create');

// Report
$routes->get('/admin/report', 'Admin/Report/ReportCon::index');
$routes->get('/admin/report/create', 'Admin/Report/ReportCon::create');

// KSM
$routes->get('/admin/master/ksm', 'Admin/Master/Ksm::index');
$routes->get('/admin/master/ksm/create', 'Admin/Master/Ksm::create');

// Facilitator
$routes->get('/admin/master/facilitator', 'Admin/Master/Facilitator::index');
$routes->get('/admin/master/facilitator/create', 'Admin/Master/Facilitator::create');

// Role
$routes->get('/admin/master/role', 'Admin/Master/Role::index');
$routes->get('/admin/master/role/create', 'Admin/Master/Role::create');

// Facilitator
$routes->get('/korkot', 'Admin/Master/KorKot::index');
$routes->get('/korkot/create', 'Admin/Master/KorKot::create');

// user
$routes->get('/user', 'Admin/Master/User::index');
$routes->get('/user/create', 'Admin/Master/User::create');
//STAGE 2
$routes->get('/report-monitoring', 'Admin/Report/ReportMonitoring::index');
$routes->get('/create-report-monitoring', 'Admin/Report/ReportMonitoring::create');
//Uji Petik
$routes->get('/uji-petik', 'Admin/Report/UjiPetik::index');
$routes->get('/create-uji-petik', 'Admin/Report/UjiPetik::create');
$routes->get('/export-uji-petik', 'Admin/Report/UjiPetik::export');
$routes->get('/dashboard-uji-petik', 'Admin/Report/UjiPetik::dashboard');
//ADMIN SECTION
// $routes->group('admin', function ($routes) {

// 	$routes->get('/', 'Admin\Dashboard::index');

// 	//MEETINGS
// 	$routes->group('meetings', function ($routes) {
// 		$routes->add('/', 'Admin\Meetings::index');
// 	});

// 	//MASTER
// 	$routes->group('master', function ($routes) {

// 		//USER
// 		$routes->group('user', function ($routes) {
// 			$routes->add('/', 'Admin\Master\User::index');
// 			$routes->add('absent', 'Admin\Master\User::absent');
// 		});
// 	});
// });


/**
 * --------------------------------------------------------------------
 * Additional Routing
 * --------------------------------------------------------------------
 *
 * There will often be times that you need additional routing and you
 * need it to be able to override any defaults in this file. Environment
 * based routes is one such time. require() additional route files here
 * to make that happen.
 *
 * You will have access to the $routes object within that file without
 * needing to reload it.
 */
if (file_exists(APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php')) {
	require APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php';
}
