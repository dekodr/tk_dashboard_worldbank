<?php

namespace App\Controllers;

use App\Models\GraphModel;
use App\Controllers\BaseController;

class Graph extends BaseController
{
    protected $helpers = ['form', 'date'];
    protected $session = null;
    public $GM;

    public function __construct()
    {
        $this->GM = new GraphModel();
        $this->session = session();
    }

    public function index()
    {
        return json_encode("pace hentai");
    }

    public function ksm()
    {
        $graph = $this->GM->report_ksm();
        print_r($graph);
    }

    public function kel()
    {
        $graph = $this->GM->report_kel();

        print_r($graph);
    }
    public function total()
    {
        $graph = $this->GM->total_report();

        print_r($graph);
    }
    public function project()
    {
        $graph = $this->GM->report_project();

        print_r($graph);
    }
}
