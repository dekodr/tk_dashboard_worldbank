<?php

namespace App\Controllers;

use App\Models\UserModel;
use App\Models\UsernameModel;
use App\Models\PasswordModel;
use App\Controllers\BaseController;

class Main extends BaseController
{
	protected $primaryKey = 'user_id';
	protected $helpers = ['form', 'date'];
	protected $session = null;
	protected $request = null;
	public $UserModel;
	public $UsernameModel;
	public $PasswordModel;
	public $Exception;
	public $client;

	public function __construct()
	{
		$this->UserModel = new UserModel();
		$this->UsernameModel = new UsernameModel();
		$this->PasswordModel = new PasswordModel();
		$this->session = session();
		$this->request = \Config\Services::request();
		$this->client = \Config\Services::curlrequest();
	}

	// public function index()
	// {
	// 	echo "main index";
	// 	return view('home');
	// }

	public function index()
	{
		if (isset($this->session->id)) {
			return redirect()->to(base_url('/admin'));
		} else {
			return redirect()->to(base_url('/main/login'));
		}
	}

	public function login()
	{

		if (isset($this->session->id)) {
			return redirect()->to('/admin');
		}

		$user 		= $this->request->getVar('user');
		$password	= $this->request->getVar('password');
		// get data form

		if ($user !== null) {

			$user 		= $this->request->getVar('user');
			$password	= $this->request->getVar('password');
			$status		= 0;
			#data login
			$dataLogin = $this->client->request(
				'post',
				'http://103.93.57.36:5000/login-user',
				['json' => [
					'username' => $user,
					'password' => $password,
					'apps_key' => 'wb_kotaku'
				], 'http_errors' => false]
			);
			$status = $dataLogin->getStatusCode();
			// dd($dataLogin);

			if ($status == 200) {
				$dataLogin = json_decode($dataLogin->getbody());
				$dataLogin = $this->UserModel->get_user($dataLogin->id_user);
				// dd($dataLogin);
				// if (($dataLogin->id_boss == 0 && $dataLogin->id_boss2 == 0) || ($dataLogin->id_boss == null && $dataLogin->id_boss2 == null)) {
				if ($dataLogin->id_role == 5) {
					$approver = "korkot";
				} //else if ($dataLogin->position == 'Fasilitator Ekonomi' || $dataLogin->position == 'Fasilitator Teknik') {
				else if ($dataLogin->id_role == 3 || $dataLogin->id_role == 4) {
					$approver = "fasilitator";
				} //else if (strpos(strtolower($dataLogin->position), strtolower("senior")) !== false) {
				else if ($dataLogin->id_role == 6) {
					$approver = "senior";
				} else {
					$approver = "ksm";
				}

				echo $approver;

				// check user
				// if ($dataLogin->id_boss != null || $dataLogin->id_boss != 0) {
				// } else {
				// 	$approver = "korkot";
				// }
				// if ($dataLogin->id_boss != null) {
				// 	//check if approver or user normal
				// 	$cekBoss = $this->UserModel->getUserByIdBoss($dataLogin->id);
				// 	if ($cekBoss == null) {
				// 		$approver = "ketua_program";
				// 	} else {
				// 		$approver = "approver_kel";
				// 	}
				// } else {
				// 	$approver = "korkot";
				// }

				$data = [
					'id' => $dataLogin->id,
					'id_boss' => $dataLogin->id_boss,
					'id_role' => $dataLogin->id_role,
					'name' => $dataLogin->name,
					'email' => $dataLogin->email,
					'position' => $dataLogin->position,
					'approver' => $dataLogin->name_role,
					'id_province' => $dataLogin->id_province,
					'id_city' => $dataLogin->id_city
				];

				if ($dataLogin->id_role == 3 || $dataLogin->id_role == 4 || $dataLogin->id_role == 6) {
					$data['id_fasilitator'] == $dataLogin->id_fasilitator;
				} else if ($dataLogin->id_role == 5) {
					$data['id_korkot'] = $dataLogin->id_korkot;
				} else if ($dataLogin->id_role == 2) {
					$data['id_ksm'] = $dataLogin->id_ksm;
				}
				// print_r($data);die;
				$this->session->set($data);
				return redirect()->to(base_url('/admin'));
			} else {
				$this->session->setFlashdata('msg', 'Username & Password not Found');
				return view('login');
			}
		} else {
			return view('login');
		}
	}

	public function logout()
	{
		$this->session->destroy();
		return redirect()->to(base_url('/main/login'));
	}


	//--------------------------------------------------------------------




	function django_password_verify(string $pass, string $djangoHash): bool
	{
		$pieces = explode('$', $djangoHash);
		if (count($pieces) !== 4) {
			throw new Exception("Illegal hash format");
		}
		list($header, $iter, $salt, $hash) = $pieces;
		// Get the hash algorithm used:
		if (preg_match('#^pbkdf2_([a-z0-9A-Z]+)$#', $header, $m)) {
			$algo = $m[1];
		} else {
			throw new Exception(sprintf("Bad header (%s)", $header));
		}
		if (!in_array($algo, hash_algos())) {
			throw new Exception(sprintf("Illegal hash algorithm (%s)", $algo));
		}

		$calc = hash_pbkdf2(
			$algo,
			$password,
			$salt,
			(int) $iter,
			32,
			true
		);
		return hash_equals($calc, base64_decode($hash));
	}

	function verify_Password($dbString, $password)
	{
		$pieces = explode("$", $dbString);

		$iterations = $pieces[1];
		$salt 		= $pieces[2];
		$old_hash 	= $pieces[3];

		$hash = hash_pbkdf2("SHA256", $password, $salt, $iterations, 32, true);
		$hash = base64_encode($hash);

		if ($hash == $old_hash) {
			// login ok.
			return true;
		} else {
			//login fail
			echo $old_hash . "<br>" . $iterations . "<br>" . $hash;
			return false;
		}
	}

	public function form1()
	{
		return view('form1');
	}

	public function form2()
	{
		return view('form2');
	}
}
