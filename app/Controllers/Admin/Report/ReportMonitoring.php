<?php

namespace App\Controllers\Admin\Report;

use App\Controllers\BaseController;
use App\Models\ReportMonitoringModel;
use App\Models\UserModel;
use App\Models\ProjectModel;
use App\Models\ProjectLocModel;
use App\Models\KsmModel;
use SebastianBergmann\CodeCoverage\Report\Xml\Report;
use Aws\S3\ObjectUploader;
use Aws\S3\S3Client;
use App\Models\ReportReplyModel;
use App\Models\CommentModel;
use App\Models\SubProjectModel;

class ReportMonitoring extends BaseController
{
    public $model;
    public $userModel;
    protected $session = null;
    public $generateTable;
    public $ksmModel;
    public $ProjectModel;
    public $ProjectLocModel;
    public $ReportReplyModel;
    public $commentModel;
    public $SubProjectLModel;

    public function __construct()
    {
        $this->model = new ReportMonitoringModel();
        $this->userModel = new UserModel();
        $this->ProjectModel     = new ProjectModel();
        $this->ProjectLocModel = new ProjectLocModel();
        $this->ksmModel = new KsmModel();
        $this->generateTable    = new \CodeIgniter\View\Table();
        $this->ReportReplyModel = new ReportReplyModel();
        $this->commentModel = new CommentModel();
        $this->SubProjectModel = new SubProjectModel();
        $this->session = session();
    }

    public function create()
    {
        if (isset($this->session->id)) {
            $data = [
                'ksm' => $this->ksmModel->get_data($this->session->id_ksm),
                'pkt_job' => $this->ProjectModel->get_project(),
                'loc' => $this->ProjectLocModel->get_project_loc(),
                'kegiatan' => $this->ProjectLocModel->get_project_loc()
            ];

            return view('admin/report/Monitoring/create', $data);
        } else {
            return redirect()->to(base_url('/main/login'));
        }
    }

    public function index()
    {
        if (isset($this->session->id)) {
            $data = $this->model->getList();

            foreach ($data as $key => $value)
                $data['table'][$key] = array(
                    ($value->title == 'foto_progress' ? 'Umum / Harian' : ($value->title == 'kemajuan_fisik' ? 'Kemajuan Fisik' : ($value->title == 'kas' ? 'Buku Kas' : ($value->title == 'bank' ? 'Buku Bank' : ($value->title == 'dkis' ? 'Buku DKIS' : ($value->title == 'rekening_bank' ? 'Buku Rekening Bank KSM' : ($value->title == 'lppu' ? 'LPPU' : ''))))))),
                    $value->description,  $this->userModel->getPositionById($value->id_user), 'Laporan ' . ucfirst($value->type), ($value->is_publish == 't') ? 'Disetujui' : ($value->is_approval == 4 ? 'Revisi' : 'Menunggu Persetujuan'), '
						<div class="d-sm-flex align-items-center justify-content-between mb-1">
						<a href="' . base_url('/admin/report/ReportMonitoring/view/' . $value->idoffline) . '" class="d-none d-sm-inline-block btn btn-sm btn-info shadow-sm"><i class="fas fa-search text-white-50"></i> View </a>
						</div>'
                );

            $data['table']  = $this->generateTable->generate($data['table']);

            //returning table body
            return view('admin/report/Monitoring/list', $data);
        } else {
            return redirect()->to(base_url('/main/login'));
        }
    }

    public function reject($id = '', $com = '')
    {
        $rprt = $this->model->getList($id);

        $data = [
            'idoffline' => $id,
            'is_approval' => '4'
        ];

        $this->model->save($data);
        $date = date("Y-m-d", time());

        $data_com = [
            'id_report' => $id,
            'id_user' => $rprt->id_user,
            'comment' => $com,
            'entry_date' => $date,
            'delete' => 0
        ];

        $this->commentModel->save($data_com);
        return redirect()->to(base_url('report-monitoring'))->with('success', 'Data report in revision.');
    }

    public function revisi_report($id = '')
    {
        if (isset($this->session->id)) {
            $report = $this->model->getList($id);
            $arrImgs = json_decode($report->images);

            $data = [
                'ksm' => $this->ksmModel->get_data($this->session->id_ksm),
                'pkt_job' => $this->ProjectModel->get_project(),
                'loc' => $this->ProjectLocModel->get_project_loc(),
                'kegiatan' => $this->ProjectLocModel->get_project_loc(),
                'report' => $report,
                'arrImgs' => $arrImgs,
                'project' => $this->SubProjectModel->getSubByProjectId($report->id_project)
            ];

            return view('admin/report/Monitoring/revisi', $data);
        } else {
            return redirect()->to(base_url('/main/login'));
        }
    }

    public function saveData()
    {
        $id_role = $this->session->id_role;
        /* Datetime */
        $timezone  = 0;
        $nowDate = gmdate("Y/m/j H:i:s", time() + 3600 * ($timezone + date("I")));

        /* Upload Images */
        $arrImage = array();
        $arrFile = array();

        if ($files = $this->request->getFiles()) {
            foreach ($files['upload_imgs'] as $file) {

                // DATABASE DATA FORMAT
                $fileName = $file->getRandomName();
                $file->move(WRITEPATH . 'uploads/report/', $fileName); //upload to dir
                $photo_uri          = WRITEPATH . 'uploads/report/' . $fileName;
                $fileName   = $this->s3($fileName, $photo_uri, "report");

                array_push($arrImage, $fileName);
            }

            /* Upload File */
            foreach ($files['upload_file'] as $file_doc) {

                $fileDocName = $file_doc->getRandomName();
                $file_doc->move(WRITEPATH . 'uploads/report/', $fileDocName);
                $file_uri          = WRITEPATH . 'uploads/report/' . $fileDocName;
                $fileDocName   = $this->s3($fileDocName, $file_uri, "report");

                array_push($arrFile,  $fileDocName);
            }
        }

        /* check dokumen name */
        if ($this->request->getVar('dok_name_input') != '') {
            $docType = $this->request->getVar('dok_name_input');
        } else {
            $docType = $this->request->getVar('dok_nameList');
        }

        /* ammount account */
        if ($this->request->getVar('terima') != '') {
            $amm =  str_replace(".00",  "", $this->request->getVar('terima'));
            $ammount =  str_replace(",",  "", $amm);
        } else {
            $ammount = null;
        }

        /* check tgl */
        if ($this->request->getVar('jns_lap') == 'keuangan') {
            $lap_date = null;
            $month = $this->request->getVar('lap_date');
        } else {
            $lap_date = $this->request->getVar('lap_date');
            $month = null;
        }

        $dataSave = [
            'type' => $this->request->getVar('jns_lap'),
            'title' => $this->request->getVar('jdl_lap'),
            'id_project' => $this->request->getVar('pkt_lap'),
            'hibah' => $this->request->getVar('hibah'),
            'id_sub_project' => $this->request->getVar('kegiatanList'),
            'id_project_loc' => $this->request->getVar('lokasi'),
            'progress' => $this->request->getVar('progress'),
            'fund_utilization' => $ammount,
            'vol_plan' => $this->request->getVar('rencana'),
            'vol_real' => $this->request->getVar('volReal'),
            'description' => $this->request->getVar('desc'),
            'start_date' => $this->request->getVar('start_date'),
            'end_date' => $this->request->getVar('end_date'),
            'doc_type' => $docType,
            'status_date' => $lap_date,
            'is_publish' => 'f',
            'is_approval' => ($id_role == 3 || $id_role == 4) ? '0' : '2',
            'delete' => '0',
            'vol_unit' => $this->request->getVar('satPlan'),
            'id_user' =>  $this->session->get('id'),
            'images' => json_encode($arrImage),
            'document' => json_encode($arrFile),
            'progress_month' => $month,
            'entry_date' => $nowDate,
            'edit_date' => $nowDate,
            'report_type' => '2'
        ];

        $this->model->save($dataSave);
        $idReport = $this->model->getInsertID();
        $_a = $this->request->getVar();

        $dataReply = [
            'id_replydata' => $idReport,
            'title' => $_a['jdl_lap'],
            'fund_utilization' => $ammount,
            'description' => $_a['desc'],
            'entry_date' => date('Y-m-d H:i:s'),
            'is_publish' => 't',
            'delete' => '0',
            'id_user' => $this->session->id,
            'images' => json_encode($arrImage),
            'progress' => $_a['progress'],
            'document' => json_encode($arrFile),
            'description' => $_a['desc'],
            'progress_month' => $month
        ];

        $this->ReportReplyModel->save($dataReply);

        return redirect()->to(base_url('report-monitoring'))->with('success', 'Data report successfully saved, waiting approval !');
    }

    public function saveDataRevisi()
    {
        $id_role = $this->session->id_role;
        $images_old = $this->request->getVar("reportImgArr");
        $files_old = $this->request->getVar("reportFileArr");

        /* Array Uploads */
        $arrImage = array();
        $arrFile = array();

        if ($files = $this->request->getFiles()) {

            /* Upload Images */
            foreach ($files['upload_imgs'] as $file) {

                if ($file != '') {
                    $fileName = $file->getRandomName();
                    $file->move(WRITEPATH . 'uploads/report/', $fileName); //upload to dir
                    $photo_uri          = WRITEPATH . 'uploads/report/' . $fileName;
                    $fileName   = $this->s3($fileName, $photo_uri, "report");

                    array_push($arrImage, $fileName);
                } else {
                    foreach (explode(",", $images_old) as $images_file) {
                        array_push($arrImage, $images_file);
                    }
                }
            }

            /* Upload Files */
            foreach ($files['upload_file'] as $file_doc) {

                if ($file_doc != '') {
                    $fileDocName = $file_doc->getRandomName();
                    $file_doc->move(WRITEPATH . 'uploads/report/', $fileDocName);
                    $file_uri          = WRITEPATH . 'uploads/report/' . $fileDocName;
                    $fileDocName   = $this->s3($fileDocName, $file_uri, "report");

                    array_push($arrFile,  $fileDocName);
                } else {
                    foreach (explode(",", $files_old) as $old_file) {
                        array_push($arrFile,  $old_file);
                    }
                }
            }
        }

        $repotid = $this->request->getVar("repotid");
        $jns_lap = $this->request->getVar("jns_lap");
        $jdl_lap = $this->request->getVar("jdl_lap");
        $pkt_lap = $this->request->getVar("pkt_lap");
        $kegiatanList = $this->request->getVar("kegiatanList");
        $lokasi = $this->request->getVar("lokasi");
        $str_date = $this->request->getVar("str_date");
        $end_date = $this->request->getVar("end_date");
        $hibah = $this->request->getVar("hibah");
        $progress = $this->request->getVar("progress");
        $lap_date = $this->request->getVar("lap_date");
        $rencana = $this->request->getVar("rencana");
        $satPlan = $this->request->getVar("satPlan");

        $volReal = $this->request->getVar("volReal");
        $terima = $this->request->getVar("terima");
        $desc = $this->request->getVar("desc");

        $dok_name_input = $this->request->getVar("dok_name_input");
        $dok_nameList = $this->request->getVar("dok_nameList");

        /* check dokumen name */
        if ($dok_name_input != '') {
            $docType = $dok_name_input;
        } else {
            $docType = $dok_nameList;
        }

        /* ammount account */
        $amm =  str_replace(",00",  "", $terima);
        $ammount =  str_replace(".",  "", $amm);

        /* check tgl */
        if ($this->request->getVar('jns_lap') == 'keuangan') {
            $lap_date = null;
            $month = $this->request->getVar('lap_date');
        } else {
            $lap_date = $this->request->getVar('lap_date');
            $month = null;
        }

        /* update report */
        $dataSave = [
            'idoffline' => $repotid,
            'type' => $jns_lap,
            'title' => $jdl_lap,
            'id_project' => $pkt_lap,
            'hibah' =>  $hibah,
            'id_sub_project' => $kegiatanList,
            'id_project_loc' => $lokasi,
            'progress' => $progress,
            'fund_utilization' => $ammount,
            'vol_plan' => $rencana,
            'vol_real' => $volReal,
            'description' => $desc,
            'start_date' => $str_date,
            'end_date' => $end_date,
            'doc_type' => $docType,
            'status_date' => $lap_date,
            'is_publish' => 'f',
            'is_approval' => ($id_role == 3 || $id_role == 4) ? '0' : '2',
            'delete' => '0',
            'vol_unit' => $satPlan,
            'id_user' =>  $this->session->get('id'),
            'images' => json_encode($arrImage),
            'document' => json_encode($arrFile),
            'progress_month' => $month
        ];

        $this->model->save($dataSave);
        return redirect()->to(base_url('report-monitoring'))->with('success', 'Data report successfully saved, waiting approval !');
    }

    public function saveProgress()
    {
        if (isset($this->session->id)) {
            $idReport = $this->request->getVar('idReport');

            if ($this->session->id_role == 9 || $this->session->id_role == 10) {

                $data = [
                    'idoffline' => $idReport,
                    'is_approval' => ($this->session->id_role == 9) ? '1' : '3',
                    'is_publish' => 't'
                ];

                $this->model->save($data);

                return redirect()->to(base_url('report-monitoring'))->with('success', 'Data report successfully approved.');
            } else {
                $idRpt = $this->request->getVar('idReport');
                $reportData = $this->model->getList($idRpt);
                $time = new \DateTime($reportData->entry_date);
                $result = $time->format("m/d/yy");

                $data = [
                    'ksm' => $this->ksmModel->get_data($this->session->id_ksm),
                    'getReport' => $reportData,
                    'getProject' => $this->ProjectModel->get_project($reportData->id_project),
                    'getSubProject' => $this->SubProjectModel->get_sub_project($reportData->id_sub_project),
                    'getLocProject' => $this->ProjectLocModel->get_project_loc($reportData->id_project_loc),
                    'pkt_job' => $this->ProjectModel->get_project(),
                    'loc' => $this->ProjectLocModel->get_project_loc(),
                    'kegiatan' => $this->ProjectLocModel->get_project_loc(),
                    'createdd' => $result
                ];
                // print_r($reportData);die;
                return view('admin/report/Monitoring/view_progress', $data);
            }
        } else {
            return redirect()->to(base_url('/main/login'));
        }
    }

    public function s3($key, $uri, $dir)
    {
        $s3 = new S3Client([
            'region'  => 'ap-southeast-1',
            'version' => 'latest',
            'credentials' => [
                'key'    => 'AKIAQDS7UK2EVXQZVK4N',
                'secret' => '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'
            ]
        ]);

        // Send a PutObject request and get the result object.
        $key = $dir . "/" . $key;

        $result = $s3->putObject([
            'Bucket'        => 'worldbank',
            'Key'           => $key,
            'SourceFile'    => $uri,
            'ACL'           => 'public-read',

        ]);

        // Print the body of the result by indexing into the result object.
        // var_dump($result["@metadata"]["effectiveUri"]);

        return $result["@metadata"]["effectiveUri"];
    }

    public function view($id = '')
    {
        if (isset($this->session->id)) {
            $reportData = $this->model->getList($id);
            $arrImgs = json_decode($reportData->images);
            $file = json_decode($reportData->document);
            $replyRep = $this->ReportReplyModel->get_replyByReport($id);
            $revisi = $this->commentModel->get_data_by_reportid_userid($id, $reportData->id_user);

            $data = [
                'ksm' => $this->ksmModel->get_data($this->session->id_ksm),
                'getReport' => $reportData,
                'arrImgs' => $arrImgs,
                'file' => $file,
                'replyRep' => $replyRep,
                'getProject' => $this->ProjectModel->get_project($reportData->id_project),
                'getSubProject' => $this->SubProjectModel->get_sub_project($reportData->id_sub_project),
                'getLocProject' => $this->ProjectLocModel->get_project_loc($reportData->id_project_loc),
                'revisi' => $revisi,
                'user' => $this->session
            ];

            return view('admin/report/Monitoring/view', $data);
        } else {
            return redirect()->to(base_url('/main/login'));
        }
    }
}
