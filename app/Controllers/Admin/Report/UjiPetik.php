<?php

namespace App\Controllers\Admin\Report;

use App\Controllers\BaseController;
use App\Models\ProvinceModel;
use App\Models\DistrictModel;
use App\Models\SubdistrictModel;
use App\Models\VillageModel;
use App\Models\UjiPetikModel;
use App\Models\CommentModel;

use Aws\S3\S3Client;
use Aws\S3\ObjectUploader;
use Aws\Exception\AwsException;
use CodeIgniter\HTTP\Request;
use CodeIgniter\HTTP\Files\UploadedFile;

class UjiPetik extends BaseController
{
    protected $session = null;
    public $model;
    public $client;
    public $request = null;
    public $generateTable;
    public $commentModel;

    public function __construct()
    {
        $this->generateTable    = new \CodeIgniter\View\Table();
        $this->request          = \Config\Services::request();
        $this->client           = \Config\Services::curlrequest();
        $this->UjiPetikModel    = new UjiPetikModel();
        $this->session          = session();
        $this->ProvinceModel    = new ProvinceModel();
        $this->DistrictModel    = new DistrictModel();
        $this->SubdistrictModel = new SubdistrictModel();
        $this->VillageModel     = new VillageModel();
        $this->commentModel = new CommentModel();
    }

    public function index()
    {
        // if (isset($this->session->id)) {
        $d = $this->UjiPetikModel->get_data();

        foreach ($d as $key => $value) {
            if ($value->is_reject == 1 && ($this->session->id == $value->id_user)) {
                $btn_rev = '<a href="' . base_url('/admin/report/UjiPetik/revisi/' . $value->id) . '" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i class="fas fa-pen text-white-50"></i> Revisi </a>';
            } else {
                $btn_rev = '';
            }

            if ($value->is_reject == 1) {
                $s = '<span class="badge badge-alert">Data di tolak</span>';
            } else if ($value->is_approved == 1) {
                $s = '<span class="badge badge-success">Data di setujui</span>';
            } else {
                $s = '<span class="badge badge-secondary">Menunggu Persetujuan</span>';
            }


            $data['table'][$key] = array($value->jenis_kegiatan, $value->tema, '
                        <div class="d-sm-flex align-items-center justify-content-between mb-1">
                        ' . $btn_rev . '
                            <a href="' . base_url('/admin/report/UjiPetik/detail/' . $value->id) . '" class="d-none d-sm-inline-block btn btn-sm btn-warning shadow-sm"><i class="fas fa-book text-white-50"></i></a>
                            <a href="' . base_url('/admin/report/UjiPetik/edit/' . $value->id) . '" class="d-none d-sm-inline-block btn btn-sm btn-info shadow-sm"><i class="fas fa-pen text-white-50"></i></a>
                            <a href="' . base_url('/admin/report/UjiPetik/delete/' . $value->id) . '" class="d-none d-sm-inline-block btn btn-sm btn-danger shadow-sm" onclick="return confirm(\'Apakah akan menghapus data ?\');"><i class="fas fa-trash text-white-50"></i></a>
                        </div>');
        }

        $data['table']  = $this->generateTable->generate($data['table']);

        //returning table body
        return view('admin/report/UjiPetik/list', $data);
        // } else {
        //     return redirect()->to(base_url('/main/login'));
        // }
    }

    public function create()
    {
        if (isset($this->session->id)) {
            $kec = $this->SubdistrictModel->get_data($kel->id_kecamatan);
            $kab = $this->DistrictModel->get_data($kec->id_kabupaten);
            $prov = $this->ProvinceModel->get_data($kab->id_province);
            $data = [
                'province'  => $this->ProvinceModel->get_data(),
                'district'  => $this->DistrictModel->getKabByProvinceId($prov->id),
                'sub'       => $this->SubdistrictModel->getSubByDistrictId($kab->id),
                'village'   => $this->VillageModel->getVilBySubId($kec->id),
            ];
            return view('admin/report/UjiPetik/create', $data);
        } else {
            return redirect()->to(base_url('/main/login'));
        }
    }

    public function save()
    {
        $timezone  = 0;
        $nowDate = gmdate("Y/m/j H:i:s", time() + 3600 * ($timezone + date("I")));

        $arrFile = array();

        if ($files = $this->request->getFiles()) {
            /* Upload File */
            foreach ($files['upload_dokumen'] as $file_doc) {

                $fileDocName = $file_doc->getRandomName();
                $file_doc->move(WRITEPATH . 'uploads/ujipetik/', $fileDocName);
                $file_uri          = WRITEPATH . 'uploads/ujipetik/' . $fileDocName;
                $fileDocName   = $this->s3($fileDocName, $file_uri, "ujipetik");

                array_push($arrFile,  $fileDocName);
            }
        }
        $data_ = [
            'id_user'           => $this->session->id,
            'jenis_kegiatan'    => $this->request->getVar('jenis_kegiatan'),
            'tingkat_kegiatan'  => $this->request->getVar('tingkat_kegiatan'),
            'start_date'        => $this->request->getVar('start_date'),
            'end_date'          => $this->request->getVar('end_date'),
            'tema'              => $this->request->getVar('tema'),
            'fokus_kegiatan'    => $this->request->getVar('fokus_kegiatan'),
            'id_provinsi'       => $this->request->getVar('id_provinsi'),
            'id_kabupaten'      => $this->request->getVar('id_kabupaten'),
            'id_kecamatan'      => $this->request->getVar('id_kecamatan'),
            'id_kelurahan'      => $this->request->getVar('id_kelurahan'),
            'topik_pendanaan'   => $this->request->getVar('topik_pendanaan'),
            'upload_dokumen'    => json_encode($arrFile),
            'kategori_dokumen'  => $this->request->getVar('kategori_dokumen'),
            'catatan_hasil'     => $this->request->getVar('catatan_hasil'),
            'kesimpulan'        => $this->request->getVar('kesimpulan'),
            'rekomendasi'       => $this->request->getVar('rekomendasi'),
            'delete'            => '0',
            'entry_date'        => $nowDate,
            'id_provinsi_pic'   => $this->request->getVar('id_provinsi_pic'),
            'id_kabupaten_pic'  => $this->request->getVar('id_kabupaten_pic'),
            'id_kecamatan_pic'  => $this->request->getVar('id_kecamatan_pic'),
            'id_kelurahan_pic'  => $this->request->getVar('id_kelurahan_pic'),
            'nama_pic'          => $this->request->getVar('nama_pic'),
            'is_approved'       => 0,
            'is_reject'         => 0,
            'tingkat_pic'       => $this->request->getVar('tingkat_pic'),
            'osp_pic'           => $this->request->getVar('osp_pic'),
            'korkot_pic'        => $this->request->getVar('korkot_pic'),
        ];

        // print_r($data_);
        // die;

        $save = $this->UjiPetikModel->save($data_);
        if ($save) {
            return redirect()->to(base_url('uji-petik'))->with('success', 'Successfully saved.');
        } else {
            return redirect()->back()->withInput()->with('error', 'We had trouble saving your data.');
        }
    }

    public function delete($id)
    {
        $data = [
            'id' => $id,
            'delete' => '1'
        ];

        $this->UjiPetikModel->save($data);

        return redirect()->to(base_url('uji-petik'))->with('success', 'Data successfully deleted.');
    }

    public function edit($id = '')
    {
        if (isset($this->session->id)) {
            $kec = $this->SubdistrictModel->get_data($kel->id_kecamatan);
            $kab = $this->DistrictModel->get_data($kec->id_kabupaten);
            $prov = $this->ProvinceModel->get_data($kab->id_province);
            $data = [
                'isEdit' => 'edit',
                'getUjiPetik' => $this->UjiPetikModel->get_data($id),
                'kelurahan' => $this->VillageModel->get_data(),
                'province'  => $this->ProvinceModel->get_data(),
                'district'  => $this->DistrictModel->get_data(),
                'sub'       => $this->SubdistrictModel->get_data()
            ];
            // print_r($this->UjiPetikModel->get_data($id));
            // die;
            return view('admin/report/UjiPetik/edit', $data);
        } else {
            return redirect()->to(base_url('/main/login'));
        }
    }

    public function save_update($action = null)
    {
        $timezone  = 0;
        $nowDate = gmdate("Y/m/j H:i:s", time() + 3600 * ($timezone + date("I")));

        $arrFile = array();

        if ($files = $this->request->getFiles()) {
            /* Upload File */
            foreach ($files['upload_dokumen'] as $file_doc) {

                $fileDocName = $file_doc->getRandomName();
                $file_doc->move(WRITEPATH . 'uploads/ujipetik/', $fileDocName);
                $file_uri          = WRITEPATH . 'uploads/ujipetik/' . $fileDocName;
                $fileDocName   = $this->s3($fileDocName, $file_uri, "ujipetik");

                array_push($arrFile,  $fileDocName);
            }
        }
        $data = [
            'id_user'           => $this->session->get('id'),
            'id'                => $this->request->getVar('id'),
            'jenis_kegiatan'    => $this->request->getVar('jenis_kegiatan'),
            'tingkat_kegiatan'  => $this->request->getVar('tingkat_kegiatan'),
            'start_date'        => $this->request->getVar('start_date'),
            'end_date'          => $this->request->getVar('end_date'),
            'tema'              => $this->request->getVar('tema'),
            'fokus_kegiatan'    => $this->request->getVar('fokus_kegiatan'),
            'id_provinsi'       => $this->request->getVar('id_provinsi'),
            'id_kabupaten'      => $this->request->getVar('id_kabupaten'),
            'id_kecamatan'      => $this->request->getVar('id_kecamatan'),
            'id_kelurahan'      => $this->request->getVar('id_kelurahan'),
            'topik_pendanaan'   => $this->request->getVar('topik_pendanaan'),
            'upload_dokumen'    => json_encode($arrFile),
            'kategori_dokumen'  => $this->request->getVar('kategori_dokumen'),
            'catatan_hasil'     => $this->request->getVar('catatan_hasil'),
            'kesimpulan'        => $this->request->getVar('kesimpulan'),
            'rekomendasi'       => $this->request->getVar('rekomendasi'),
            'delete'            => '0',
            'edit_date'         => $nowDate,
            'id_provinsi_pic'   => $this->request->getVar('id_provinsi_pic'),
            'id_kabupaten_pic'  => $this->request->getVar('id_kabupaten_pic'),
            'id_kecamatan_pic'  => $this->request->getVar('id_kecamatan_pic'),
            'id_kelurahan_pic'  => $this->request->getVar('id_kelurahan_pic'),
            'nama_pic'          => $this->request->getVar('nama_pic'),
            'tingkat_pic'       => $this->request->getVar('tingkat_pic'),
            'osp_pic'           => $this->request->getVar('osp_pic'),
            'korkot_pic'        => $this->request->getVar('korkot_pic'),
        ];

        if ($action != null) {
            $data['is_reject'] = 0;
        }

        $save = $this->UjiPetikModel->save($data);
        if ($save) {
            return redirect()->to(base_url('uji-petik'))->with('success', 'Data successfully saved.');
        } else {
            return redirect()->back()->withInput()->with('error', 'We had trouble saving your data.');
        }
    }

    public function getKabByProv()
    {
        $prov_id = $this->request->getVar('id_provinsi');
        $data = $this->DistrictModel->getKabByProvinceId($prov_id);
        echo json_encode($data);
    }

    public function getSubByDistrict()
    {
        $dist_id = $this->request->getVar('id_kabupaten');
        // $data = $this->SubdistrictModel->getSubByDistrictId($dist_id);
        $data = $this->UjiPetikModel->getSubDistrictByCity($dist_id);
        echo json_encode($data);
    }

    public function getVilBySub()
    {
        $sub_id = $this->request->getVar('id_kecamatan');
        $data = $this->VillageModel->getVilBySubId($sub_id);
        echo json_encode($data);
    }

    public function s3($key, $uri, $dir)
    {
        $s3 = new S3Client([
            'region'  => 'ap-southeast-1',
            'version' => 'latest',
            'credentials' => [
                'key'    => 'AKIAQDS7UK2EVXQZVK4N',
                'secret' => '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'
            ]
        ]);

        // Send a PutObject request and get the result object.
        $key = $dir . "/" . $key;

        $result = $s3->putObject([
            'Bucket'        => 'worldbank',
            'Key'           => $key,
            'SourceFile'    => $uri,
            'ACL'           => 'public-read',

        ]);

        // Print the body of the result by indexing into the result object.
        // var_dump($result["@metadata"]["effectiveUri"]);

        return $result["@metadata"]["effectiveUri"];
    }

    public function detail($id = '')
    {
        if (isset($this->session->id)) {
            $admin = session()->get('admin');
            $detailData = $this->UjiPetikModel->getDetail($id);
            $file = json_decode($detailData->upload_dokumen);
            $data = [
                'getDetail' => $detailData,
                'file'      => $file,
                'id_role'     => $this->session->id_role
            ];

            return view('admin/report/UjiPetik/detail', $data);
        } else {
            return redirect()->to(base_url('/main/login'));
        }
    }

    public function reject($id = '', $com = '')
    {
        $detailData = $this->UjiPetikModel->getDetail($id);

        $data = [
            'id' => $id,
            'is_reject' => 1
        ];

        $this->UjiPetikModel->save($data);
        $date = date("Y-m-d", time());

        $data_com = [
            'id_report' => $id,
            'id_user' => $detailData->id_user,
            'comment' => $com,
            'entry_date' => $date,
            'type' => 1,
            'delete' => 0
        ];

        $this->commentModel->save($data_com);
        return redirect()->to(base_url('uji-petik'))->with('success', 'Data in revision.');
    }

    public function revisi($id)
    {
        if (isset($this->session->id)) {
            $data = [
                'isEdit' => 'edit',
                'getUjiPetik' => $this->UjiPetikModel->get_data($id),
                'kelurahan' => $this->VillageModel->get_data(),
                'province'  => $this->ProvinceModel->get_data(),
                'district'  => $this->DistrictModel->get_data(),
                'sub'       => $this->SubdistrictModel->get_data(),
                'comment'   => $this->commentModel->get_data_by_reportid_userid($id, $this->session->id, 1)
            ];
            // print_r($this->UjiPetikModel->get_data($id));die;
            return view('admin/report/UjiPetik/revisi', $data);
        } else {
            return redirect()->to(base_url('/main/login'));
        }
    }

    public function approve()
    {
        $id = $this->request->getVar('id');

        $data = [
            'id' => $id,
            'is_approved' => 1
        ];

        $save = $this->UjiPetikModel->save($data);
        if ($save) {
            return redirect()->to(base_url('uji-petik'))->with('success', 'Data successfully approved.');
        } else {
            return redirect()->back()->withInput()->with('error', 'We had trouble saving your data.');
        }
    }

    public function export()
    {
        $data = $this->UjiPetikModel->get_data_export();

        $html = "<html>
                    <head></head>
                    <body>
                        <table border=1>
                            <tr>
                                <th rowspan=2>Jenis Kegiatan</th>
                                <th rowspan=2>Tingkat Kegiatan</th>
                                <th rowspan=2>Tanggal Mulai</th>
                                <th rowspan=2>Tema</th>
                                <th rowspan=2>Fokus Kegiatan</th>
                                <th colspan=3>Lokasi Kegiatan</th>
                                <th colspan=4>Asal PIC</th>
                                <th rowspan=2>Topik Pendanaan</th>
                                <th rowspan=2>Keterangan Upload Dokumen</th>
                                <th rowspan=2>Kategori Dokumen</th>
                                <th rowspan=2>Catatan Hasil</th>
                                <th rowspan=2>Kesimpulan</th>
                                <th rowspan=2>Rekomendasi</th>
                            </tr>
                            <tr>
                                <td>Provinsi</td>
                                <td>Kabupaten</td>
                                <td>Kelurahan</td>
                                <td>Tingkat PIC</td>
                                <td>OSP PIC</td>
                                <td>Korkot PIC</td>
                                <td>Nama PIC</td>
                            </tr>";
        foreach ($data as $key => $value) {
            $html .= "<tr>
                        <td>" . $value->jenis_kegiatan . "</td>
                        <td>" . $value->tingkat_kegiatan . "</td>
                        <td>" . date('d M Y', strtotime($value->start_date)) . " sampai " . date('d M Y', strtotime($value->end_date)) . "</td>
                        <td>" . $value->tema . "</td>
                        <td>" . $value->fokus_kegiatan . "</td>
                        <td>" . $value->provinsi . "</td>
                        <td>" . $value->kabupaten . "</td>
                        <td>" . $value->kelurahan . "</td>
                        <td>" . $value->tingkat_pic . "</td>
                        <td>" . $value->osp_pic . "</td>
                        <td>" . $value->korkot_pic . "</td>
                        <td>" . $value->nama_pic . "</td>
                        <td>" . str_replace('(', '', str_replace(')', '', str_replace('_', ' ', ucfirst($value->topik_pendanaan)))) . "</td>
                        <td>" . (($value->upload_dokumen == null) ? 'Tidak' : 'Ya') . "</td>
                        <td>" . $value->kategori_dokumen . "</td>
                        <td>" . $value->catatan_hasil . "</td>
                        <td>" . $value->kesimpulan . "</td>
                        <td>" . $value->rekomendasi . "</td>
                    </tr>";
        }
        header('Content-type: application/ms-excel');
        header('Content-Disposition: attachment; filename=Report Uji Petik.xls');
        header('Cache-Control: max-age=0');
        echo $html;
    }

    public function dashboard()
    {
        if (isset($this->session->id)) {
            $data = [
                'jenis_kegiatan' => $this->UjiPetikModel->get_jumlah_jenis_kegiatan('number'),
                'jenis_kegiatan_pie' => $this->UjiPetikModel->get_jumlah_jenis_kegiatan(),
                'tema_kegiatan' => $this->UjiPetikModel->get_tema_kegiatan(),
                'jumlah_provinsi' => $this->UjiPetikModel->get_total_provinsi(),
                'jumlah_kabupaten' => $this->UjiPetikModel->get_total_kabupaten(),
                'jumlah_kelurahan' => $this->UjiPetikModel->get_total_kelurahan(),
            ];
            return view('admin/report/UjiPetik/dashboard', $data);
        } else {
            return redirect()->to(base_url('/main/login'));
        }
    }

    public function getJenisByProvince()
    {
        echo json_encode($this->UjiPetikModel->getJenisByProvince($this->request->getPost('tingkat')));
    }
}
