<?php

namespace App\Controllers\Admin\Report;

use App\Controllers\BaseController;
use App\Models\CommentModel;
use App\Models\KsmModel;
use App\Models\ReportModel;
use App\Models\ProjectModel;
use App\Models\ProjectLocModel;
use App\Models\SubProjectModel;
use App\Models\ReportReplyModel;
use App\Models\UserModel;

use CodeIgniter\HTTP\Files\UploadedFile;
use CodeIgniter\HTTP\Request;
use Aws\Exception\AwsException;
use Aws\S3\ObjectUploader;
use Aws\S3\S3Client;

use DateTime;
use PHPExcel;
use PHPExcel_IOFactory;

ini_set('memory_limit', '1024M');
ini_set('upload_max_filesize', '200000M');
class ReportCon extends BaseController
{
	protected $session = null;
	public $ReportModel;
	public $generateTable;
	public $ProjectModel;
	public $ProjectLocModel;
	public $SubProjectLModel;
	public $ReportReplyModel;
	// public $session;
	public $ksmModel;
	public $userModel;
	public $commentModel;

	public function __construct()
	{
		$this->session = session();
		$this->ReportModel      = new ReportModel();
		$this->ProjectModel     = new ProjectModel();
		$this->ProjectLocModel = new ProjectLocModel();
		$this->SubProjectModel = new SubProjectModel();
		$this->ReportReplyModel = new ReportReplyModel();
		$this->generateTable    = new \CodeIgniter\View\Table();
		$this->session = session();
		$this->ksmModel = new KsmModel();
		$this->userModel = new UserModel();
		$this->commentModel = new CommentModel();
	}

	public function saveData()
	{

		/* Datetime */
		$timezone  = 0;
		$nowDate = gmdate("Y/m/j H:i:s", time() + 3600 * ($timezone + date("I")));

		/* Upload Images */
		$arrImage = array();
		$arrFile = array();

		if ($files = $this->request->getFiles()) {
			foreach ($files['upload_imgs'] as $file) {

				// DATABASE DATA FORMAT
				$fileName = $file->getRandomName();
				$file->move(WRITEPATH . 'uploads/report/', $fileName); //upload to dir
				$photo_uri          = WRITEPATH . 'uploads/report/' . $fileName;
				$fileName   = $this->s3($fileName, $photo_uri, "report");

				array_push($arrImage, $fileName);
			}

			/* Upload File */
			foreach ($files['upload_file'] as $file_doc) {

				$fileDocName = $file_doc->getRandomName();
				$file_doc->move(WRITEPATH . 'uploads/report/', $fileDocName);
				$file_uri          = WRITEPATH . 'uploads/report/' . $fileDocName;
				$fileDocName   = $this->s3($fileDocName, $file_uri, "report");

				array_push($arrFile,  $fileDocName);
			}
		}

		/* check dokumen name */
		if ($this->request->getVar('dok_name_input') != '') {
			$docType = $this->request->getVar('dok_name_input');
		} else {
			$docType = $this->request->getVar('dok_nameList');
		}

		/* ammount account */
		if ($this->request->getVar('terima') != '') {
			$amm =  str_replace(".00",  "", $this->request->getVar('terima'));
			$ammount =  str_replace(",",  "", $amm);
		} else {
			$ammount = null;
		}

		/* check tgl */
		if ($this->request->getVar('jns_lap') == 'keuangan') {
			$lap_date = null;
			$month = $this->request->getVar('lap_date');
		} else {
			$lap_date = $this->request->getVar('lap_date');
			$month = null;
		}

		$dataSave = [
			'type' => $this->request->getVar('jns_lap'),
			'title' => $this->request->getVar('jdl_lap'),
			'id_project' => $this->request->getVar('pkt_lap'),
			'hibah' => $this->request->getVar('hibah'),
			'id_sub_project' => $this->request->getVar('kegiatanList'),
			'id_project_loc' => $this->request->getVar('lokasi'),
			'progress' => $this->request->getVar('progress'),
			'fund_utilization' => $ammount,
			'vol_plan' => $this->request->getVar('rencana'),
			'vol_real' => $this->request->getVar('volReal'),
			'description' => $this->request->getVar('desc'),
			'start_date' => $this->request->getVar('start_date'),
			'end_date' => $this->request->getVar('end_date'),
			'doc_type' => $docType,
			'status_date' => $lap_date,
			'is_publish' => 'f',
			'is_approval' => '0',
			'delete' => '0',
			'vol_unit' => $this->request->getVar('satPlan'),
			'id_user' =>  $this->session->get('id'),
			'images' => json_encode($arrImage),
			'document' => json_encode($arrFile),
			'progress_month' => $month,
			'entry_date' => $nowDate,
			'edit_date' => $nowDate,
			'report_type' => 1
		];

		$this->ReportModel->save($dataSave);
		$idReport = $this->ReportModel->getInsertID();
		$_a = $this->request->getVar();

		$dataReply = [
			'id_replydata' => $idReport,
			'title' => $_a['jdl_lap'],
			'fund_utilization' => $ammount,
			'description' => $_a['desc'],
			'entry_date' => date('Y-m-d H:i:s'),
			'is_publish' => 't',
			'delete' => '0',
			'id_user' => $this->session->id,
			'images' => json_encode($arrImage),
			'progress' => $_a['progress'],
			'document' => json_encode($arrFile),
			'description' => $_a['desc'],
			'progress_month' => $month
		];

		$this->ReportReplyModel->save($dataReply);

		return redirect()->to(base_url('admin/report'))->with('success', 'Data report successfully saved, waiting approval !');
	}

	public function index()
	{
		if (isset($this->session->id)) {
			$isApp = $this->session->id_role;
			// print_r($this->session);die;
			// echo $isApp;
			// die;
			if ($isApp == 3 || $isApp == 4 || $isApp == 5 || $isApp == 6 || $isApp == 9) {
				$data   = $this->ReportModel->get_index_kel($isApp);
			} else if ($this->session->position == 'KORKOT 01 - OSP- 2 - JATENG') {
				$data   = $this->ReportModel->getList($this->session->position);
			} else if ($isApp == 7) {
				$data   = $this->ReportModel->getList($this->session->position);
			} else {
				$data   = $this->ReportModel->getList($this->session->id);
			}

			foreach ($data as $key => $value) {
				if ($isApp == 5 || $isApp == 9) {
					$data['table'][$key] = array(
						($value->title == 'foto_progress' ? 'Umum / Harian' : ($value->title == 'kemajuan_fisik' ? 'Kemajuan Fisik' : ($value->title == 'kas' ? 'Buku Kas' : ($value->title == 'bank' ? 'Buku Bank' : ($value->title == 'dkis' ? 'Buku DKIS' : ($value->title == 'rekening_bank' ? 'Buku Rekening Bank KSM' : ($value->title == 'lppu' ? 'LPPU' : ''))))))),
						$value->description,
						$this->userModel->getPositionById($value->id_user),
						$value->kelurahan,
						$value->kota,
						'Laporan ' . ucfirst($value->type),
						($value->is_publish == 't') ? 'Disetujui' : ($value->is_approval == 3 ? 'Revisi' : 'Menunggu Persetujuan'),
						'<div class="d-sm-flex align-items-center justify-content-between mb-1">
							<a href="' . base_url('/admin/report/reportCon/view/' . $value->idoffline) . '" class="d-none d-sm-inline-block btn btn-sm btn-info shadow-sm"><i class="fas fa-search text-white-50"></i> View </a>
							</div>'
					);
				} else if ($isApp == 10 || $isApp == 11) {
					$data['table'][$key] = array(
						($value->title == 'foto_progress' ? 'Umum / Harian' : ($value->title == 'kemajuan_fisik' ? 'Kemajuan Fisik' : ($value->title == 'kas' ? 'Buku Kas' : ($value->title == 'bank' ? 'Buku Bank' : ($value->title == 'dkis' ? 'Buku DKIS' : ($value->title == 'rekening_bank' ? 'Buku Rekening Bank KSM' : ($value->title == 'lppu' ? 'LPPU' : ''))))))),
						$value->description,
						$this->userModel->getPositionById($value->id_user),
						$value->kelurahan,
						$value->kota,
						$value->provinsi,
						'Laporan ' . ucfirst($value->type),
						($value->is_publish == 't') ? 'Disetujui' : ($value->is_approval == 3 ? 'Revisi' : 'Menunggu Persetujuan'),
						'<div class="d-sm-flex align-items-center justify-content-between mb-1">
							<a href="' . base_url('/admin/report/reportCon/view/' . $value->idoffline) . '" class="d-none d-sm-inline-block btn btn-sm btn-info shadow-sm"><i class="fas fa-search text-white-50"></i> View </a>
							</div>'
					);
				} else {
					$data['table'][$key] = array(
						($value->title == 'foto_progress' ? 'Umum / Harian' : ($value->title == 'kemajuan_fisik' ? 'Kemajuan Fisik' : ($value->title == 'kas' ? 'Buku Kas' : ($value->title == 'bank' ? 'Buku Bank' : ($value->title == 'dkis' ? 'Buku DKIS' : ($value->title == 'rekening_bank' ? 'Buku Rekening Bank KSM' : ($value->title == 'lppu' ? 'LPPU' : ''))))))),
						$value->description,
						($isApp == 12) ? $value->name_role : $this->userModel->getPositionById($value->id_user),
						$value->kelurahan,
						'Laporan ' . ucfirst($value->type),
						($value->is_publish == 't') ? 'Disetujui' : ($value->is_approval == 3 ? 'Revisi' : 'Menunggu Persetujuan'),
						'<div class="d-sm-flex align-items-center justify-content-between mb-1">
							<a href="' . base_url('/admin/report/reportCon/view/' . $value->idoffline) . '" class="d-none d-sm-inline-block btn btn-sm btn-info shadow-sm"><i class="fas fa-search text-white-50"></i> View </a>
						</div>'
					);
				}
			}

			$data['table']  = $this->generateTable->generate($data['table']);

			//returning table body
			return view('admin/report/list', $data);
		} else {
			return redirect()->to(base_url('/main/login'));
		}
	}

	public function create()
	{
		if (isset($this->session->id)) {
			$data = [
				'ksm' => $this->ksmModel->get_data($this->session->id_ksm),
				'pkt_job' => $this->ProjectModel->get_project(),
				'loc' => $this->ReportModel->getLoc($this->session->id_ksm),
				'kegiatan' => $this->ProjectLocModel->get_project_loc()
			];

			return view('admin/report/create', $data);
		} else {
			return redirect()->to(base_url('/main/login'));
		}
	}

	function getSubProjectById()
	{
		$id = $this->request->getVar('pkt_lap');
		$data = $this->SubProjectModel->getSubByProjectId($id);
		echo json_encode($data);
	}

	public function view($id = '')
	{
		if (isset($this->session->id)) {
			$reportData = $this->ReportModel->get_report($id);
			$arrImgs = json_decode($reportData->images);
			$file = json_decode($reportData->document);
			$replyRep = $this->ReportReplyModel->get_replyByReport($id);
			$usr = $this->userModel->get_user($this->session->id);
			$revisi = $this->commentModel->get_data_by_reportid_userid($id, $reportData->id_user);
			// print_r($usr);die;
			if ($usr->id_role == 3) {
				$usr_type = 'infrastruktur';
			} else if ($usr->id_role == 4) {
				$usr_type = 'keuangan';
			} else if ($usr->id_role == 5) {
				$usr_type = 'korkot';
			} else {
				$usr_type = 'ksm';
			}

			$data = [
				'ksm' => $this->ksmModel->get_data($this->session->id_ksm),
				'getReport' => $reportData,
				'arrImgs' => $arrImgs,
				'file' => $file,
				'replyRep' => $replyRep,
				'getProject' => $this->ProjectModel->get_project($reportData->id_project),
				'getSubProject' => $this->SubProjectModel->get_sub_project($reportData->id_sub_project),
				'getLocProject' => $this->ProjectLocModel->get_project_loc($reportData->id_project_loc),
				'usr_type' =>  $usr_type,
				'revisi' => $revisi
			];
			// print_r($data);die;
			// print_r($reportData);die;
			// dd($data);
			return view('admin/report/view', $data);
		} else {
			return redirect()->to(base_url('/main/login'));
		}
	}

	//saving file to s3
	public function s3($key, $uri, $dir)
	{
		$s3 = new S3Client([
			'region'  => 'ap-southeast-1',
			'version' => 'latest',
			'credentials' => [
				'key'    => 'AKIAQDS7UK2EVXQZVK4N',
				'secret' => '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'
			]
		]);

		// Send a PutObject request and get the result object.
		$key = $dir . "/" . $key;

		$result = $s3->putObject([
			'Bucket'        => 'worldbank',
			'Key'           => $key,
			'SourceFile'    => $uri,
			'ACL'           => 'public-read',

		]);

		// Print the body of the result by indexing into the result object.
		// var_dump($result["@metadata"]["effectiveUri"]);

		return $result["@metadata"]["effectiveUri"];
	}

	public function saveProgress()
	{
		if (isset($this->session->id)) {
			$idReport = $this->request->getVar('idReport');

			if ($this->session->id_role == 3 || $this->session->id_role == 4) {

				$data = [
					'idoffline' => $idReport,
					'is_approval' => '1',
					'is_publish' => 't'
				];

				$this->ReportModel->save($data);

				return redirect()->to(base_url('admin/report'))->with('success', 'Data report successfully approved.');
			} else {
				$idRpt = $this->request->getVar('idReport');
				$reportData = $this->ReportModel->get_report($idRpt);
				$time = new DateTime($reportData->entry_date);
				$result = $time->format("m/d/yy");

				$data = [
					'ksm' => $this->ksmModel->get_data($this->session->id_ksm),
					'getReport' => $reportData,
					'getProject' => $this->ProjectModel->get_project($reportData->id_project),
					'getSubProject' => $this->SubProjectModel->get_sub_project($reportData->id_sub_project),
					'getLocProject' => $this->ProjectLocModel->get_project_loc($reportData->id_project_loc),
					'pkt_job' => $this->ProjectModel->get_project(),
					'loc' => $this->ProjectLocModel->get_project_loc(),
					'kegiatan' => $this->ProjectLocModel->get_project_loc(),
					'createdd' => $result
				];
				// print_r($reportData);die;
				return view('admin/report/view_progress', $data);
			}
		} else {
			return redirect()->to(base_url('/main/login'));
		}
	}

	public function reject($id = '', $com = '')
	{
		$rprt = $this->ReportModel->get_report($id);

		$data = [
			'idoffline' => $id,
			'is_approval' => '3'
		];

		$this->ReportModel->save($data);
		$date = date("Y-m-d", time());

		$data_com = [
			'id_report' => $id,
			'id_user' => $rprt->id_user,
			'comment' => $com,
			'entry_date' => $date,
			'delete' => 0
		];

		$this->commentModel->save($data_com);
		return redirect()->to(base_url('admin/report'))->with('success', 'Data report in revision.');
	}

	public function saveDataProgres()
	{
		/* Upload Images */
		$arrImage = array();
		$arrFile = array();

		if ($files = $this->request->getFiles()) {
			foreach ($files['upload_imgs'] as $file) {

				// DATABASE DATA FORMA
				$fileName = $file->getRandomName();
				$file->move(WRITEPATH . 'uploads/report/', $fileName); //upload to dir
				$photo_uri          = WRITEPATH . 'uploads/report/' . $fileName;
				$fileName   = $this->s3($fileName, $photo_uri, "report");

				array_push($arrImage, $fileName);
			}

			/* Upload File */
			foreach ($files['upload_file'] as $file_doc) {

				$fileDocName = $file_doc->getRandomName();
				$file_doc->move(WRITEPATH . 'uploads/report/', $fileDocName);
				$file_uri          = WRITEPATH . 'uploads/report/' . $fileDocName;
				$fileDocName   = $this->s3($fileDocName, $file_uri, "report");

				array_push($arrFile,  $fileDocName);
			}
		}

		$idReport = $this->request->getVar('idReport');
		$lap_date = $this->request->getVar('lap_date');
		$pgrsReport = $this->request->getVar('progress');
		$rencana = $this->request->getVar('rencana');
		$satPlan = $this->request->getVar('satPlan');
		$volReal = $this->request->getVar('volReal');
		$start_date = $this->request->getVar('start_date');
		$end_date = $this->request->getVar('end_date');
		$desc = $this->request->getVar('desc');
		$dataReport = $this->ReportModel->get_report($idReport);

		// check type report
		if ($dataReport->type == 'keuangan') {
			$month_date = $lap_date;
			$lap_date = null;
		}

		//update report
		$data = [
			'idoffline' => $idReport,
			'progress' => $pgrsReport,
			'status_date' => $lap_date,
			'vol_real' => $volReal,
			'vol_unit' => $satPlan,
			'vol_plan' => $rencana,
			'start_date' => $start_date,
			'end_date' => $end_date
		];

		$this->ReportModel->save($data);

		// save reply
		$dataReply = [
			'id_replydata' => $idReport,
			'title' => $dataReport->title,
			'fund_utilization' => $dataReport->fund_utilization,
			'description' => $dataReport->description,
			'entry_date' => $lap_date,
			'edit_date' => $lap_date,
			'start_date' => $start_date,
			'end_date' => $end_date,
			'is_publish' => 't',
			'delete' => '0',
			'id_user' => $this->session->id,
			'images' => json_encode($arrImage),
			'progress' => $pgrsReport,
			'document' => json_encode($arrFile),
			'description' => $desc,
			'progress_month' => $month_date
		];

		$this->ReportReplyModel->save($dataReply);

		return redirect()->to(base_url('admin/report'))->with('success', 'Data report successfully updated.');
	}

	public function revisi_report($id = '')
	{
		if (isset($this->session->id)) {
			$report = $this->ReportModel->get_report($id);
			$arrImgs = json_decode($report->images);

			$data = [
				'ksm' => $this->ksmModel->get_data($this->session->id_ksm),
				'pkt_job' => $this->ProjectModel->get_project(),
				'loc' => $this->ProjectLocModel->get_project_loc(),
				'kegiatan' => $this->ProjectLocModel->get_project_loc(),
				'report' => $report,
				'arrImgs' => $arrImgs,
				'project' => $this->SubProjectModel->getSubByProjectId($report->id_project)
			];

			return view('admin/report/revisi', $data);
		} else {
			return redirect()->to(base_url('/main/login'));
		}
	}

	public function saveDataRevisi()
	{

		$images_old = $this->request->getVar("reportImgArr");
		$files_old = $this->request->getVar("reportFileArr");

		/* Array Uploads */
		$arrImage = array();
		$arrFile = array();

		if ($files = $this->request->getFiles()) {

			/* Upload Images */
			foreach ($files['upload_imgs'] as $file) {

				if ($file != '') {
					$fileName = $file->getRandomName();
					$file->move(WRITEPATH . 'uploads/report/', $fileName); //upload to dir
					$photo_uri          = WRITEPATH . 'uploads/report/' . $fileName;
					$fileName   = $this->s3($fileName, $photo_uri, "report");

					array_push($arrImage, $fileName);
				} else {
					foreach (explode(",", $images_old) as $images_file) {
						array_push($arrImage, $images_file);
					}
				}
			}

			/* Upload Files */
			foreach ($files['upload_file'] as $file_doc) {

				if ($file_doc != '') {
					$fileDocName = $file_doc->getRandomName();
					$file_doc->move(WRITEPATH . 'uploads/report/', $fileDocName);
					$file_uri          = WRITEPATH . 'uploads/report/' . $fileDocName;
					$fileDocName   = $this->s3($fileDocName, $file_uri, "report");

					array_push($arrFile,  $fileDocName);
				} else {
					foreach (explode(",", $files_old) as $old_file) {
						array_push($arrFile,  $old_file);
					}
				}
			}
		}

		$repotid = $this->request->getVar("repotid");
		$jns_lap = $this->request->getVar("jns_lap");
		$jdl_lap = $this->request->getVar("jdl_lap");
		$pkt_lap = $this->request->getVar("pkt_lap");
		$kegiatanList = $this->request->getVar("kegiatanList");
		$lokasi = $this->request->getVar("lokasi");
		$str_date = $this->request->getVar("str_date");
		$end_date = $this->request->getVar("end_date");
		$hibah = $this->request->getVar("hibah");
		$progress = $this->request->getVar("progress");
		$lap_date = $this->request->getVar("lap_date");
		$rencana = $this->request->getVar("rencana");
		$satPlan = $this->request->getVar("satPlan");

		$volReal = $this->request->getVar("volReal");
		$terima = $this->request->getVar("terima");
		$desc = $this->request->getVar("desc");

		$dok_name_input = $this->request->getVar("dok_name_input");
		$dok_nameList = $this->request->getVar("dok_nameList");

		/* check dokumen name */
		if ($dok_name_input != '') {
			$docType = $dok_name_input;
		} else {
			$docType = $dok_nameList;
		}

		/* ammount account */
		$amm =  str_replace(",00",  "", $terima);
		$ammount =  str_replace(".",  "", $amm);

		/* check tgl */
		if ($this->request->getVar('jns_lap') == 'keuangan') {
			$lap_date = null;
			$month = $this->request->getVar('lap_date');
		} else {
			$lap_date = $this->request->getVar('lap_date');
			$month = null;
		}

		/* update report */
		$dataSave = [
			'idoffline' => $repotid,
			'type' => $jns_lap,
			'title' => $jdl_lap,
			'id_project' => $pkt_lap,
			'hibah' =>  $hibah,
			'id_sub_project' => $kegiatanList,
			'id_project_loc' => $lokasi,
			'progress' => $progress,
			'fund_utilization' => $ammount,
			'vol_plan' => $rencana,
			'vol_real' => $volReal,
			'description' => $desc,
			'start_date' => $str_date,
			'end_date' => $end_date,
			'doc_type' => $docType,
			'status_date' => $lap_date,
			'is_publish' => 'f',
			'is_approval' => '0',
			'delete' => '0',
			'vol_unit' => $satPlan,
			'id_user' =>  $this->session->get('id'),
			'images' => json_encode($arrImage),
			'document' => json_encode($arrFile),
			'progress_month' => $month
		];

		$this->ReportModel->save($dataSave);
		return redirect()->to(base_url('admin/report'))->with('success', 'Data report successfully saved, waiting approval !');
	}

	public function test()
	{
		return view('test-slider');
	}
}
