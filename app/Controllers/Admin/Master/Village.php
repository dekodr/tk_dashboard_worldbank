<?php

namespace App\Controllers\Admin\Master;

use App\Controllers\BaseController;
use App\Models\VillageModel;
use App\Models\DistrictModel;
use App\Models\SubdistrictModel;
use App\Models\ProvinceModel;
use App\Models\SubProjectModel;

class Village extends BaseController
{
    protected $session = null;
    public $VillageModel;
    public $generateTable;
    public $DistrictModel;
    public $SubdistrictModel;
    public $SubProjectModel;

    public function __construct()
    {
        $this->session = session();
        $this->ProvinceModel    = new ProvinceModel();
        $this->DistrictModel    = new DistrictModel();
        $this->SubdistrictModel = new SubdistrictModel();
        $this->VillageModel     = new VillageModel();
        $this->SubProjectModel     = new SubProjectModel();
        $this->generateTable    = new \CodeIgniter\View\Table();
    }

    public function index()
    {
        if (isset($this->session->id)) {
            $data   = $this->VillageModel->get_data();

            foreach ($data as $key => $value)
                $data['table'][$key] = array($value->code, $value->name, '
                <div class="d-sm-flex align-items-center justify-content-between mb-1">
                <a href="' . base_url('/admin/master/village/edit/' . $value->id) . '" class="d-none d-sm-inline-block btn btn-sm btn-info shadow-sm"><i class="fas fa-pen text-white-50"></i> Edit </a>
                <a href="' . base_url('/admin/master/village/delete/' . $value->id) . '" class="d-none d-sm-inline-block btn btn-sm btn-danger shadow-sm" onclick="return confirm(\'Apakah akan menghapus data ?\');"><i class="fas fa-trash text-white-50"></i> Delete</a>
            </div>');

            $data['table']  = $this->generateTable->generate($data['table']);

            //returning table body
            return view('admin/master/village/list', $data);

        }else{
            return redirect()->to(base_url('/main/login'));
        }
    }

    public function create()
    {
        if (isset($this->session->id)) {
            $kab = $this->DistrictModel->get_data($kec->id_kabupaten);
            $prov = $this->ProvinceModel->get_data($kab->id_province);
            $data = [
                // 'kablist'   => $this->DistrictModel->get_data(),
                'province'  => $this->ProvinceModel->getProvince(),
                'district'  => $this->DistrictModel->getKabByProvinceId($prov->id),
                'sub'       => $this->SubdistrictModel->getSubByDistrictId($kab->id)
            ];

            return view('admin/master/village/create', $data);

        }else{
            return redirect()->to(base_url('/main/login'));
        }
    }

    // function getSubByDistrict()
    // {
    //     $dist_id = $this->request->getVar('idkab');
    //     $data = $this->SubdistrictModel->getSubByDistrictId($dist_id);
    //     echo json_encode($data);
    // }

    public function save()
    {
        $getKel = $this->request->getVar('getKel');

        $data = [
            'id' => $getKel,
            'id_kabupaten' => $this->request->getVar('idkab'),
            'name' => $this->request->getVar('name'),
            'code' => $this->request->getVar('code'),
            'id_kecamatan' => $this->request->getVar('idkec'),
            'delete' => '0'
        ];

        //save to db
        $save = $this->VillageModel->insert_($data);

        if ($save) {
            return redirect()->to(base_url('admin/master/village'))->with('success', 'Data kabupaten successfully saved.');
        } else {
            return redirect()->back()->withInput()->with('error', 'We had trouble saving your data.');
        }
    }

    public function delete($id)
    {
        $data = [
            'id' => $id,
            'delete' => '1'
        ];

        $this->VillageModel->save($data);


        return redirect()->to(base_url('admin/master/district'))->with('success', 'Data kabupaten successfully deleted.');
    }

    public function edit($id = '')
    {
        if (isset($this->session->id)) {

            $data = [
                'isEdit'    => 'edit',
                'getKel'    => $this->VillageModel->get_data($id),
                'province'  => $this->ProvinceModel->get_data(),
                'district'  => $this->DistrictModel->get_data(),
                'sub'       => $this->SubdistrictModel->get_data()
            ];
            // print_r($this->VillageModel->get_data($id));die;
            return view('admin/master/village/create', $data);

        }else{
            return redirect()->to(base_url('/main/login'));
        }
    }

    function getSubProjectById()
    {
        $id = $this->request->getVar('pkt_lap');
        $data = $this->SubProjectModel->getSubByProjectId($id);
        echo json_encode($data);
    }

    function getKabByProv()
    {
        $prov_id = $this->request->getVar('id_provinsi');
        $data = $this->DistrictModel->getKabByProvinceId($prov_id);
        echo json_encode($data);
    }

    function getSubByDistrict()
    {
        $dist_id = $this->request->getVar('id_kabupaten');
        $data = $this->SubdistrictModel->getSubByDistrictId($dist_id);
        echo json_encode($data);
    }

    function getVilBySub()
    {
        $sub_id = $this->request->getVar('id_kecamatan');
        $data = $this->VillageModel->getVilBySubId($sub_id);
        echo json_encode($data);
    }
}
