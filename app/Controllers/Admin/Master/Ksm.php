<?php

namespace App\Controllers\Admin\Master;

use App\Controllers\BaseController;
use Aws\S3\S3Client;
use Aws\Exception\AwsException;
use CodeIgniter\HTTP\Request;
use App\Models\ProvinceModel;
use App\Models\DistrictModel;
use App\Models\SubdistrictModel;
use App\Models\VillageModel;
use App\Models\KsmModel;

class Ksm extends BaseController
{
    protected $session = null;
    public $model;
    public $client;
    public $request = null;
    public $generateTable;
    public $villageModel;

    public function __construct()
    {
        $this->generateTable    = new \CodeIgniter\View\Table();
        $this->request          = \Config\Services::request();
        $this->client           = \Config\Services::curlrequest();
        $this->model            = new KsmModel();
        $this->villageModel     = new VillageModel();
        $this->session          = session();
        $this->ProvinceModel    = new ProvinceModel();
        $this->DistrictModel    = new DistrictModel();
        $this->SubdistrictModel = new SubdistrictModel();
        $this->VillageModel     = new VillageModel();
    }

    public function create()
    {
        if (isset($this->session->id)) {
            $kec = $this->SubdistrictModel->get_data($kel->id_kecamatan);
            $kab = $this->DistrictModel->get_data($kec->id_kabupaten);
            $prov = $this->ProvinceModel->get_data($kab->id_province);
            $data = [
                'kelurahan' => $this->villageModel->get_data(),
                'province'  => $this->ProvinceModel->get_data(),
                'district'  => $this->DistrictModel->getKabByProvinceId($prov->id),
                'sub'       => $this->SubdistrictModel->getSubByDistrictId($kab->id),
                'village'   => $this->VillageModel->getVilBySubId($kec->id),
            ];

            return view('admin/master/ksm/create', $data);

        }else{
            return redirect()->to(base_url('/main/login'));
        }
    }

    public function delete($id)
    {
        $delete = $this->model->delete($id);
        return redirect()->to(base_url('admin/master/ksm'))->with('success', 'Data KSM successfully deleted.');
    }

    public function edit($id = '')
    {
        if (isset($this->session->id)) {
            $kec = $this->SubdistrictModel->get_data($kel->id_kecamatan);
            $kab = $this->DistrictModel->get_data($kec->id_kabupaten);
            $prov = $this->ProvinceModel->get_data($kab->id_province);
            $data = [
                'isEdit' => 'edit',
                'getKsm' => $this->model->get_data($id),
                'kelurahan' => $this->villageModel->get_data(),
                'province'  => $this->ProvinceModel->get_data(),
                'district'  => $this->DistrictModel->get_data(),
                'sub'       => $this->SubdistrictModel->get_data(),
                'village'   => $this->VillageModel->getVilBySubId($kec->id)
            ];
            // print_r($this->model->get_data($id));die;
            return view('admin/master/ksm/ksm_edit', $data);

        }else{
            return redirect()->to(base_url('/main/login'));
        }
    }

    public function index()
    {
        if (isset($this->session->id)) {
            $data = $this->model->get_data();

            foreach ($data as $key => $value)
                $data['table'][$key] = array($value->name, $value->kelurahan, $value->kode_ksm, '
                <div class="d-sm-flex align-items-center justify-content-between mb-1">
                <a href="' . base_url('/admin/master/ksm/edit/' . $value->id) . '" class="d-none d-sm-inline-block btn btn-sm btn-info shadow-sm"><i class="fas fa-pen text-white-50"></i> Edit </a>
                <a href="' . base_url('/admin/master/ksm/delete/' . $value->id) . '" class="d-none d-sm-inline-block btn btn-sm btn-danger shadow-sm" onclick="return confirm(\'Apakah akan menghapus data ?\');"><i class="fas fa-trash text-white-50"></i> Delete</a>
                ');

            $data['table']  = $this->generateTable->generate($data['table']);

            //returning table body
            return view('admin/master/ksm/list', $data);

        }else{
            return redirect()->to(base_url('/main/login'));
        }
    }

    public function save()
    {   
        $name           = $this->request->getVar('name'); 
        $kode_ksm       = $this->request->getVar('kode_ksm');    
        $id_kelurahan   = $this->request->getVar('id_kelurahan');     
        $tanggal        = date('Y-m-d H:i:s');
        // $delete         = 0;
        $data = [
            'name'          => $name,
            'kode_ksm'      => $kode_ksm,
            'entry_date'    => $tanggal,
            'id_kelurahan'  => $id_kelurahan,
            'delete'        => 0,
        ];

        $save = $this->model->save($data);
        if ($save) {
            return redirect()->to(base_url('admin/master/ksm'))->with('success', 'Data KSM successfully saved.');
        } else {
            return redirect()->back()->withInput()->with('error', 'We had trouble saving your data.');
        }
    }

    public function save_update()
    {
        $id             = $this->request->getVar('id');
        $name           = $this->request->getVar('name'); 
        $kode_ksm       = $this->request->getVar('kode_ksm');    
        $id_kelurahan   = $this->request->getVar('id_kelurahan');      
        $tanggal        = date('Y-m-d H:i:s');
        // $delete         = 0;

        $data = [
            'id'            => $id,
            'name'          => $name,
            'kode_ksm'      => $kode_ksm,
            'edit_date'     => $tanggal,
            'id_kelurahan'  => $id_kelurahan,
            'delete'        => 0,
        ];

        $save = $this->model->save($data);
        if ($save) {
            return redirect()->to(base_url('admin/master/ksm'))->with('success', 'Data KSM successfully saved.');
        } else {
            return redirect()->back()->withInput()->with('error', 'We had trouble saving your data.');
        }
    }

    function getKabByProv()
    {
        $prov_id = $this->request->getVar('id_provinsi');
        $data = $this->DistrictModel->getKabByProvinceId($prov_id);
        echo json_encode($data);
    }

    function getSubByDistrict()
    {
        $dist_id = $this->request->getVar('id_kabupaten');
        $data = $this->SubdistrictModel->getSubByDistrictId($dist_id);
        echo json_encode($data);
    }

    function getVilBySub()
    {
        $sub_id = $this->request->getVar('id_kecamatan');
        $data = $this->VillageModel->getVilBySubId($sub_id);
        echo json_encode($data);
    }
}
