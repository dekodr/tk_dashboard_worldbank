<?php

namespace App\Controllers\Admin\Master;

use App\Controllers\BaseController;
use Aws\S3\S3Client;
use Aws\Exception\AwsException;
use CodeIgniter\HTTP\Request;
use App\Models\FacilitatorModel;
use App\Models\KsmModel;
use App\Models\ProvinceModel;
use App\Models\DistrictModel;

class Facilitator extends BaseController
{
    protected $session = null;
    public $model;
    public $client;
    public $request = null;
    public $generateTable;
    public $villageModel;

    public function __construct()
    {
        $this->generateTable    = new \CodeIgniter\View\Table();
        $this->request = \Config\Services::request();
        $this->client = \Config\Services::curlrequest();
        $this->ksmModel = new KsmModel();
        $this->model = new FacilitatorModel();
        $this->provincemodel = new ProvinceModel();
        $this->districtmodel = new DistrictModel();
        $this->session = session();
    }

    public function create()
    {
        if (isset($this->session->id)) {
            $kab = $this->districtmodel->get_data($kec->id_kabupaten);
            $prov = $this->provincemodel->get_data($kab->id_province);
            $data = [
                'district'  => $this->districtmodel->getKabByProvinceId($prov->id),
                'ksm' => $this->ksmModel->getKSMByKabupatenId($kab->id),
                'prov' => $this->provincemodel->getProvince()
            ];
            
            return view('admin/master/facilitator/create', $data);

        }else{
            return redirect()->to(base_url('/main/login'));
        }
    }

    public function delete($id)
    {
        $data = [
            'id' => $id,
            'del' => 1
        ];
        $delete = $this->model->save($data);
        return redirect()->to(base_url('admin/master/facilitator'))->with('success', 'Data Fasilitator successfully deleted.');
    }

    public function edit($id = '')
    {
        if (isset($this->session->id)) {
            $kab = $this->districtmodel->get_data($kec->id_kabupaten);
            $prov = $this->provincemodel->get_data($kab->id_province);
            $data = [
                'isEdit' => 'edit',
                'district'  => $this->districtmodel->get_data(),
                'ksm' => $this->ksmModel->getKSMByKabupatenId($kab->id),
                'prov' => $this->provincemodel->getProvince(),
                'getFasil' => $this->model->get_data($id)
            ];
            // print_r($this->model->get_data($id));die;
            return view('admin/master/facilitator/fasilitator_edit', $data);

        }else{
            return redirect()->to(base_url('/main/login'));
        }
    }

    public function index()
    {
        if (isset($this->session->id)) {
            $data = $this->model->get_data();
            // print_r($data);die;
            foreach ($data as $key => $value) {
                $_a = explode(',', $value->id_ksm);

                if (!empty($_a)) {
                    $_l = '<ul>';
                    foreach ($_a as $k => $v) {
                        $_d = $this->ksmModel->get_data($v);
                        $_l .= '<li>' . $_d->name . '</li>';
                    }
                    $_l .= '</ul>';
                } else {
                    $_l = '-';
                }

                $data['table'][$key] = array(
                    $value->code, $value->name, $_l,
                    '<div class="d-sm-flex align-items-center justify-content-between mb-1">
                <a href="' . base_url('/admin/master/facilitator/edit/' . $value->id) . '" class="d-none d-sm-inline-block btn btn-sm btn-info shadow-sm"><i class="fas fa-pen text-white-50"></i> Edit </a>
                <a href="' . base_url('/admin/master/facilitator/delete/' . $value->id) . '" class="d-none d-sm-inline-block btn btn-sm btn-danger shadow-sm" onclick="return confirm(\'Apakah akan menghapus data ?\');"><i class="fas fa-trash text-white-50"></i> Delete</a>
                '
                );
            }

            $data['table']  = $this->generateTable->generate($data['table']);

            //returning table body
            return view('admin/master/facilitator/list', $data);

        }else{
            return redirect()->to(base_url('/main/login'));
        }
    }

    public function save()
    {
        $data = $this->request->getVar();
        $data['id_ksm'] = implode(',', $data['id_ksm']);
        $data['entry_stamp'] = date('Y-m-d H:i:s');
        $data['del'] = 0;
        $save = $this->model->save($data);
        if ($save) {
            return redirect()->to(base_url('admin/master/facilitator'))->with('success', 'Data Fasilitator successfully saved.');
        } else {
            return redirect()->back()->withInput()->with('error', 'We had trouble saving your data.');
        }
    }

    public function save_update()
    {
        $data = $this->request->getVar();

        $data['id'] = $data['id'];
        $data['id_ksm'] = implode(',', $data['id_ksm']);
        $data['edit_stamp'] = date('Y-m-d H:i:s');

        $save = $this->model->save($data);
        if ($save) {
            return redirect()->to(base_url('admin/master/facilitator'))->with('success', 'Data Fasilitator successfully saved.');
        } else {
            return redirect()->back()->withInput()->with('error', 'We had trouble saving your data.');
        }
    }

    function getKabByProv()
    {
        $prov_id = $this->request->getVar('id_provinsi');
        $data = $this->districtmodel->getKabByProvinceId($prov_id);
        echo json_encode($data);
    }

    function getKSMByKab()
    {
        $kab_id = $this->request->getVar('id_kabupaten');
        $data = $this->ksmModel->getKSMByKabupatenId($kab_id);
        echo json_encode($data);
    }
}
