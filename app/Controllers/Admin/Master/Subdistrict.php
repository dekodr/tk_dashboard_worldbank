<?php

namespace App\Controllers\Admin\Master;

use App\Controllers\BaseController;
use App\Models\ProvinceModel;
use App\Models\SubdistrictModel;
use App\Models\DistrictModel;

// use PHPExcel;
// use PHPExcel_IOFactory;

class Subdistrict extends BaseController
{
    protected $session = null;
    public $DistrictModel;
    public $SubdistrictModel;
    public $generateTable;

    public function __construct()
    {

        $this->session = session();
        $this->SubdistrictModel     = new SubdistrictModel();
        $this->generateTable        = new \CodeIgniter\View\Table();
        $this->DistrictModel        = new DistrictModel();
        $this->ProvinceModel        = new ProvinceModel();
    }

    public function index()
    {
        if (isset($this->session->id)) {
            $data   = $this->SubdistrictModel->get_data();

            foreach ($data as $key => $value)
                $data['table'][$key] = array($value->code, $value->name, '
                    <div class="d-sm-flex align-items-center justify-content-between mb-1">
                    <a href=" ' . base_url('/admin/master/subdistrict/edit/' . $value->id) . '" class="d-none d-sm-inline-block btn btn-sm btn-info shadow-sm"><i class="fas fa-pen text-white-50"></i> Edit </a>
                    <a href=" ' . base_url('/admin/master/subdistrict/delete/' . $value->id) . '" class="d-none d-sm-inline-block btn btn-sm btn-danger shadow-sm" onclick="return confirm(\'Apakah akan menghapus data ?\');"><i class="fas fa-trash text-white-50"></i> Delete</a>
                    </div>');

            $data['table']  = $this->generateTable->generate($data['table']);

            //returning table body
            return view('admin/master/sub-district/list', $data);

        }else{
            return redirect()->to(base_url('/main/login'));
        }
    }


    public function create()
    {
        if (isset($this->session->id)) {
            $prov = $this->ProvinceModel->get_data($kab->id_province);
            $data = [
                'kablist' => $this->DistrictModel->get_data(),
                'province'  => $this->ProvinceModel->get_data(),
                'district'  => $this->DistrictModel->getKabByProvinceId($prov->id)
            ];

            return view('admin/master/sub-district/create',  $data);

        }else{
            return redirect()->to(base_url('/main/login'));
        }
    }

    public function save()
    {
        $idKcm = $this->request->getVar('idKcm');

        $data = [
            'id' => $idKcm,
            'code' => $this->request->getVar('code'),
            'name' => $this->request->getVar('name'),
            'id_kabupaten' => $this->request->getVar('idkab'),
            'delete' => '0'
        ];

        //save to db
        $save  = $this->SubdistrictModel->save($data);

        if ($save) {
            return redirect()->to(base_url('admin/master/subdistrict'))->with('success', 'Data kabupaten successfully saved.');
        } else {
            return redirect()->back()->withInput()->with('error', 'We had trouble saving your data.');
        }
    }

    public function delete($id)
    {

        $data = [
            'id' => $id,
            'delete' => '1'
        ];

        //save to db
        $save  = $this->SubdistrictModel->save($data);
        return redirect()->to(base_url('admin/master/subdistrict'))->with('success', 'Data kabupaten successfully deleted.');
    }

    public function edit($id = '')
    {
        if (isset($this->session->id)) {
            $prov = $this->ProvinceModel->get_data($kab->id_province);
            $data = [
                'isEdit' => 'edit',
                'getKcm' => $this->SubdistrictModel->get_data($id),
                'kablist' => $this->DistrictModel->get_data(),
                'province'  => $this->ProvinceModel->get_data(),
                'district'  => $this->DistrictModel->get_data()
            ];
            // print_r($this->SubdistrictModel->get_data($id));die;
            return view('admin/master/sub-district/create', $data);

        }else{
            return redirect()->to(base_url('/main/login'));
        }
    }

    function getKabByProv()
    {
        $prov_id = $this->request->getVar('id_provinsi');
        $data = $this->DistrictModel->getKabByProvinceId($prov_id);
        echo json_encode($data);
    }

    // public function excelRead($file)
    // {
    //     echo "masuk gan";
    //     print_r($file);
    //     if ($file) {
    //         echo "masuk gan";
    //         $excelReader  = new PHPExcel();
    //         //mengambil lokasi temp file
    //         $fileLocation = $file->getTempName();
    //         //baca file
    //         $objPHPExcel = PHPExcel_IOFactory::load($file);
    //         //ambil sheet active
    //         $sheet    = $objPHPExcel->setLoadSheetsOnly('Data KSM')->toArray(null, true, true, true);
    //         //looping untuk mengambil data
    //         foreach ($sheet as $idx => $data) {
    //             //skip index 1 karena title excel
    //             if ($idx == 1) {
    //                 continue;
    //             }
    //             $code = $data['F'];
    //             $name = $data['G'];

    //             print_r($code, $name);
    //             echo "_____________________________<br>";
    //         }
    //     }

    //     die;
    // }
    //--------------------------------------------------------------------

}
