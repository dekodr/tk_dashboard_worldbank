<?php

namespace App\Controllers\Admin\Master;

use App\Controllers\BaseController;
use App\Models\KsmModel;
use App\Models\PasswordModel;
use App\Models\UserModel;
use App\Models\RoleModel;
use App\Models\ProvinceModel;
use App\Models\DistrictModel;
use App\Models\SubdistrictModel;
use App\Models\FacilitatorModel;
use App\Models\VillageModel;
use App\Models\KorkotModel;
use App\Models\UsernameModel;
use CodeIgniter\HTTP\Files\UploadedFile;

// use Aws\S3\S3Client;
// use Aws\Exception\AwsException;
// use Aws\S3\ObjectUploader;

use Aws\S3\S3Client;
use Aws\Exception\AwsException;
use CodeIgniter\HTTP\Request;
use PHPExcel;
use PHPExcel_IOFactory;

// ini_set('memory_limit', '1024M');
// ini_set('upload_max_filesize', '200000M');
class User extends BaseController
{
    protected $session = null;
    public $UserModel;
    public $generateTable;
    public $KsmModel;
    public $client;
    public $request = null;
    public $usernameModel;
    public $passwordModel;

    public function __construct()
    {
        $this->UserModel        = new UserModel();
        $this->generateTable    = new \CodeIgniter\View\Table();
        $this->KsmModel         = new KsmModel();
        $this->request          = \Config\Services::request();
        $this->client           = \Config\Services::curlrequest();
        $this->session          = session();
        $this->usernameModel    = new UsernameModel();
        $this->passwordModel    = new PasswordModel();
        $this->RoleModel        = new RoleModel();
        $this->ProvinceModel    = new ProvinceModel();
        $this->DistrictModel    = new DistrictModel();
        $this->SubdistrictModel = new SubdistrictModel();
        $this->VillageModel     = new VillageModel();
        $this->FacilitatorModel = new FacilitatorModel();
        $this->KorkotModel      = new KorkotModel();
    }

    public function index()
    {
        if (isset($this->session->id)) {
            $data   = $this->UserModel->get_user();
            // print_r($data);die;
            foreach ($data as $key => $value)
                $data['table'][$key] = array($value->name, $value->position, $this->KsmModel->get_data($value->id_ksm)->name, $value->email, '
                <div class="d-sm-flex align-items-center justify-content-between mb-1">
                <a href="' . base_url('/admin/master/user/edit/' . $value->id) . '" class="d-none d-sm-inline-block btn btn-sm btn-info shadow-sm"><i class="fas fa-pen text-white-50"></i> Edit </a>
                <a href="' . base_url('/admin/master/user/delete/' . $value->id) . '" class="d-none d-sm-inline-block btn btn-sm btn-danger shadow-sm" onclick="return confirm(\'Apakah akan menghapus data ?\');"><i class="fas fa-trash text-white-50"></i> Delete</a>
                ');
            // print_r($this->KsmModel->get_data($value->id_ksm)->name);die;
            $data['table']  = $this->generateTable->generate($data['table']);
            //returning table body
            return view('admin/master/user/list', $data);            
        } else {
            return redirect()->to(base_url('/main/login'));            
        }
    }

    public function save()
    {
        $user = $this->request->getVar('username');
        $isActive = $this->usernameModel->get_usernameByUser($user);

        if (isset($isActive)) {
            return redirect()->to(base_url('admin/master/user'))->with('success', 'Data user sudah pernah digunakan.');
        }

        $id_role        = $this->request->getVar('id_role');
        $pass           = $this->request->getVar('password');
        $name           = $this->request->getVar('name');
        $position1      = $this->request->getVar('position1');
        $position2      = $this->request->getVar('position2');
        $phone          = $this->request->getVar('phone');
        $email          = $this->request->getVar('email');
        $id_boss        = $this->request->getVar('idboss');
        $id_boss2       = $this->request->getVar('idboss2');
        $korkot         = $this->request->getVar('korkot');
        $senior         = $this->request->getVar('senior_fasil');
        $id_provinsi    = $this->request->getVar('id_provinsi');
        $id_kabupaten   = $this->request->getVar('id_kabupaten');
        $id_kecamatan   = $this->request->getVar('id_kecamatan');
        $id_kelurahan   = $this->request->getVar('id_kelurahan');
        $idKsm          = $this->request->getVar('idksm');
        $id_fasilitator = $this->request->getVar('id_fasilitator');
        $id_korkot      = $this->request->getVar('id_korkot');

        // print_r( $this->request->getVar());die;
        if ($id_role == 3 || $id_role == 4) 
        {
            $idKsm = null;
            $id_korkot = null;
            $id_boss2 = 0;
            $position = $position2;
            $id_boss = $senior;
            $id_fasilitator = $id_fasilitator;
        } else if ($id_role == 5 || $id_role == 9 || $id_role == 6) 
        {
            $id_boss = null;
            $id_boss2 = null;
            $idKsm = null;
            $id_fasilitator = null;
            $position = $position1;
            $id_korkot = $id_korkot;
        } else if ($id_role == 1) 
        {
            $id_boss = null;
            $id_boss2 = null;
            $idKsm = null;
            $id_korkot = null;
            $id_fasilitator = null;
            $position = $position1;
        } else {
            $id_korkot = null;
            $id_fasilitator = null;
            $position = $position1;
        }

        $dataLogin = $this->client->request(
            'post',
            'http://103.93.57.36:5000/user/add',
            ['json' => [
                "id_ksm"            => intval($idKsm),
                "name"              => $name,
                "email"             => $email,
                "phone"             => $phone,
                "username"          => $user,
                "password"          => $pass,
                "is_active"         => 1,
                "delete"            => 0,
                "id_boss"           => intval($id_boss),
                "id_boss2"          => intval($id_boss2),
                "position"          => $position,
                "id_role"           => intval($id_role),
                "id_city"           => intval($id_kabupaten),
                "id_province"       => intval($id_provinsi),
                "id_kabupaten"      => intval($id_kabupaten),
                "id_kelurahan"      => intval($id_kelurahan),
                "id_korkot"         => intval($id_korkot),
                "id_fasilitator"    => intval($id_fasilitator)
            ], 'http_errors' => false]
        );
        // print_r($dataLogin);die;
        $status = $dataLogin->getStatusCode();

        // $data_ = [
        //     'id' => $id_fasilitator,
        //     'is_account' => 1
        // ];
        // $this->FacilitatorModel->update_status($data_);

        if ($status == 200) {
            return redirect()->to(base_url('admin/master/user'))->with('success', 'Data user successfully saved.');
        } else {
            return redirect()->to(base_url('admin/master/user'))->with('success', 'Data user Failed saved.');
        }
    }

    public function delete($id = '')
    {
        $data = [
            'id' => $id,
            'delete' => '1'
        ];

        $this->UserModel->save($data);

        return redirect()->to(base_url('admin/master/user'))->with('success', 'Data user successfully deleted.');
    }

    public function edit($id = '')
    {
        if (isset($this->session->id)) {

            $ksm = $this->KsmModel->get_data($usr->id_ksm);
            $kel = $this->VillageModel->get_data($ksm->id_kelurahan);
            $kec = $this->SubdistrictModel->get_data($kel->id_kecamatan);
            $kab = $this->DistrictModel->get_data($kec->id_kabupaten);
            $prov = $this->ProvinceModel->get_data($kab->id_province);
            $data = [
                'korkot'        => $this->UserModel->getkorkot(),
                'boss'          => $this->UserModel->get_user(),
                'province'      => $this->ProvinceModel->get_data(),
                'district'      => $this->DistrictModel->getKabByProvinceId($prov->id),
                'sub'           => $this->SubdistrictModel->getSubByDistrictId($kab->id),
                'village'       => $this->VillageModel->getVilBySubId($kec->id),
                'ksm'           => $this->KsmModel->getKSMByVilId($kel->id),
                'ksm1'          => $this->FacilitatorModel->getKSMByDistrictId($kab->id),
                'koordinator'   => $this->KorkotModel->getKorkotByProvId($prov->id),
                'isEdit'        => 'edit',
                'getUser'       => $this->UserModel->get_user($id)
            ];
            // print_r($this->UserModel->get_user($id));die;
            return view('admin/master/user/user_edit', $data);
        }else{
            return redirect()->to(base_url('/main/login')); 
        }
    }

    public function save_update()
    {
        $id_user        = $this->request->getVar("iduser");
        $data_user      = $this->UserModel->get_user($id_user);
        $name           = $this->request->getVar("name");
        $position1      = $this->request->getVar("position1");
        $position2      = $this->request->getVar("position2");
        $phone          = $this->request->getVar("phone");
        $email          = $this->request->getVar("email");
        $id_boss        = $this->request->getVar("idboss");
        $id_boss2       = $this->request->getVar("idboss2");
        $senior         = $this->request->getVar("senior_fasil");
        $korkot         = $this->request->getVar("korkot");
        $id_provinsi    = $this->request->getVar('id_provinsi');
        $id_kabupaten   = $this->request->getVar('id_kabupaten');
        $id_kecamatan   = $this->request->getVar('id_kecamatan');
        $id_kelurahan   = $this->request->getVar('id_kelurahan');
        $id_ksm         = $this->request->getVar("idksm");
        $id_fasilitator = $this->request->getVar('id_fasilitator');
        $id_korkot      = $this->request->getVar('id_korkot');
        // print_r($this->request->getVar());die;
        if (strtolower($data_user->id_role) == 3 || strtolower($data_user->id_role) == 4) {
            $data = [
                'id' => $id_user,
                'name' => $name,
                'position' => $position2,
                'phone' => $phone,
                'email' =>  $email,
                'id_boss' => $senior,
                'id_ksm' => $id_ksm,
                'id_province' => $id_provinsi,
                'id_kabupaten' => $id_kabupaten,
            ];
        } else if (strtolower($data_user->id_role) == 5 || strtolower($data_user->id_role) == 9 || strtolower($data_user->id_role) == 9) {

            $data = [
                'id' => $id_user,
                'name' => $name,
                'position' => $position2,
                'phone' => $phone,
                'email' =>  $email,
                'id_boss' => $korkot,
                'id_province' => $id_provinsi,
            ];
        } else if ($data_user->id_boss == 1) {
            $data = [
                'id' => $id_user,
                'name' => $name,
                'position' => $position2,
                'phone' => $phone,
                'email' =>  $email,
            ];
        } else {
            $data = [
                'id' => $id_user,
                'name' => $name,
                'position' => $position2,
                'phone' => $phone,
                'email' =>  $email,
                'id_ksm' => $id_ksm,
                'id_boss' => $id_boss,
                'id_boss2' => $id_boss2,
                'id_province' => $id_provinsi,
                'id_kabupaten' => $id_kabupaten
            ];
        }

        $this->UserModel->save($data);
        // print_r($data_user);die;
        return redirect()->to(base_url('admin/master/user'))->with('success', 'Data user successfully saved.');
    }

    public function change($id = '')
    {

        if (isset($this->session->id)) {
            $user_data = $this->UserModel->get_user($id);
            $usernm = $this->usernameModel->get_usernameByUserId($id);

            $data = [
                'getUser' => $user_data,
                'usernm' => $usernm
            ];

            return view('admin/master/user/user_change', $data);
        }else{
            return redirect()->to(base_url('/main/login')); 
        }
    }

    public function change_user()
    {
        $userid = $this->request->getVar("iduser");
        $newPass = $this->request->getVar("passbaru");

        $dataLogin = $this->client->request(
            'put',
            'http://103.93.57.36:5000/user/changepassword',
            ['json' => [
                "password" => $newPass,
                "id_user" => $userid
            ], 'http_errors' => false]
        );

        $status = $dataLogin->getStatusCode();
        // print_r($dataLogin->getStatusCode());die;

        if ($status == 200) {
            return redirect()->to(base_url('admin'))->with('success', 'Data login user successfully change.');
        } else {
            return redirect()->to(base_url('admin'))->with('success', 'Data login user failed change.');
        }
    }

    public function create()
    {
        if (isset($this->session->id)) {
            $file               = $this->request->getFiles('fileexcel');

            if ($file) {

                $excelReader  = new PHPExcel();

                $fileLocation = $file['fileexcel']->getTempName();
                $objPHPExcel = PHPExcel_IOFactory::load($fileLocation);

                $objPHPExcel->setActiveSheetIndex(1);
                $sheet    = $objPHPExcel->getActiveSheet()->toArray(null, true, true, true);

                $objPHPExcel->setActiveSheetIndex(4);
                $sheet_    = $objPHPExcel->getActiveSheet()->toArray(null, true, true, true);
                $project = array();



                foreach ($sheet_ as $idx => $data) {
                    if ($idx > 2) {
                        $project['project'][$data['Q']]['code'] = $data['Q'];
                        $project['project'][$data['Q']]['name'] = $data['R'];
                        $project['project'][$data['Q']]['sub_project'][$data['S']]['code'] = $data['S'];
                        $project['project'][$data['Q']]['sub_project'][$data['S']]['name'] = $data['T'];
                        $project['lokasi'][$data['BI']]['name'] = $data['BI'];
                    }
                }

                $this->UserModel->create_batch($project['project']);

                die;
            }
            // die;

            // if ($_POST['name']) {
            //     # code...

            //     $file               = $this->request->getFiles('photo_id');

            //     // DATABASE DATA FORMAT
            //     $data               = $_POST;
            //     $data['photo_id']   = $file['photo_id']->getRandomName();
            //     $file['photo_id']->move(WRITEPATH.'uploads/register', $data['photo_id']); //upload to dir
            //     $photo_uri          = WRITEPATH.'uploads/register/'.$data['photo_id'];
            //     $data['photo_id']   = $this->s3($data['photo_id'], $photo_uri, "register");

            //     // RKB DATA FORMAT
            //     $rkbDATA = array();
            //     $rkbDATA['name']            = $data['name'];
            //     $rkbDATA['company_id']      = 3;
            //     $rkbDATA['phone_no']        = uniqid();
            //     $rkbDATA['email']           = $data['email'];
            //     $rkbDATA['nik']             = $data['number_id'];
            //     $rkbDATA['photo']           = $data['photo_id'];

            //     print_r(json_encode($rkbDATA));
            //     // registering face id
            //     $context = stream_context_create(array(
            //         'http' => array(
            //             'method' => 'POST',
            //             'header' => "Content-Type: application/json\r\n",
            //             'content' => json_encode($rkbDATA)
            //         )
            //     ));
            //     $response = file_get_contents('http://103.93.56.248:8000/absent/registration', FALSE, $context);

            //     if($response === FALSE)
            //         die('Error');

            //     $responseData = json_decode($response, TRUE);

            //     //save to db
            //     $save               = $this->UserModel->insert_user($data);

            //     if ($save) {
            //         return redirect()->to(base_url('admin/master/user'))->with('success', 'Data successfully saved.');
            //     } else {
            //         return redirect()->back()->withInput()->with('error', 'We had trouble saving your data.');
            //     }
            // }

            // $data                   = null;
            $ksm = $this->KsmModel->get_data($usr->id_ksm);
            $kel = $this->VillageModel->get_data($ksm->id_kelurahan);
            $kec = $this->SubdistrictModel->get_data($kel->id_kecamatan);
            $kab = $this->DistrictModel->get_data($kec->id_kabupaten);
            $prov = $this->ProvinceModel->get_data($kab->id_province);
            $data = [
                'korkot'        => $this->UserModel->getkorkot(),
                'boss'          => $this->UserModel->get_user(),
                'role'          => $this->RoleModel->getRole(),
                'province'      => $this->ProvinceModel->getProvince(),
                'district'      => $this->DistrictModel->getKabByProvinceId($prov->id),
                'sub'           => $this->SubdistrictModel->getSubByDistrictId($kab->id),
                'village'       => $this->VillageModel->getVilBySubId($kec->id),
                'ksm'           => $this->KsmModel->getKSMByVilId($kel->id),
                'ksm1'          => $this->FacilitatorModel->getKSMByDistrictId($kab->id),
                'koordinator'   => $this->KorkotModel->getKorkotByProvId($prov->id)
            ];
            // print_r($this->KsmModel->get_data());die;
            return view('admin/master/user/create', $data);

        }else{
            return redirect()->to(base_url('/main/login')); 
        }
        // $this->s3('/Users/didd/Office/project/sua/website/php/sua/writable/uploads/register/1599555364_904d09b2cbf527fc4bcc.jpeg'); 

    }

    public function absent($data = null)
    {
        if ($this->request->isAJAX()) {
            $base64Img = service('request')->getPost('base64Img');

            //------------------------- decode image
            $dataENCODED    = $this->request->getPost('base64Img');
            $image_parts    = explode(";base64,", $dataENCODED);
            $image_type_aux = explode("image/", $image_parts[0]);
            $image_type     = $image_type_aux[1];
            $image_base64   = base64_decode($image_parts[1]);
            $filename       = uniqid() . '.jpg';
            $file           = WRITEPATH . 'uploads/absent/' . $filename;
            $return         = file_put_contents($file, $image_base64);

            //------------------------- upload S3
            $linkS3 = $this->s3($filename, $file, 'absent');


            //------------------------- absent confirmation
            $rkbDATA = array();
            $rkbDATA['photo']       = $linkS3;
            $rkbDATA['company_id']  = 3;

            $context = stream_context_create(array(
                'http' => array(
                    'method' => 'POST',
                    'header' => "Content-Type: application/json\r\n",
                    'content' => json_encode($rkbDATA)
                )
            ));

            $response = file_get_contents('http://103.93.56.248:8000/absent/confirmation', TRUE, $context);

            print_r($response);
            die;
            if ($response) {
                # code...

                print_r($file);
                print_r($linkS3);

                return json_encode($response);
            } else {
                // return "User tidak ditemukan";
                return $response;
            }
        } else {
            // die;
            $rkbDATA = array();
            $rkbDATA['photo']       = 'https://rkb-fr.s3-ap-southeast-1.amazonaws.com/absent/5f58e35be068c.jpg';
            $rkbDATA['company_id']  = 3;

            $context = stream_context_create(array(
                'http' => array(
                    'method' => 'POST',
                    'header' => "Content-Type: application/json\r\n",
                    'content' => json_encode($rkbDATA)
                )
            ));

            $result = file_get_contents('http://103.93.56.248:8000/absent/confirmation', FALSE, $context);
            $result = json_decode($result);
            var_dump($result);

            // return "kosong gan";
        }
    }

    //saving file to s3
    public function s3($key, $uri, $dir)
    {
        $s3 = new S3Client([
            'region'  => 'ap-southeast-1',
            'version' => 'latest',
            'credentials' => [
                'key'    => 'AKIAIFQK53A6O7EOSTVQ',
                'secret' => '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'
            ]
        ]);

        // Send a PutObject request and get the result object.
        $key = $dir . "/" . $key;

        $result = $s3->putObject([
            'Bucket'        => 'rkb-fr',
            'Key'           => $key,
            // 'SourceFile' => 'file:///Users/didd/Office/project/sua/website/php/sua/writable/uploads/register/1599555364_904d09b2cbf527fc4bcc.jpeg'
            'SourceFile'    => $uri,
            'ACL'           => 'public-read',

        ]);

        // Print the body of the result by indexing into the result object.
        // var_dump($result["@metadata"]["effectiveUri"]);

        return $result["@metadata"]["effectiveUri"];
    }

    public function create_username_password()
    {
        $this->UserModel->create_username_password();
    }
    //--------------------------------------------------------------------

    public function check_username()
    {
        $user = $this->request->getVar('username');
        $isActive = $this->usernameModel->get_usernameByUser($user);

        echo json_encode($isActive);
    }

    function getKabByProv()
    {
        $prov_id = $this->request->getVar('id_provinsi');
        $data = $this->DistrictModel->getKabByProvinceId($prov_id);
        echo json_encode($data);
    }

    function getSubByDistrict()
    {
        $dist_id = $this->request->getVar('id_kabupaten');
        $data = $this->SubdistrictModel->getSubByDistrictId($dist_id);
        echo json_encode($data);
    }

    function getVilBySub()
    {
        $sub_id = $this->request->getVar('id_kecamatan');
        $data = $this->VillageModel->getVilBySubId($sub_id);
        echo json_encode($data);
    }

    function getKSMByVil()
    {
        $vill_id = $this->request->getVar('id_kelurahan');
        $data = $this->KsmModel->getKSMByVilId($vill_id);
        echo json_encode($data);
    }

    function getKSMByDistrict()
    {
        $kab_id = $this->request->getVar('id_kabupaten');
        $data = $this->FacilitatorModel->getKSMByDistrictId($kab_id);
        echo json_encode($data);
    }

    function getKorkotByProv()
    {
        $prov_id = $this->request->getVar('id_provinsi');
        $data = $this->KorkotModel->getKorkotByProvId($prov_id);
        echo json_encode($data);
    }
}
