<?php

namespace App\Controllers\Admin\Master;

use App\Controllers\BaseController;
use App\Models\DistrictModel;
use CodeIgniter\HTTP\Files\UploadedFile;
use Aws\S3\S3Client;
use Aws\Exception\AwsException;
use App\Models\ProvinceModel;

class District extends BaseController
{
    protected $session = null;
    public $DistrictModel;
    public $generateTable;

    public function __construct()
    {
        $this->session = session();
        $this->DistrictModel    = new DistrictModel();
        $this->generateTable    = new \CodeIgniter\View\Table();
        $this->ProvinceModel    = new ProvinceModel();
    }

    public function index()
    {
        if (isset($this->session->id)) {
            $data  = $this->DistrictModel->get_data();

            foreach ($data as $key => $value)
                $data['table'][$key] = array($value->province, $value->code, $value->name, '
                        <div class="d-sm-flex align-items-center justify-content-between mb-1">
                            <a href="' . base_url('/admin/master/district/edit/' . $value->id) . '" class="d-none d-sm-inline-block btn btn-sm btn-info shadow-sm"><i class="fas fa-pen text-white-50"></i> Edit </a>
                            <a href="' . base_url('/admin/master/district/delete/' . $value->id) . '" class="d-none d-sm-inline-block btn btn-sm btn-danger shadow-sm" onclick="return confirm(\'Apakah akan menghapus data ?\');"><i class="fas fa-trash text-white-50"></i> Delete</a>
                        </div>');

            $data['table']  = $this->generateTable->generate($data['table']);

            //returning table body
            return view('admin/master/district/list', $data);            
        }else{
            return redirect()->to(base_url('/main/login'));  
        }
    }

    public function create()
    {
        if (isset($this->session->id)) {
            $data = [
                'provinsi'      => $this->ProvinceModel->getProvince()
            ];
            return view('admin/master/district/create', $data);
        }else{
            return redirect()->to(base_url('/main/login'));
        }

    }

    public function save()
    {
        $data = [
            'id_province' => $this->request->getVar('id_province'),
            'code' => $this->request->getVar('code'),
            'name' => $this->request->getVar('name'),
            'delete' => '0'
        ];

        //save to db

        $save  = $this->DistrictModel->save($data);

        if ($save) {
            return redirect()->to(base_url('admin/master/district'))->with('success', 'Data kabupaten successfully saved.');
        } else {
            return redirect()->back()->withInput()->with('error', 'We had trouble saving your data.');
        }
    }

    public function delete($id)
    {
        $data = [
            'id' => $id,
            'delete' => '1'
        ];

        $this->DistrictModel->save($data);

        return redirect()->to(base_url('admin/master/district'))->with('success', 'Data kabupaten successfully deleted.');
    }

    public function edit($id = '')
    {
        if (isset($this->session->id)) {
            $data = [
                'isEdit' => 'edit',
                'getKab' => $this->DistrictModel->get_data($id),
                'provinsi'      => $this->ProvinceModel->get_data()
            ];
            // print_r($this->DistrictModel->get_data($id));die;
            return view('admin/master/district/edit', $data);            
        }else{
            return redirect()->to(base_url('/main/login'));
        }
    }

    public function save_update()
    {
        $data = [
            'id' => $this->request->getVar('id'),
            'id_province' => $this->request->getVar('id_province'),
            'code' => $this->request->getVar('code'),
            'name' => $this->request->getVar('name'),
            'delete' => '0'
        ];
        // print_r($data);die;
        $save = $this->DistrictModel->save($data);
        if ($save) {
            return redirect()->to(base_url('admin/master/district'))->with('success', 'Data kabupaten successfully saved.');
        } else {
            return redirect()->back()->withInput()->with('error', 'We had trouble saving your data.');
        }
    }
}
