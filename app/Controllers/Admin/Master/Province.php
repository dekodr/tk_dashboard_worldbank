<?php

namespace App\Controllers\Admin\Master;

use App\Controllers\BaseController;
use Aws\S3\S3Client;
use Aws\Exception\AwsException;
use CodeIgniter\HTTP\Request;
use App\Models\ProvinceModel;

class Province extends BaseController
{
    protected $session = null;
    public $model;
    public $client;
    public $request = null;
    public $generateTable;

    public function __construct()
    {
        $this->generateTable    = new \CodeIgniter\View\Table();
        $this->request = \Config\Services::request();
        $this->client = \Config\Services::curlrequest();
        $this->model = new ProvinceModel();
        $this->session = session();
    }

    public function index()
    {
        if (isset($this->session->id)) {
            $data = $this->model->get_data();

            foreach ($data as $key => $value)
                 $data['table'][$key] = array($value->name,$value->code, '
                        <div class="d-sm-flex align-items-center justify-content-between mb-1">
                            <a href="' . base_url('/admin/master/province/edit/' . $value->id) . '" class="d-none d-sm-inline-block btn btn-sm btn-info shadow-sm"><i class="fas fa-pen text-white-50"></i> Edit </a>
                            <a href="' . base_url('/admin/master/province/delete/' . $value->id) . '" class="d-none d-sm-inline-block btn btn-sm btn-danger shadow-sm" onclick="return confirm(\'Apakah akan menghapus data ?\');"><i class="fas fa-trash text-white-50"></i> Delete</a>
                        </div>');

            $data['table']  = $this->generateTable->generate($data['table']);

            //returning table body
            return view('admin/master/province/list', $data);

        }else{
            return redirect()->to(base_url('/main/login'));
        }
    }

    public function create()
    {
        if (isset($this->session->id)) {
            return view('admin/master/province/create');    

        }else{
            return redirect()->to(base_url('/main/login'));
        }
    }

    public function save()
    {
        $data = [
            'name' => $this->request->getVar('name'),
            'code' => $this->request->getVar('code'),
            'delete' => '0'
        ];
        // $data['entry_date'] = date('Y-m-d H:i:s');
        // print_r($data);die;
        $save = $this->model->save($data);
        if ($save) {
            return redirect()->to(base_url('admin/master/province'))->with('success', 'Province successfully saved.');
        } else {
            return redirect()->back()->withInput()->with('error', 'We had trouble saving your data.');
        }
    }

    public function delete($id)
    {
        $data = [
            'id' => $id,
            'delete' => '1'
        ];

        $this->model->save($data);

        return redirect()->to(base_url('admin/master/province'))->with('success', 'Province successfully deleted.');
    }

    public function edit($id = '')
    {
        if (isset($this->session->id)) {
            $data = [
                'isEdit' => 'edit',
                'getProvince' => $this->model->get_data($id)
            ];

            return view('admin/master/province/edit', $data);

        }else{
            return redirect()->to(base_url('/main/login'));
        }
    }

    public function save_update()
    {
        $data = $this->request->getVar();

        $data['id'] = $data['id'];
        // $data['edit_date'] = date('Y-m-d H:i:s');

        $save = $this->model->save($data);
        if ($save) {
            return redirect()->to(base_url('admin/master/province'))->with('success', 'Province successfully saved.');
        } else {
            return redirect()->back()->withInput()->with('error', 'We had trouble saving your data.');
        }
    }
}
