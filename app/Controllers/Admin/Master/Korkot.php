<?php

namespace App\Controllers\Admin\Master;

use App\Controllers\BaseController;
use Aws\S3\S3Client;
use Aws\Exception\AwsException;
use CodeIgniter\HTTP\Request;
use App\Models\KorkotModel;
use App\Models\KsmModel;
use App\Models\ProvinceModel;
use App\Models\DistrictModel;

class Korkot extends BaseController
{
    protected $session = null;
    public $model;
    public $client;
    public $request = null;
    public $generateTable;
    public $villageModel;

    public function __construct()
    {
        $this->generateTable    = new \CodeIgniter\View\Table();
        $this->request = \Config\Services::request();
        $this->client = \Config\Services::curlrequest();
        $this->ksmModel = new KsmModel();
        $this->korkotmodel = new KorkotModel();
        $this->provincemodel = new ProvinceModel();
        $this->districtmodel = new DistrictModel();
        $this->session = session();
    }

    public function create()
    {
        if (isset($this->session->id)) {

            $prov = $this->provincemodel->get_data($kab->id_province);
            $data = [
                'ksm' => $this->ksmModel->get_data(),
                'prov' => $this->provincemodel->getProvince(),
                'district'  => $this->districtmodel->getKabByProvinceId($prov->id)
            ];

            return view('admin/master/korkot/create', $data);

        }else{
            return redirect()->to(base_url('/main/login'));
        }
    }

    public function delete($id)
    {
        $data = [
            'id' => $id,
            'delete' => 1
        ];
        $delete = $this->korkotmodel->save($data);
        return redirect()->to(base_url('admin/master/korkot'))->with('success', 'Data Fasilitator successfully deleted.');
    }

    public function edit($id = '')
    {
        if (isset($this->session->id)) {
            $data = [
                'isEdit' => 'edit',
                'ksm' => $this->ksmModel->get_data(),
                'prov' => $this->provincemodel->getProvince(),
                'getFasil' => $this->korkotmodel->get_data($id),
                'district'  => $this->districtmodel->getKabByProvinceId($prov->id)
            ];

            return view('admin/master/korkot/edit', $data);

        }else{
            return redirect()->to(base_url('/main/login'));
        }
    }

    public function index()
    {
        if (isset($this->session->id)) {
            $data = $this->korkotmodel->get_data();

            foreach ($data as $key => $value) {
                $_a = explode(',', $value->id_kabupaten);

                if (!empty($_a)) {
                    $_l = '<ul>';
                    foreach ($_a as $k => $v) {
                        $_d = $this->districtmodel->get_data($v);
                        $_l .= '<li>' . $_d->name . '</li>';
                    }
                    $_l .= '</ul>';
                } else {
                    $_l = '-';
                }

                $data['table'][$key] = array(
                    $value->code, $value->name, $_l,
                    '<div class="d-sm-flex align-items-center justify-content-between mb-1">
                <a href="' . base_url('/admin/master/korkot/edit/' . $value->id) . '" class="d-none d-sm-inline-block btn btn-sm btn-info shadow-sm"><i class="fas fa-pen text-white-50"></i> Edit </a>
                <a href="' . base_url('/admin/master/korkot/delete/' . $value->id) . '" class="d-none d-sm-inline-block btn btn-sm btn-danger shadow-sm" onclick="return confirm(\'Apakah akan menghapus data ?\');"><i class="fas fa-trash text-white-50"></i> Delete</a>
                '
                );
            }

            $data['table']  = $this->generateTable->generate($data['table']);

            //returning table body
            return view('admin/master/korkot/list', $data);

        }else{
            return redirect()->to(base_url('/main/login'));
        }
    }

    public function save()
    {
        $data = $this->request->getVar();

        $data['delete'] = 0;
        $data['id_kabupaten'] = implode(',', $data['id_kabupaten']);
        $data['entry_stamp'] = date('Y-m-d H:i:s');

        $save = $this->korkotmodel->save($data);
        if ($save) {
            return redirect()->to(base_url('admin/master/korkot'))->with('success', 'Data Fasilitator successfully saved.');
        } else {
            return redirect()->back()->withInput()->with('error', 'We had trouble saving your data.');
        }
    }

    public function save_update()
    {
        $data = $this->request->getVar();

        $data['id'] = $data['id'];
        $data['delete'] = 0;
        $data['id_kabupaten'] = implode(',', $data['id_kabupaten']);
        $data['id_provinsi'] = $data['id_provinsi'];
        $data['edit_stamp'] = date('Y-m-d H:i:s');

        $save = $this->korkotmodel->save($data);
        if ($save) {
            return redirect()->to(base_url('admin/master/korkot'))->with('success', 'Data Fasilitator successfully saved.');
        } else {
            return redirect()->back()->withInput()->with('error', 'We had trouble saving your data.');
        }
    }

    function getKabByProv()
    {
        $prov_id = $this->request->getVar('id_provinsi');
        $data = $this->districtmodel->getKabByProvinceId($prov_id);
        echo json_encode($data);
    }
}
