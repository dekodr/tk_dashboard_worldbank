<?php

namespace App\Controllers\Admin;

use App\Models\UserModel;
use App\Models\UsernameModel;
use App\Models\GraphModel;
use App\Models\PasswordModel;
use App\Controllers\BaseController;

class Dashboard extends BaseController
{

	protected $primaryKey = 'user_id';
	protected $helpers = ['form', 'date'];
	protected $session = null;
	protected $request = null;
	public $UserModel;
	public $UsernameModel;
	public $PasswordModel;
	public $Exception;
	public $GM;

	public function __construct()
	{
		$this->UserModel = new UserModel();
		$this->UsernameModel = new UsernameModel();
		$this->PasswordModel = new PasswordModel();
		// $this->GM = new GraphModel();
		$this->session = session();
		$this->request = \Config\Services::request();

		$this->is_logged_in();
	}

	public function index()
	{
		// if (isset($this->session->id)) {
		// 	echo "Hai";
		// 	die;
		// 	$data 			= $this->GM->total_report();
		// 	$data['ksm'] 	= $this->GM->report_ksm();
		// 	$data['kel'] 	= $this->GM->report_kel();
		// 	$data['project'] = $this->GM->report_project();
		// 	$data['reportByType'] = $this->GM->reportByType();
		// 	// print_r($this->GM->report_ksm());die;
		// 	return view('admin/dashboard', $data);
		// } else {
		return redirect()->to(base_url('uji-petik'));
		// }
	}

	public function filter_dashboard()
	{
		$post = $this->request->getVar();
		$data = $this->GM->filter_dashboard($post);
		echo json_encode($data);
	}

	public function detail_graph()
	{
		if (isset($this->session->id)) {
			$data['ksm'] 	= $this->GM->report_ksm();
			$data['kel'] 	= $this->GM->report_kel();
			$data['province'] 	= $this->GM->report_province();
			$data['kabupaten'] 	= $this->GM->report_kabupaten();
			$data['reportByType'] = $this->GM->reportByType();

			return view('admin/detail_graph', $data);
		} else {
			return redirect()->to(base_url('/main/login'));
		}
	}
}
